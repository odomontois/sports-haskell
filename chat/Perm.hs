{-# LANGUAGE ViewPatterns #-}
module Chat.Perm where

import           Data.List (delete, (!!))
import qualified Data.Set  as Set

decodePerm::Integer->Integer
decodePerm x = snd $ foldr go ((Set.fromList [0..9], x), 0) [10, 9..1] where
    go i ((digs, code), num) = let (code', fromInteger -> d) = code `divMod` i
                                   dig = Set.elemAt d digs
                                   digs' = Set.deleteAt d digs
                                   num' = num * 10 + dig
                                in ((digs', code'), num')


decodePerm1::Integer->Integer
decodePerm1 x = snd $ foldr go (([0..9], x), 0) [10, 9..1] where
    go i ((digs, code), num) = let (code', d) = code `divMod` i
                                   dig = digs !! fromInteger d
                                   digs' = delete  dig digs
                                   num' = num * 10 + dig
                                in ((digs', code'), num')
