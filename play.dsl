GET   /clients/:id          controllers.Clients.show(id: Long)
GET   /clients              controllers.Clients.list(page: Int ?= 1)
GET   /:page                controllers.Application.show(page)
