{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE LambdaCase #-}

module Kata.Coroutine where

import Control.Foldable
import Control.Monad

--import Preloaded

-- Preloaded contains the following:
--
newtype Coroutine r u d a = Coroutine {runCoroutine :: (Command r u d a -> r) -> r} deriving (Functor)

--
data Command r u d a
  = Done a
  | Out d (Coroutine r u d a)
  | In (u -> Coroutine r u d a)
  deriving (Functor)

-- Useful alias
apply :: Coroutine r u d a -> (Command r u d a -> r) -> r
apply = runCoroutine

instance Applicative (Coroutine r u d) where
  pure = return
  (<*>) = ap

instance Monad (Coroutine r u d) where
  return x = Coroutine $ \k -> k $ Done x
  f >>= g = Coroutine $ \k -> runCoroutine f $ \case
    Done a -> runCoroutine (g a) k
    Out d c -> k $ Out d $ c >>= g
    In l -> k $ In $ l >=> g

--concatFeed :: m -> Coroutine r u m a -> Coroutine r m d a -> Coroutine r u d a
--concatFeed m p1 p2 = Coroutine $ \k -> runCoroutine p2 $ \case
--  Done a -> k $ Done a
--  Out d p2' -> k $ Out d $ concatFeed m p1 p2'
--  In l -> p1 >>> l m `runCoroutine` k

--(>>>) :: Coroutine r u m a -> Coroutine r m d a -> Coroutine r u d a
--p1 >>> p2 = Coroutine $ \k -> runCoroutine p1 $ \case
--  Done a -> k $ Done a
--  In l -> k $ In $ \u -> l u >>> p2
--  Out m p1' -> concatFeed m p1' p2 `runCoroutine` k

(>>>) :: Coroutine r u m a -> Coroutine r m d a -> Coroutine r u d a
p1 >>> p2 = Coroutine $ \k -> runCoroutine p2 $ \case
  Done a -> k $ Done a
  In l -> pipe2 p1 l `runCoroutine` k
  Out d p2' -> k $ Out d $ p1 >>> p2'

-- It might be useful to define the following function
pipe2 :: Coroutine r u m a -> (m -> Coroutine r m d a) -> Coroutine r u d a
pipe2 p1 f = Coroutine $ \k -> runCoroutine p1 $ \case
  Done a -> k $ Done a
  In l -> k $ In $ \u -> pipe2 (l u) f
  Out m p1' -> p1' >>> f m `runCoroutine` k

-- Library functions

output :: a -> Coroutine r u a ()
output v = Coroutine $ \k -> k $ Out v $ return ()

input :: Coroutine r v d v
input = Coroutine $ \k -> k $ In return

produce :: [a] -> Coroutine r u a ()
produce = mapM_ output

consume :: Coroutine [t] u t a -> [t]
consume = flip runCoroutine $ \case
  Out t c -> t : consume c
  _ -> []

filterC :: (v -> Bool) -> Coroutine r v v ()
filterC p = do
  u <- input
  if p u then output u >> filterC p else filterC p

pass :: Coroutine r v v ()
pass = input >>= output

ident :: Coroutine r v v a
ident = forever pass

limit :: Int -> Coroutine r v v ()
limit n = replicateM_ n pass

suppress :: Int -> Coroutine r v v ()
suppress n = if n <= 0 then ident else input >> suppress (n - 1)

add :: Coroutine r Int Int ()
add = forever $ liftM2 (+) input input >>= output

duplicate :: Coroutine r v v ()
duplicate = forever $ input >>= liftM2 (>>) output output

-- Programs
-- 1. A program which outputs the first 5 even numbers of a stream.
-- 2. A program which produces a stream of the triangle numbers
-- 3. A program which multiplies a stream by 2
-- 4. A program which sums adjacent pairs of integers

p1, p2, p3, p4 :: Coroutine r Int Int ()
p1 = filterC even >>> limit 5
p2 = produce $ scanl (+) 1 [2 ..]
p3 = forever $ input >>= output . (* 2)
p4 = input >>= go where go x = input >>= liftM2 (>>) (output . (x +)) go
