{-# LANGUAGE RankNTypes #-}

module Kata.Church where

import Prelude hiding (Bool, False, True, and, not, or, succ, (&&), (/=), (==), (||))

newtype Number = Nr (forall a. (a -> a) -> a -> a)

zero :: Number
zero = Nr (\_ z -> z)

succ :: Number -> Number
succ (Nr a) = Nr (\s z -> s (a s z))

one :: Number
one = succ zero

add :: Number -> Number -> Number
add (Nr a) = a succ

mult :: Number -> Number -> Number
mult (Nr a) b = a (add b) zero

pow :: Number -> Number -> Number
pow a (Nr b) = b (mult a) one

type Boolean = forall a. a -> a -> a -- this requires RankNTypes

false,true :: Boolean
false _ x = x
true = const

not :: Boolean -> Boolean
and,or,xor :: Boolean -> Boolean -> Boolean

not = flip
and a = flip a false
or a = a true
xor a b = a (not b) b
