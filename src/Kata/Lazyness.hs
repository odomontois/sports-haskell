module Kata.Lazyness where

import Data.List
import Data.Maybe
import Control.Monad

type Matrix = [[Bool]]

generate :: Int -> Int -> Matrix
generate n m =
  let falses = repeat False
      oneTrue = replicate m False ++ [ True ] ++ falses
  in replicate n falses ++ [oneTrue] ++ repeat falses

findTrue :: Matrix -> (Int, Int)
findTrue =
  let ixmatrix = zipWith (\i -> zipWith (\j a -> ((i, j), a)) [0 ..]) [0 ..]
      triangle = zipWith take [1 ..]
      merge (x : xs) (y : ys) = x : y : merge xs ys
      lst ms = let ix = ixmatrix ms in join $ merge (triangle ix) $ triangle $ transpose ix
   in fst . fromJust . find snd . lst
