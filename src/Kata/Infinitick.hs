module Kata.Infinitick where

import Data.Char
import Data.Word

interpreter :: String -> String
interpreter = go 0 [0, 0 ..] [0, 0 ..] . cycle
  where
    go :: Word8 -> [Word8] -> [Word8] -> String -> String
    go x ~ls@(l : ls') ~rs@(r : rs') (c : cs) = case c of
      '>' -> go r (x : ls) rs' cs
      '<' -> go l ls' (x : rs) cs
      '+' -> go (x + 1) ls rs cs
      '-' -> go (x - 1) ls rs cs
      '*' -> chr (fromIntegral x) : go x ls rs cs
      '&' -> []
      '/' -> go x ls rs $ if x == 0 then tail cs else cs
      '\\' -> go x ls rs $ if x == 0 then cs else tail cs
      _ -> go x ls rs cs
