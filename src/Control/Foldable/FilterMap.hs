{-# LANGUAGE ExistentialQuantification #-}

module Control.Foldable.FilterMap where

import           Control.Applicative
import           Control.Foldable.Zip
import           Control.Monad
import           Data.Functor.Compose
import           Data.Maybe

data FilterMap f a = forall b. FilterMap (b->Bool) (b -> a) (f b)

instance Foldable f=>Foldable (FilterMap f) where
  foldMap f (FilterMap p g xs) = fromMaybe mempty $ foldMap h xs where
    h = fmap (f.g). mfilter p. return

instance Functor (FilterMap f) where
  fmap f (FilterMap p g xs) = FilterMap p (f.g) xs

instance Zip f => Zip (FilterMap f) where
  fzipWith f (FilterMap p g xs) (FilterMap q h ys) =
    FilterMap isJust fromJust $ fzipWith fm (fmap u xs) (fmap v ys) where
      u  = fmap g. mfilter p. return
      v  = fmap h. mfilter q. return
      fm = liftA2 f

instance Applicative f => Applicative (FilterMap f) where
  pure = FilterMap (const True) id . pure
  FilterMap pf mf f <*> FilterMap p m x = FilterMap isJust fromJust res where
    res = getCompose $ (Compose $ fmap u f) <*> (Compose $ fmap v x)
    u =  fmap mf . mfilter pf. return
    v  = fmap m  . mfilter p . return
