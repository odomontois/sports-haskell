{-# LANGUAGE TypeSynonymInstances #-}
module Control.Foldable.Zip where

import           Control.Applicative
import           Data.Functor.Compose
import           Data.Sequence        (Seq)
import qualified Data.Sequence        as Seq

class Functor f=>Zip f where
  fzip::f a->f b->f (a,b)
  fzip = fzipWith (,)

  fzipWith::(a->b->c)->f a->f b->f c
  fzipWith f = (fmap (uncurry f) .) . fzip

  fzip3::f a->f b->f c->f (a,b,c)
  fzip3 = (fzipWith ($) .). fzipWith (,,)

  fzipWith3::(a->b->c->d)->f a->f b->f c->f d
  fzipWith3 f = (fzipWith ($) .). fzipWith f

instance Zip [] where
  fzip = zip
  fzipWith = zipWith
  fzip3 = zip3
  fzipWith3 = zipWith3

instance Zip Seq where
  fzip = Seq.zip
  fzipWith = Seq.zipWith
  fzip3 = Seq.zip3
  fzipWith3 = Seq.zipWith3

instance Zip ((->) a) where
  fzipWith f g h x = f (g x) (h x)

instance Monoid a=>Zip ((,) a) where
  fzipWith f (x, c) (y, d) = (mappend x y, f c d)

instance (Zip f, Zip g)=>Zip (Compose f g) where
  fzipWith f (Compose x) (Compose y) = Compose $ fzipWith (fzipWith f) x y

zipComp::(Zip f, Zip g)=>f (g a)->f (g b)->f (g (a, b))
zipComp x y = getCompose $ fzip (Compose x) (Compose y)

zipWithComp::(Zip f, Zip g)=>(a->b->c)->f (g a)->f (g b)->f (g c)
zipWithComp f x y = getCompose $ fzipWith f (Compose x) (Compose y)

zip3Comp::(Zip f, Zip g)=>f (g a)->f (g b)->f (g c)->f (g (a, b, c))
zip3Comp x y z = getCompose $ fzip3 (Compose x) (Compose y) (Compose z)

zipWith3Comp::(Zip f, Zip g)=>(a->b->c->d)->f (g a)->f (g b)-> f (g c)->f (g d)
zipWith3Comp f x y z = getCompose $ fzipWith3 f (Compose x) (Compose y) (Compose z)

zipComp3::(Zip f, Zip g, Zip h)=>f (g (h a))->f (g (h b))->f (g (h (a, b)))
zipComp3 x y = getCompose $  getCompose $ fzip (Compose $ Compose x) (Compose $ Compose y)

zipWithComp3::(Zip f, Zip g, Zip h)=>(a->b->c)->f (g (h a))->f (g (h b))->f (g (h c))
zipWithComp3 f x y = getCompose $  getCompose $ fzipWith f (Compose $Compose x ) (Compose $ Compose y)

zip3Comp3::(Zip f, Zip g, Zip h)=>f (g (h a))->f (g (h b))->f (g (h c))->f (g (h (a, b, c)))
zip3Comp3 x y z = getCompose $ getCompose $ fzip3 (Compose $ Compose x) (Compose $ Compose y) (Compose $ Compose z)

zipWith3Comp3::(Zip f, Zip g, Zip h)=>(a->b->c->d)->f (g (h a))->f (g (h b))->f (g (h c))->f (g (h d))
zipWith3Comp3 f x y z= getCompose $  getCompose $ fzipWith3 f (Compose $Compose x ) (Compose $ Compose y)  (Compose $ Compose z)
