module Math.Seq where

import           Data.List
import           GHC.Exts

convGen::(a->a->a)->(a->a->a)->a->[a]->[a]->[a]
convGen mul plus zero = go where
  go [] _ = []
  go [x] ys = fmap (mul x) ys
  go (x:xs) ys = zipWith plus (zero:go xs ys) $ fmap (mul x) ys ++ repeat zero

conv::Num a=>[a]->[a]->[a]
conv = convGen (*) (+) 0

combinations::Int->[a]->[[a]]
combinations 0 _ = [[]]
combinations k xs = do
    x:rest <- take (length xs - k + 1) $ tails xs
    fmap (x:) (combinations (k - 1) rest)

indexes::Int->[Int]->[Bool]
indexes n = go 0 where
  go i xs      | i == n  = []
  go i (x:xs ) | i == x = True : go (i + 1) xs
  go i xs               = False : go (i + 1) xs
