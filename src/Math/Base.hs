{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE DataKinds, PolyKinds, TypeApplications, ScopedTypeVariables #-}

module Math.Base where

import           Data.Proxy
import           Data.Kind
import           Data.List                     as L
import           Data.Maybe                     ( fromMaybe )
import           Control.Arrow
import           Data.Ratio
import           Data.Vector.Unboxed           as V

class IntValueOf b where
    value :: Proxy b -> Int

newtype Base t = Base {getBase :: Int} deriving (Eq, Ord, Enum)

deriving instance IntValueOf b => Real (Base b)
deriving instance IntValueOf b => Integral (Base b)

instance IntValueOf b => Show (Base b) where
    showsPrec _ (Base a) =
        shows a . (" (mod " <>) . shows (value $ Proxy @b) . (")" <>)


base :: forall b . IntValueOf b => Int -> Base b
base a = Base $ a `mod` value (Proxy @b)

instance IntValueOf b => Num (Base b) where
    (Base x) + (Base y) = Base $ (x + y) `mod` value (Proxy @b)
    (Base x) - (Base y) = Base $ (x - y) `mod` value (Proxy @b)
    (Base x) * (Base y) = Base $ (x * y) `mod` value (Proxy @b)
    abs         = id
    signum      = id
    fromInteger = base . fromInteger


instance IntValueOf b => Bounded (Base b) where
    minBound = 0
    maxBound = Base $ value (Proxy @b) - 1

instance IntValueOf b => Fractional (Base b) where
    recip = Base . (cache V.!) . fromIntegral      where
        nums = [0 .. maxBound :: Base b]
        calc n = fromMaybe (Base 0) $ L.find (\p -> p * n == 1) nums
        cache = V.fromList $ getBase . calc <$> nums
    fromRational r = fromInteger (numerator r) / fromInteger (denominator r)


