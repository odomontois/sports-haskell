{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE InstanceSigs              #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeFamilies              #-}

module Math.Primes where
import           Control.Foldable
import           Data.Foldable
import           Data.Monoid
import           Data.Proxy

class FactorKind k where
  fkPrimes::Integral n=>Proxy k->[n]
  fkPrimes _ = primes

  fkFacts::Proxy k->[a]->[a]
  fkFacts _  = id

data AllFactors
data OddFactors
data SingularFactors
data SingularOddFactors

allFactors::Proxy AllFactors
allFactors = Proxy

oddFactors::Proxy OddFactors
oddFactors = Proxy

singularFactors::Proxy SingularFactors
singularFactors = Proxy

singularOddFactors::Proxy SingularOddFactors
singularOddFactors = Proxy

instance FactorKind AllFactors
instance FactorKind OddFactors where fkPrimes _ = tail primes
instance FactorKind SingularFactors where fkFacts _ = tail
instance FactorKind SingularOddFactors where
  fkPrimes _ = tail primes
  fkFacts _ = tail

primes::Integral a=>[a]
primes = 2:3:filter isPrime [5,7..]

isPrime::Integral n=>n->Bool
isPrime n = all (\p->mod n p /= 0) . takeWhile (\p->p*p<=n) $ tail primes

data FactStep n s = FactStep{
  fsPrevNum   ::n,
  fsPrime     ::n,
  fsLastPrime ::n,
  fsVal       ::s
}

instance Functor (FactStep n) where
  fmap f (FactStep pn p lp v) = FactStep pn p lp (f v)

fsNum::Num n=>FactStep n s->n
fsNum step = fsPrevNum step * fsPrime step

data GenFactsI k n a=forall s.GenFacts{
  gfLimit ::n,
  gfStart ::s,
  gfMap   ::s->a,
  gfAccum ::FactStep n s->s,
  gfProxy ::Proxy k
}

type GenFacts = GenFactsI AllFactors

instance (FactorKind k, Integral n)=>Foldable (GenFactsI k n) where
  foldMap f (GenFacts lim start fm accum proxy) = mappend initAcc run  where
    run = go 1 1 start $ fkPrimes proxy
    initAcc = f . fm . accum $ FactStep 1 1 1 start
    go n lp s (p:ps) = let
      n' = n * p
      val = accum (FactStep n p lp s)
      next = go n lp s ps
      further = go n' p val $ fkFacts proxy (p:ps)
      that = f (fm val)
      in if n' > lim then mempty else that <> further <> next

instance Functor (GenFactsI k n) where
  fmap f (GenFacts lim s g a proxy) = GenFacts lim s (f.g) a proxy

instance Ord n=>Zip (GenFactsI k n) where
  fzipWith f (GenFacts lx sx fmx ax px) (GenFacts ly sy fmy ay py) =
    GenFacts (min lx ly) (sx, sy) fm accum px where
      accum step =  (ax (fmap fst step), ay (fmap snd step))
      fm (x,y) = f (fmx x) (fmy y)

-- | Just the number itseld
gfValI::Num n=>Proxy k->n->GenFactsI k n n
gfValI proxy lim = GenFacts lim 0 id fsNum proxy

gfVal::Num n=>n->GenFacts n n
gfVal = gfValI Proxy

-- | Euler totient function
gfTotientI::(Num n, Eq n)=>Proxy k->n->GenFactsI k n n
gfTotientI proxy lim = GenFacts lim 1 id f proxy where
  f step = fsVal step *
    if fsPrime step == fsLastPrime step then fsPrime step
      else fsPrime step - 1

gfTotient::(Num n, Eq n)=>n->GenFacts n n
gfTotient = gfTotientI Proxy

gfOddTotient::Integral n=>n->GenFactsI OddFactors n n
gfOddTotient = fmap (`div` 2) . gfTotientI Proxy

-- | Prime factorization
gfPFactI::(Eq n, Num a)=>Proxy k->n->GenFactsI k n [(n,a)]
gfPFactI proxy lim = GenFacts lim [] id f proxy where
  f step = if fsPrime step == fsLastPrime step
            then case fsVal step of
              [] -> [(fsPrime step, 1)]
              (p, count):ps -> (p, count + 1):ps
            else (fsPrime step, 1) : fsVal step

gfPFact::(Eq n, Num a)=>n->GenFacts n [(n,a)]
gfPFact = gfPFactI Proxy
