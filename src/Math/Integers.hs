module Math.Integers(
  euler, sqrint,sqr, pithagorean, coprimes, oddCoprimes
) where

import           Control.Applicative
import           Control.Monad
import           Data.Tuple
import           System.IO.Unsafe

euler::(Integral n, Show n)=>n->n->n->Maybe (n,n)
euler  = ((fmap equalize .).). go where
  equalize ((x,u),(y,v))
    | x >= 0    = (x,y)
    | u > 0     = (x + u, y + v)
    | otherwise = (x - u, y - v)
  go a b k  | k == 0 = let  u = gcd a b
      in Just ((0,- div b u), (0,div a u))
            | b > a  = swap <$> go b a k
            | b == 0 = do
    let (d,r) = divMod k a
    guard $ r == 0
    return ((d,0), (0,1))
    | otherwise = do
      let (d,r) = divMod a b
          (u,v) = divMod k b
      ((t,f),(s,e)) <- go b r v
      return ((s,e), (t + u - s * d, f - e*d))

sqrint::Integral a=>a->(a,a)
sqrint n | n < 0 = error "negative square"
sqrint 0 = (0,0)
sqrint 1 = (1,0)
sqrint 2 = (1,1)
sqrint 3 = (1,2)
sqrint x  =
  let (d4, r4) = divMod x 4
      (q, r)   = sqrint d4
      r'  = 4*r + r4
      q'  = 2 * q
      r'' = r' - 2*q' - 1
  in if r'' >= 0 then (q' + 1, r'') else (q' , r')

sqr::Integral a=>a->a
sqr = fst.sqrint

coPrimeProcedure::(Num n, Ord n)=>(n,n)->n->[(n,n)]
coPrimeProcedure (m,n) lim
  | m > lim = []
  | otherwise = (m,n) :
    coPrimeProcedure (2*m - n, m) lim ++
    coPrimeProcedure (2*m + n, m) lim ++
    coPrimeProcedure (m + 2*n, n) lim

coprimes, oddCoprimes::(Num n, Ord n)=>n->[(n,n)]
coprimes = coPrimeProcedure (2,1)
oddCoprimes = coPrimeProcedure (3,1)

pithagorean::Integral a=>a->[(a, a, a)]
pithagorean r = do
    (i,j) <- oddCoprimes r
    let i2 = i*i
        j2 = j*j
        c = div (i2 + j2) 2
        b = div (i2 - j2) 2
        a = i * j
    guard $ c <= r
    return (a, b, c)
