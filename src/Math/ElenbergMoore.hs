{-# LANGUAGE TypeFamilies , FlexibleContexts, GeneralizedNewtypeDeriving, MultiParamTypeClasses #-}
module ElenbergMoore where

import Control.Category
import Data.Proxy
import Prelude hiding (id, (.))

class (Category (GFFrom f), Category (GFTo f)) => GFunctor f where
    type GFFrom  f :: (* -> * -> *)
    type GFTo    f :: (* -> * -> *)
    apply::Proxy f -> GFFrom f a b -> GFTo f a b

newtype Algebra f a = Algebra{ getAlgebra:: f a -> a }

newtype ElenbergMoore (f :: * -> * ) a b = EM{ runEM:: a -> b } 

instance Category (ElenbergMoore f) where
    id = EM id
    EM f . EM g = EM (f . g)

data ForgetAlgebra (f :: * -> *)

instance GFunctor (ForgetAlgebra f) where
    type GFFrom   (ForgetAlgebra f) = ElenbergMoore f
    type GFTo     (ForgetAlgebra f) = (->)
    apply _ = runEM

data FreeAlgebra (f :: * -> *)

instance GFunctor (FreeAlgebra f) where
    type GFFrom   (FreeAlgebra f) = (->)
    type GFTo     (FreeAlgebra f) = ElenbergMoore f
    apply _ = EM

class (GFunctor f , GFunctor g, GFFrom f ~ GFTo g, GFTo g ~ GFFrom f) => Adjoint f g where
    leftAdjunct :: Proxy f -> Proxy g -> GFTo f a b -> GFTo g a b
    rightAdjunct :: Proxy f -> Proxy g -> GFTo g a b -> GFTo f a b

    adjUnit :: Proxy f -> Proxy g -> GFTo g a a
    adjUnit pf pg = leftAdjunct pf pg id

    adjCounit :: Proxy f -> Proxy g -> GFTo f a a
    adjCounit pf pg = rightAdjunct pf pg id   
    
instance Adjoint (FreeAlgebra f) (ForgetAlgebra f) where
    leftAdjunct _ pg = apply pg
    rightAdjunct pf _ = apply pf
