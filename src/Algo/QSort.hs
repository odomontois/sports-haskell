{-# LANGUAGE GADTs      #-}
{-# LANGUAGE MultiWayIf #-}
import           Control.Monad       hiding (replicateM)
import           Data.Vector.Mutable
import           Prelude             hiding (length, read)
import           System.Random

medOfmeds::Ord x=>IOVector x->IO ()
medOfmeds v = let l = length v
                  choose x y z | x > y     = if z > x then 0 else if y > z then 1 else 2
                               | otherwise = if z > y then 1 else if x > z then 0 else 2
                  cell i j = read v (i * 3 + j)
                  go3 i = do
                    t <- liftM3 choose (cell i 0) (cell i 1) (cell i 2)
                    swap v i (3*i+t)
                  walk = forM_ [0..quot l 3 - 1] go3
                  recur = medOfmeds (slice 0 ((l + 2) `quot` 3) v)
              in unless (l < 3) $ walk >> recur

qsort::Ord x=>IOVector x->IO ()
qsort v = unless (length v <= 1) $ do
  medOfmeds v
  m <- read v 0
  let go i j = if i == j then return i else do
          r <- read v (i+1)
          if r < m
            then swap v i (i+1) >> go (i+1) j
            else swap v (i+1) j >> go i (j-1)
  i <- go 0 (length v - 1)
  qsort $ slice 0 i v
  qsort $ slice (i+1) (length v-i-1) v

printVec::Show x=>IOVector x->IO ()
printVec v = mapM (read v) [0..length v - 1] >>= putStrLn.unwords.map show

main :: IO ()
main = do
  xs <- replicateM 100 $ randomRIO (1, 1000) :: IO (IOVector Int)
  printVec xs
  qsort xs
  printVec xs
