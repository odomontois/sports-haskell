{-# LANGUAGE DeriveFunctor       #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections       #-}

module Algo.SegTree.Valued where

import           Control.Applicative
import           Control.Monad
import           Data.Array.IArray
import           Data.Array.ST.Safe
import           Data.Char
import           Data.Monoid

data IMonoid a = IMonoid{_mappend::a->a->a, _mempty::a}
imonoid::Monoid a=>IMonoid a
imonoid = IMonoid{_mappend = mappend, _mempty = mempty}

data SegTree a = SegTree{ segValues :: Array Int a, segLength:: Int} deriving (Show, Functor)

_splitSegs::Int->Int->Int->(Int->Int->Int->a)->(a->a->a)->a
_splitSegs k i j f p = let m = quot (i + j + 1) 2
                           u = 2 * k + 1
                       in f u i m `p` f (u + 1) m j

segTree::forall a.IMonoid a->[a]->SegTree a
segTree IMonoid{..} xs = let  n = length xs
                              vals::Array Int a
                              vals = listArray (0, n - 1) xs
                              calcSize k i j | i + 1 == j = k
                                             | otherwise = _splitSegs k i j calcSize max
                              size = calcSize 0 0 n
                              arr = runSTArray $ do
                                els <- newArray (0, size) _mempty
                                let fill k i j | i + 1 == j = let res = vals ! i in writeArray els k res >> return res
                                               | otherwise  = do
                                                 res <- _splitSegs k i j fill $ liftA2 _mappend
                                                 writeArray els k res
                                                 return res
                                void $ fill 0 0 n
                                return els
                          in SegTree arr n

segCalc::IMonoid a->SegTree a->Int->Int->a
segCalc IMonoid{..} SegTree{..} l r = go 0 0 segLength
  where go k i j| i >= l && j <= r = segValues ! k
                | i >= r || j <= l = _mempty
                | otherwise        = _splitSegs k i j go _mappend
