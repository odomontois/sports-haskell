{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Utils.Arrays where

import           Control.Monad
import           Control.Monad.ST
import           Data.Array.MArray
import           Data.Array.ST
import           Data.Array.Unboxed


fromListFold::Ix i=>(a->b->b)->b->(i,i)->[(i,a)]->Array i b
fromListFold comb z bs lst = runSTArray $ do
  arr <- newArray bs z
  forM_ lst $ \(i,x) -> do
    new <- comb x <$> readArray arr i
    writeArray arr i new
  return arr

fromListFoldInt::forall i a.Ix i=>(Int->a->Int)->Int->(i,i)->[(i,a)]->UArray i Int
fromListFoldInt comb z bs lst = runSTUArray $ do
  arr <- newArray bs z
  forM_ lst $ \(i,x) -> do
    new <- (`comb` x) <$> readArray arr i
    writeArray arr i new
  return arr

fromListWith::forall i a.Ix i=>(a->a->a)->(i,i)->[(i,a)]->Array i a
fromListWith comb bs lst = runSTArray $ do
  arr <- newArray_ bs
  wasA <- newArray bs False:: ST s (STUArray s i Bool)
  forM_ lst $ \(i,x) -> do
    was <- readArray wasA i
    new <- if was then (`comb` x) <$> readArray arr i else pure x
    writeArray wasA i True
    writeArray arr i new
  return arr

fromListWithInt::forall i.Ix i=>(Int->Int->Int)->(i,i)->[(i,Int)]->UArray i Int
fromListWithInt comb bs lst = runSTUArray $ do
  arr <- newArray_ bs
  wasA <- newArray bs False:: ST s (STUArray s i Bool)
  forM_ lst $ \(i,x) -> do
    was <- readArray wasA i
    new <- if was then (`comb` x) <$> readArray arr i else pure x
    writeArray wasA i True
    writeArray arr i new
  return arr
