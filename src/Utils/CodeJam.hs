{-#LANGUAGE ExistentialQuantification#-}

module Utils.CodeJam(
  module System.IO,
  Prog(..),
  runProg,
  caseProg,
  runCases
) where

import System.IO
import System.Environment
import Control.Monad
import Control.Applicative

data Prog a = Prog {
  progRead::Handle->IO a,
  progWrite::Handle->a->IO ()
}

runProg::String->Prog a->IO ()
runProg name prog = do
  args <- getArgs
  let cas = case args of
        c: _ -> c
        _    -> "test"
  let infile  = name ++"-" ++ cas ++ ".in"
      outfile = name ++"-" ++ cas ++ ".out"
  inH <- openFile infile ReadMode
  outH <- openFile outfile WriteMode
  x <- progRead prog inH
  progWrite prog outH x
  hClose outH
  hClose inH

caseProg::Prog a->Prog [(a, Integer)]
caseProg prog = let
  pRead handle = do
    n <- read <$> hGetLine handle
    as <- replicateM n $ progRead prog handle
    return (zip as [1..])
  pWrite handle ais =  forM_ ais $ \(a,i) -> do
    hPutStr handle $ "Case #"++show i++": "
    progWrite prog handle a
  in Prog pRead pWrite

runCases :: String -> (Handle -> IO a) -> (Handle -> a -> IO ()) -> IO ()
runCases name pw pr = runProg name $ caseProg $ Prog pw pr
