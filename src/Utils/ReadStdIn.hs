module Utils.ReadStdIn where

import qualified Data.ByteString.Char8 as BS
import qualified Data.Vector.Unboxed           as UVec

getInts::IO [Int]
getInts = go <$> BS.getLine where
  go s = case  BS.readInt s of
      Nothing      -> []
      Just (x, s') -> x : go (BS.drop 1 s')

getInt::IO Int
getInt = do
  line <- BS.getLine
  let Just (x, _ ) = BS.readInt line
  return x

getInt2::IO (Int,Int)
getInt2 = do
  line <- BS.getLine
  let Just (x, l') = BS.readInt line
      Just (y, _)  = BS.readInt $ BS.drop 1 l'
  return (x,y)

getInt3::IO (Int,Int, Int)
getInt3 = do
  line <- BS.getLine
  let Just (x, l') = BS.readInt line
      Just (y, l'')  = BS.readInt $ BS.drop 1 l'
      Just (z, _) = BS.readInt $ BS.drop 1 l''
  return (x,y,z)

getInt4::IO (Int,Int, Int, Int)
getInt4 = do
  line <- BS.getLine
  let Just (x1, l1) = BS.readInt line
      Just (x2, l2)  = BS.readInt $ BS.drop 1 l1
      Just (x3, l3) = BS.readInt $ BS.drop 1 l2
      Just (x4, _) = BS.readInt $ BS.drop 1 l3
  return (x1, x2, x3, x4)

getVector :: IO (UVec.Vector Int)
getVector = UVec.unfoldr (BS.readInt . BS.drop 1) <$> BS.getLine
