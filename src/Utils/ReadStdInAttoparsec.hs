module ReadStdInAttoparsec where
import           Data.Attoparsec.ByteString.Char8
import           Data.ByteString.Char8            (getLine)
import           Prelude                          hiding (getLine)

double3:: Parser (Double, Double, Double)
double3 = (,,) <$> double <*> (space *> double) <*> (space *>double)

decimal3::Parser (Int,Int, Int)
decimal3 =  (,,) <$> decimal <*> (space *> decimal) <*> (space *> decimal)

int3::Parser (Int,Int, Int)
int3 =  let int = signed decimal in (,,) <$> int <*> (space *> int) <*> (space *> int)

parseLine:: Parser a->IO a
parseLine p = do
  Right x <- parseOnly p <$> getLine
  return x
