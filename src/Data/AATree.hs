{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE ViewPatterns          #-}
module Data.AATree
    (
    ) where


import           Control.Arrow
import           Data.List      (foldl', sortBy)
import           Data.Ord       (comparing)
import           Data.Semigroup
import           Debug.Trace

class Semigroup (Measure a)=>Measured a where
  type Measure a
  measure::a->Measure a

data AATree a where
   AASingle::a->AATree a
   AANode::Measured a=>Int->Measure a->AATree a->AATree a->AATree a

data DisplayTree a where
 DisplayTree::(Measured a, Show a,Show (Measure a))=>AATree a->DisplayTree a

instance Show (DisplayTree a) where
 showsPrec _ (DisplayTree t)  = pref "" "  " t where
    pref s _ (AASingle x) = showString (s <> "+:") . shows x
    pref s u (AANode l v tl tr) =
       showString (s <> "+-[").shows l.showString "]".shows v.showString "\n".
         pref (s <> u) "| " tl. showString "\n".
         pref (s <> u) "  " tr

level::AATree a->Int
level (AASingle _) = 1
level (AANode l _ _ _ ) = l

instance Measured a=>Measured (AATree a) where
 type Measure (AATree a) = Measure a
 measure (AASingle x) = measure x
 measure (AANode _ v _ _) = v

split,skew::Measured a=>AATree a->AATree a
skew (AANode l v (AANode sl _ tl tm) tr) | l == sl =
 AANode l v tl (AANode l (measure tm <> measure tr) tm tr)
skew t = t
split (AANode l v tl (AANode _ _ tm tr)) | l == level tr =
 AANode (l+1) v (AANode l (measure tl <> measure tm) tl tm) tr
split t = t

append::(Measured a,v~Measure a)=>a->AATree a->AATree a
append x (AASingle y) = AANode 2 (measure y <> measure x) (AASingle y) (AASingle x)
append x (AANode l v tl tr) = skew . split $ AANode l (v <> measure x) tl (append x tr)

instance Semigroup b=>Measured (a,b) where
 type Measure (a,b) = b
 measure = snd

newtype Size a = Size a deriving (Eq, Ord, Show)
instance Measured a=>Measured (Size a) where
 type Measure (Size a) = (Sum Int, Measure a)
 measure (Size x) = (Sum 1, measure x)

fromList::(Measured a,v~Measure a)=>[a]->AATree a
fromList [] = error "empty list"
fromList (x:xs) = foldl' (flip append) (AASingle x) xs

type RangeTree a = AATree (Size a)
rangeList::(Measured a,v~Measure a)=>[a]->RangeTree a
rangeList = fromList . fmap Size

request::Measured a=>Int->Int->RangeTree a->Measure a
request from to (AASingle (Size x))
 | from == 0 && to == 1  = measure x
 | otherwise = error $"single" <> show from <> show to
request from to (AANode _ (getSum -> sz, v) tl tr)
 | from >= sz || to <= 0 = error $ "empty request" <> show from <> show to <>show sz
 | from <= 0 && to >= sz = v
 | otherwise = res where
   szl = getSum . fst $ measure tl
   res | from >= szl = rr
       | to   <= szl = rl
       | otherwise   = rl <> rr
   rl =  request from (min to szl) tl
   rr = request (max 0 (from - szl)) (to - szl) tr

newtype Index = Index (Int, Int) deriving (Eq, Ord, Show)
instance Measured Index where
 type Measure Index = Min (Int, Int)
 measure (Index (x, y)) = Min (y, x)

makeTree::[Int]->RangeTree Index
makeTree =  rangeList.fmap Index .zip [0..].fmap fst.sortBy (comparing snd).zip [0..]
