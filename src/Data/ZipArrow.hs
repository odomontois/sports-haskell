{-# LANGUAGE Arrows                #-}
{-# LANGUAGE PartialTypeSignatures #-}

module Data.ZipArrow where

import           Control.Arrow
import           Control.Category
import           Prelude             hiding (id, (.))

newtype ZipArrow a b = ZipArr{unzipArr::[a]->[b]}

instance Category ZipArrow where
  id = ZipArr id
  ZipArr x . ZipArr y = ZipArr (x.y)

instance Arrow ZipArrow where
  arr f = ZipArr $ fmap f
  ZipArr f *** ZipArr g = ZipArr $ uncurry zip . (f *** g) . unzip

multiZip::(a->[b])->ZipArrow a b
multiZip f = ZipArr (>>= f)


test1::ZipArrow Int Int
test1  = proc x -> do
  y <- arr (+1) -< x
  z <- arr (*2) -< x
  multiZip (uncurry replicate) -< (y, z)
