{-# LANGUAGE ViewPatterns #-}
module Data.Interval(
  module Data.Ix,
  Interval(..), mkFrom, mkTo, intRange
) where

import           Data.Ix

data Interval a = Interval{ intFrom ::Maybe a, intTo :: Maybe a} deriving (Eq, Ord, Show)

mkFrom, mkTo::a->Interval a
mkFrom  x = Interval (Just x) Nothing
mkTo    x = Interval Nothing (Just x)

intRange::a->a->(Interval a, Interval a)
intRange x y = (mkFrom x, mkTo y)

_rangeError::a
_rangeError = error "range error"

instance Ix a => Ix (Interval a) where
  range (intFrom -> Just f, intTo -> Just t) =
    [Interval (Just x) (Just y)| x <- range (f,t) , y <- range (x,t)]
  range _ =_rangeError
  rangeSize (intFrom -> Just f, intTo -> Just t) =
    n * (n + 1) `quot` 2 where n = rangeSize (f, t)
  rangeSize _ = _rangeError
  index (intFrom -> Just f, intTo -> Just t) (Interval (Just x) (Just y)) =
    u + k where
      m = index (f, t) x
      u = rangeSize (f, t) * m - m * (m - 1) `quot` 2
      k = index (x, t) y
  index _ _ = _rangeError
  inRange (intFrom -> Just f, intTo -> Just t) (Interval (Just x) (Just y)) =
    inRange (f, t) x && inRange (x, t) y
  inRange _ _ = _rangeError
