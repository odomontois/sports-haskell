module LastDigit where



lastDigit :: [Integer] -> Integer
lastDigit []        = 1
lastDigit (x: rest) | isZero rest = 1
lastDigit (x: rest) = mod (mod x 10 ^ (mod4 rest + 4)) 10

mod4 []       = 1
mod4 (x:rest)   | isZero rest  = 1
                | mod x 4 == 0 = 0
                | mod x 4 == 1 = 1
                | mod x 4 == 3 = mod (3 ^ mod2 rest) 4
                | isOne rest = 2
                | otherwise  = 0
isZero (0:rest) | isZero rest = False
                | otherwise   = True
isZero _                      = False
isOne []       = True
isOne (1:_)    = True
isOne (x:rest) | isZero rest = True
isOne _        = False
mod2 [] = 1
mod2 (x:rest) | odd x = 1
              | isZero rest = 1
              | otherwise = 0
