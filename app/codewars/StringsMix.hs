module Codewars.G964.Mixin where

import           Data.Char  (isAsciiLower)
import           Data.List  (intercalate, lookup, sortBy)
import           Data.Map   (Map, fromListWith, mergeWithKey, toList)
import           Data.Maybe
import           Data.Ord   (Down (..), comparing)

mix :: String -> String -> String
mix s1 s2 = translate (freqs s1 `mergeMax` freqs s2) where
  translate = intercalate "/" . fmap code .sortBy (comparing order). filter good. toList
  order (c, (u, cnt)) = (Down cnt, idx u,  c)
  good (c, (_, cnt)) = isAsciiLower c && cnt > 1
  idx = flip lookup [(LT, '2'), (EQ, '='), (GT, '1')]
  code (c, (o, cnt)) = fromJust (idx o) : ':' : replicate cnt c

freqs:: (Num b, Ord a)=>[a]->Map a b
freqs = fromListWith (+).(`zip` repeat 1)

mergeMax:: (Ord a, Ord b)=>Map a b->Map a b->Map a (Ordering, b)
mergeMax = mergeWithKey choose (fmap $ (,) GT) (fmap $ (,) LT) where
  choose _ x y = Just (compare x y, max x y)
