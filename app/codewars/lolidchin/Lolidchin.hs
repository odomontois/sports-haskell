{-# LANGUAGE ViewPatterns #-}

module Lolidchin where

import Control.Monad
import Data.List (find, scanl', transpose)
import Data.Monoid
import Data.Maybe
import Data.Foldable

foldWhile :: (b -> Bool) -> (b -> a -> b) -> b -> [a] -> b
foldWhile p f z = last . (z :) . takeWhile p . scanl' f z

foldOnce :: [Int] -> [Int]
foldOnce ls = (++ mid) $ take k $ zipWith (+) ls $ reverse ls
  where
    k = length ls `quot` 2
    k' = (length ls + 1) `quot` 2
    mid = drop k $ take k' ls

foldList :: [Int] -> Int -> [Int]
foldList = (. (mconcat . flip replicate (Endo foldOnce))) . flip appEndo

shortestList :: [[a]] -> [a]
shortestList xss@(x : xs') =
  let l = length $ foldr (zipWith const) x xs'
      checkLength xs = length (take (l + 1) xs) == l
   in fromMaybe [] $ find checkLength xss
shortestList [] = []

eqAll :: (Foldable t, Eq a) => t a -> Bool
eqAll ta = let first = listToMaybe $ toList ta in all ((== first) . Just) ta
