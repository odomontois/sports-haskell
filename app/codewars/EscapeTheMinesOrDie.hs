{-# LANGUAGE LambdaCase, RecordWildCards, NamedFieldPuns #-}

module EscapeTheMinesOrDie where

import qualified Data.Map.Strict as Map
:r
import Data.Map.Strict (Map)
import Control.Monad.Reader
import Control.Monad
import Data.Maybe
import Data.Monoid
import Data.List

type XY = (Int, Int)
data Move =  R | L | U | D  deriving (Eq, Show, Bounded, Enum)

moves::[Move]
moves = [minBound..maxBound]

move::XY->Move->XY
move (x, y) = \case  
    U -> (x, y - 1)
    D -> (x, y + 1)
    R -> (x + 1, y)
    L -> (x - 1, y)

data Dungeon = Dungeon{target:: XY, 
                       free  :: Map XY Bool,
                       way::[Move]}

addStep::Move->Dungeon->Dungeon
addStep m d@Dungeon{way} = d{way = m:way}

markCell::XY->Dungeon->Dungeon
markCell xy d@Dungeon{free} = d{free = Map.delete xy free}

dungeon::XY->[[Bool]]->Dungeon
dungeon target m =  
    let free = Map.fromAscList $ do 
        (xs, i) <- zip m [0..]
        (x, j)  <- zip xs [0..]
        return ((i,j), x) 
    in Dungeon{way = [], ..}

good:: XY -> Dungeon -> Bool
good xy = fromMaybe False . Map.lookup xy . free

step::XY->Dungeon->Maybe [Move]
step xy = do
    ok <- good xy
    if not ok then return Nothing else do 
        t <- target
        if xy == t then Just . reader way    
        else local (markCell xy) $ do 
            descend <- forM moves $ \s -> 
                local (addStep s) $ First <$> (step $ move xy s)
            return $ getFirst $ mconcat descend

solve :: [[Bool]]->XY->XY->Maybe [Move] 
solve m b t = reverse<$> step b (dungeon t m)