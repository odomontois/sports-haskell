{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE LambdaCase    #-}
module Coroutine where

import           Control.Monad (ap, forever, (>=>))

newtype Coroutine r u d a = Coroutine { runCoroutine :: (Command r u d a -> r) -> r } deriving (Functor)

data Command r u d a =
  Done a
  | Out d (Coroutine r u d a)
  | In (u -> Coroutine r u d a) deriving Functor

_make::Command r u d a->Coroutine r u d a
_make com = Coroutine $ \k -> k com

_done::a -> Coroutine r u d a
_done a = _make $ Done a

_out::d->Coroutine r u d a->Coroutine r u d a
_out d c = _make $ Out d c

_in::(u -> Coroutine r u d a)->Coroutine r u d a
_in f = _make $ In f


-- Useful alias
apply :: Coroutine r u d a -> (Command r u d a -> r) -> r
apply = runCoroutine

instance Applicative (Coroutine r u d) where
  pure = return
  (<*>) = ap

instance Monad (Coroutine r u d) where
  return = _done
  f >>= g  = Coroutine $ \k -> runCoroutine f $ \case
                Done a -> runCoroutine (g a) k
                Out d c -> k (Out d (c >>= g))
                In f -> k (In $ f >=> g)


(>>>) :: Coroutine r u m a -> Coroutine r m d a -> Coroutine r u d a
-- p1 >>> p2 = Coroutine $ \k -> runCoroutine p1 $ \case
--               Done a -> runCoroutine p2 $ \case
--                 Done a' -> k $ Done a'
--                 Out d p2' -> k (Out d (p1 >>> p2'))
--                 In _ -> k $ Done a
--               Out m p1' -> runCoroutine p2 $ \case
--                 Done a' -> k $ Done a'
--                 Out d p2' -> k (Out d (p1 >>> p2'))
--                 In f -> runCoroutine (p1' >>> f m) k
--               In f -> k $ In $ \u -> f u >>> p2
c1 >>> c2 = Coroutine $ \k -> runCoroutine c2 $ \case
              Done a -> k $ Done a
              Out d c2' -> k (Out d (c1 >>> c2'))
              In f -> runCoroutine c1 $ \case 
                Done a -> k $ Done a
                In g -> k $ In $ \u -> g u >>> c2
                Out m c1' -> runCoroutine (c1' >>> f m) k


-- It might be useful to define the following function
-- pipe2 :: Coroutine r u m a -> (m -> Coroutine r m d a) -> Coroutine r u d a

-- Library functions

_map :: (a -> b) -> Coroutine r a b u
_map f = _in $ \a -> _out (f a) $ _map f

_id :: Coroutine r v v a
_id = _in $ \v -> _out v _id

output :: a -> Coroutine r u a ()
output v = _out v $ _done ()

input :: Coroutine r v d v
input = _in $ \v -> _done v

produce :: [a] -> Coroutine r u a ()
produce []      = _done ()
produce (x: xs) = _out x $ produce xs

consume :: Coroutine [t] u t a -> [t]
consume c = runCoroutine c $ \case
  Out t c' -> t : consume c'
  _ -> []

filterC :: (v -> Bool) -> Coroutine r v v ()
filterC p = _in $ \v -> if p v then _out v $ filterC p else filterC p

limit :: Int -> Coroutine r v v ()
limit n | n <= 0 = _done ()
        | otherwise =  _in $ \v -> _out v $ limit (n - 1)

suppress :: Int -> Coroutine r v v ()
suppress n | n <= 0    = _id
           | otherwise = _in $ \_ -> suppress (n - 1)

add :: Coroutine r Int Int ()
add = _in $ \x -> _in $ \y -> _out (x + y) add

duplicate :: Coroutine r v v ()
duplicate = _in $ \v -> _out v $ _out v $ duplicate

scanC :: b -> (a -> b -> b) -> Coroutine r a b ()
scanC b f = _out b $ _in $ \a -> scanC (f a b) f 

repeatC :: a -> Coroutine r a a ()
repeatC a = _out a $ repeatC a

-- Programs
-- 1. A program which outputs the first 5 even numbers of a stream.
-- 2. A program which produces a stream of the triangle numbers
-- 3. A program which multiplies a stream by 2
-- 4. A program which sums adjacent pairs of integers

p1, p2, p3, p4 :: Coroutine r Int Int ()

p1 = filterC even >>> limit 5
p2 = repeatC 1 >>> sums >>> sums where sums = scanC 0 (+) >>> suppress 1
p3 = _map (*2)
p4 = scanC (0, 0) (\x (_, p) -> (p + x, x)) >>> _map fst >>> suppress 2
