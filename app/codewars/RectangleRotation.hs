-- import Test.Hspec
import           Control.Monad
import           Debug.



rectangleRot :: Int -> Int -> Int
rectangleRot a' b' = length $ do
  let q2 = sqrt 2
      w  = floor $ fia (a + b) / q2
  traceShowM (w, q2)
  x <- [-w..w]
  y <- [-w..w]
  guard $ fia (x + y) <= q2 * fia a
  guard $ fia (x - y) <= q2 * fia b
  where fia = fromIntegral . abs
        a = quot a' 2
        b = quot b' 2

main = undefined

-- main = hspec $ do
--      describe "Basic Tests"  $ do
--      it "6 4" $ do
--          rectangleRot 6 4 `shouldBe` 23
--      it "30 2" $ do
--          rectangleRot 30 2 `shouldBe` 65
--      it "8 6" $ do
--          rectangleRot 8 6 `shouldBe` 49
--      it "16 20" $ do
--          rectangleRot 16 20 `shouldBe` 333
