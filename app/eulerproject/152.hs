{-# LANGUAGE FlexibleContexts #-}
import           Math.Primes
import           Control.Monad.Writer.Strict
import           Control.Applicative
import           Data.Ratio
import           Data.Semigroup
import           Control.Arrow
import           Data.IntMap.Strict            as IntMap
                                         hiding ( foldr )

lim, base, b2 :: Num a => a
lim = 80

base = 5040
b2 = base ^ 2

possibleReductions :: Integer -> [(Integer, [Integer])]
possibleReductions pm = runWriterT fltred  where
    ps = takeWhile (< lim) $ dropWhile (< pm) primes
    nums =
        mfilter (> 0) $ (numerator . sum) <$> traverse make [2 .. quot lim pm]
    r2 = recip . fromIntegral . (^ 2)
    make i = writer (r2 i, [i]) <|> pure 0
    fltred = do
        (num, ns) <- listen nums
        let mx = maximum ns
        p <- lift ps
        guard $ p * mx <= lim
        guard $ num `mod` (p ^ 2) == 0
        return p

type St = IntMap Int

walk :: Int -> St -> St
walk i | mod base i > 0 = id
       | otherwise = unionWith (+) <*> (fromListWith (+) . fmap add . toList)  where
    i2 = quot base i ^ 2
    add (k, ls) | 2 * (k + i2) > b2 = (0, 0)
                | otherwise         = (k + i2, ls)

solution = foldr walk (singleton 0 1) [2 .. lim] ! (quot b2 2)

main = print solution

