powm1::Integral n=>n->n
powm1 k = 1 - 2 *  mod k 2

formula::Integral n=>n->n->n
formula 1 i = i * (i + 1) `div` 2
formula n i = let (k,r) = divMod n 2 in case r of
  0 -> formula 1 (i + n - 1) + k * powm1 i
  _ -> formula 1 (i + n - 2) + k * powm1 (i + 1)

ans::Integer
ans = sum $ do
  let mp2,mp3::Integer
      mp2 = 27
      mp3 = 12
  p2 <- [0..mp2]
  p3 <- [0..mp3]
  let n = 2^p2 * 3 ^p3
      i = 2^(mp2 - p2) * 3 ^ (mp3 - p3)
  return $ formula n i

main::IO ()
main = print $ mod ans (10^8)
