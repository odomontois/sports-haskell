{-# LANGUAGE NamedFieldPuns #-}

import           Control.Foldable
import           Data.Foldable
import           Data.Map.Strict    (Map)
import qualified Data.Map.Strict    as Map
import           Data.Monoid
import           Math.Integers
import           Math.Primes
import           System.Environment


calcSize::Integral n=>n->n
calcSize lim = sum . fmap (quot lim)  $  gfValI singularOddFactors lim

calcSize2::Integral n=>n->n
calcSize2 lim = sum $ GenFacts lim 1 id divs oddFactors where
  divs FactStep {fsPrime = p , fsLastPrime = lp, fsVal = x}
    | p == lp = x
    | otherwise = 2 * x

quotCombs::Integral n=>n->[n]->n
quotCombs x [] = x
quotCombs x (p:ps) = quotCombs x ps + quotCombs (negate $ quot x p) ps

pcount::Integral a=>a->a
pcount n =
  let n'  = 2* n
      lim = sqr n
      gen = GenFacts lim (1, 1, []) countU divs oddFactors
      countU (x, tot, ps) =
        let o = sqr (n' - x*x)
            l = max o x
        in count l ps - count (div l 2) ps - div tot 2
      count = quotCombs
      divs FactStep{ fsVal = (x, tot, ps), fsPrime = p , fsLastPrime = lp}
        | p == lp   = (x * p, tot * p      , ps)
        | otherwise = (x * p, tot * (p - 1), p:ps)
  in sum gen - 1

main::IO ()
main = do
  args <- getArgs
  let n = case args of
        []   -> 10000::Integer
        x:_ -> read x
  print $ calcSize n
  print $ calcSize2 n
