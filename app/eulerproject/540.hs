{-# LANGUAGE NamedFieldPuns #-}

import           Math.Primes
import           System.Environment
import           Math.Integers

quotCombs::Integral n=>n->[n]->n
quotCombs x [] = x
quotCombs x (p:ps) = quotCombs x ps + quotCombs (negate $ quot x p) ps

pcount::Integral a=>a->a
pcount n =
  let n'  = 2* n
      lim = sqr n
      gen = GenFacts lim (1, 1, []) countU divs oddFactors
      countU (x, tot, ps) =
        let o = sqr (n' - x*x)
            l = max o x
        in count l ps - count (div l 2) ps - div tot 2
      count = quotCombs
      divs FactStep{ fsVal = (x, tot, ps), fsPrime = p , fsLastPrime = lp}
        | p == lp   = (x * p, tot * p      , ps)
        | otherwise = (x * p, tot * (p - 1), p:ps)
  in sum gen - 1

main::IO ()
main = do
  args <- getArgs
  let n = case args of
        []   -> 1000000::Integer
        x:_ -> read x
  print $ pcount n
