import Data.List
import qualified Data.IntSet as Set

primes::[Integer]
primes = 2: filter isPrime [3,5..]

isPrime::Integer->Bool
isPrime n = all (\p->mod n p/=0) $ takeWhile (\p->p*p<=n) primes

(+~)::Integer->Integer->Integer
(+~) = (flip mod m.). (+) where m = 2 ^ (32::Integer)

smooth5::Integer->IO Integer
smooth5 n = let
  ni =  fromInteger n
  lim  = takeWhile (<= n)
  lims p = fst . Set.split (div ni p + 1)
  pows =  (`map` ([0..]::[Integer])) . (^)
  nums = sort $ do
    i2 <- lim (pows 2)
    i23 <- lim $ map (i2 *) $ pows 3
    lim $ map (i23 * ) $ pows 5
  sums = reverse $ zip nums $ scanl1 (+~) nums
  sps = filter isPrime $ map (+1) $ dropWhile (< 5) nums
  prod ms p =  Set.union ms $ Set.map (* p) $ lims p ms
  prodInit = Set.singleton 1
  prods = map toInteger $ Set.toAscList $ foldl' prod prodInit $ map fromInteger sps
  go (acc, ss) pr = let
    ss' = dropWhile ((>n).(*pr).fst) ss
    acc' = acc +~ (pr * snd (head ss'))
    in (acc', ss')
  in do
    print (length nums)
    print (length prods)
    print (length sps)
    -- return (sum prods)
    return $ fst $ foldl' go (0, sums) prods

main :: IO ()
main = do
  p <- fmap read getLine::IO Integer
  -- let p = 100000000::Integer
  ans <- smooth5 p
  print ans
