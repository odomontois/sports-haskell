{-# LANGUAGE ExistentialQuantification, UnicodeSyntax #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE MonadComprehensions       #-}
{-# LANGUAGE MultiParamTypeClasses     #-}

module Main where
import           Control.Monad
import           Control.Monad.Trans
import           Control.Monad.Trans.Writer.Strict hiding (tell)
import           Control.Monad.Writer.Class


report::(Show b,MonadWriter [Labelled] m)=>String->b->m ()
report x y = tell [Labelled x y]

label::MonadWriter [Labelled] m=>String->m ()
label x = tell [Label x]

data Labelled =
  forall a.Show a=>Labelled String a |
  Label String

instance Show Labelled where
  show (Labelled l x) = l ++ ": " ++ show x
  show (Label l) = l

type Nums a = WriterT [Labelled] [] a

pih::Integer -> Nums ()
pih r = do
    let s = floor ( sqrt $ fromInteger  r::Double)
    i <- lift [0..s]
    j <- lift [i+1..s]
    let k = 2 * i + 1
        l = 2 * j + 1
    guard $ gcd k l == 1
    let k2 = k*k
        l2 = l*l
        c = (l2 + k2) `div` 2
        (u,v) = divMod r c
    guard $ v == 0
    label "pithagorean"
    report "a" $  k * l
    report "b" $ (l2 - k2) `div` 2
    report "c" c
    report "coef" u

-- quad::Integer -> Nums ()
-- quad r = do
--   let s = floor ( sqrt $ fromInteger  r::Double)
--   i <- lift [1..s]
--   let i2 = i*i
--   guard $ mod r i2 == 0

printWrites:: Show a=>WriterT [a] [] b->IO ()
printWrites wr = do
  let idxs::[Integer]
      idxs    = [1..]
      divline = putStrLn "----"
      cases   = idxs `zip` execWriterT wr
  divline
  forM_ cases $ \(i,writes) -> do
    putStrLn $ "case " ++ show i ++ ":"
    forM_ writes $ putStrLn . ( "| " ++). show
    divline

jab::Integer->Integer
jab n =  sum [(-1)^((d - 1) `div` 2) | d <- [1,3..n] , mod n d == 0]

main :: IO ()
main = do
  let n =  40
  printWrites $pih n
  print$ jab n
