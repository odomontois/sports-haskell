{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell       #-}

import           Control.Lens
import           Control.Monad.Reader
import           Control.Zip
import           Data.Foldable
import qualified Data.Map.Strict      as Map
import           Data.Maybe
import           Math.Integers
import           Math.Primes
import           System.Environment

data Conf = Conf {
  _cLow  :: Integer,
  _cHigh :: Integer,
  _cTot  :: Integer->Integer
}
makeLenses ''Conf

type Tot = Integer->Integer

conf::Integer->Integer->Conf
conf low high = Conf low high (m Map.!) where
  m = Map.fromList . filter window . toList $ zipComp gfVal gfTotient high
  window (x,_) =  x >= low

f::MonadReader Tot m=>Integer->Integer->m Integer
f a b = do
  tot <- ask
  let ta = tot a
      tb = tot b
      mu = fst <$> euler a b (tb - ta)
      r u = u * a + ta
  return $ maybe 0 r mu

mkTotF::Integer->(Integer->Integer->Integer, Tot)
mkTotF high =
  let tot = _cTot $ conf 1 high
  in ((flip runReader tot.). f, tot)

bruteF::Integer->Integer->Integer
bruteF m n =
  let tot x = fromIntegral . length $ filter (\y-> gcd x y == 1) [1..x-1]
      mt = tot m
      nt = tot n
  in fromMaybe 0 $ find (\x -> mod x m == mt && mod x n == nt) [1..m*n]

res::Reader Conf Integer
res = do
  Conf low high _ <-ask
  nums <- sequence $ do
      i <- [low..high-1]
      j <- [i+1..high]
      return . magnify cTot $ f i j
  return (sum nums)

solution::Integer->Integer->Integer
solution = (runReader res. ). conf

main::IO ()
main = fmap read <$> getArgs >>= \[x,y] -> print $ solution x y
