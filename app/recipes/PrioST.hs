{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TupleSections    #-}

import           Control.Monad.ST.Safe
import           Control.Monad.State.Strict
import           Data.Array.ST.Safe
import           Data.Array.Unboxed
import           Data.List                  (sort)
import           Data.STRef
import           Debug.Trace
import           System.Random

type Cost = Int

data Prio  s   = Prio (STArray s Int Cost) (STRef s Int) (STUArray s Int Int) (STUArray s Int Int)

putPrio::Prio s->Int->Int->ST s ()
putPrio (Prio _ _ arr idx) i x = do
  writeArray arr i x
  writeArray idx x i

newPrio::STArray s Int Cost->ST s (Prio s)
newPrio values = do
    bs <-  getBounds values
    let n  = rangeSize bs
    len <- newSTRef 0
    arr <- newArray_ (0, n)
    idx <- newArray  bs (-1)
    return $ Prio values len arr idx

pushPrio::Prio s->Int->ST s ()
pushPrio prio@(Prio _ ref _ _) x = do
  n <- readSTRef ref
  writeSTRef ref (n + 1)
  putPrio prio n x
  fixUp prio n

deletePrio::Prio s->Int->ST s ()
deletePrio prio@(Prio val ref arr idx) x = do
  n <- readSTRef ref
  writeSTRef ref (n - 1)
  i <- readArray idx x
  xv <- readArray val x
  w <- readArray arr (n - 1)
  wv <- readArray val w
  traceShowM (x, i, xv)
  putPrio prio i w
  case compare xv wv of
    EQ -> return ()
    LT -> fixDown prio i
    GT -> fixUp prio i

pullPrio::Prio s->ST s Int
pullPrio prio@(Prio _ ref arr _) = do
  n <- readSTRef ref
  writeSTRef ref (n - 1)
  x <- readArray arr 0
  w <- readArray arr (n - 1)
  putPrio prio 0 w
  fixDown prio 0
  return x

nullPrio::Prio s->ST s Bool
nullPrio (Prio _ ref _ _) = (== 0) <$> readSTRef ref

fixUp::Prio s->Int->ST s ()
fixUp _ 0     = return ()
fixUp prio@(Prio val _ arr _) start  = do
  x <- readArray arr start
  xv <- readArray val x
  let go 0 = return 0
      go i = do
        let p = quot (i - 1)  2
        px <- readArray arr p
        pv <- readArray val px
        if xv >= pv then return i else putPrio prio i px >> go p
  top <- go start
  unless (top == start) $ putPrio prio top x


fixDown::Prio s->Int->ST s ()
fixDown prio@(Prio val ref arr _) start = do
  size <- readSTRef ref
  x <- readArray arr start
  xv <- readArray val x
  let go i | 2 * i + 1 >= size = return i
           | 2 * i + 2 == size = do
             let c = 2 * i + 1
             cx <- readArray arr c
             cv <- readArray val cx
             if xv <= cv then return i else putPrio prio i cx >> return c
           | otherwise  = do
            let l = 2 * i + 1
                r = 2 * i + 2
            lx <- readArray arr l
            rx <- readArray arr r
            lv <- readArray val lx
            rv <- readArray val rx
            let goLeft  = putPrio prio i lx >> go l
                goRight = putPrio prio i rx >> go r
            if xv <= lv then
              if xv <= rv then return i else goRight
            else
              if lv <= rv then goLeft else goRight
  bottom <- go start
  unless(bottom == start) $ putPrio prio bottom x


main = do
  gen <- newStdGen
  let list = [100,99..1]
      srt = runST $ do
        vals <- newListArray (0,100) [0..100]
        let mkArray::ST s (STUArray s Int Int)
            mkArray = newArray_ (1, 100)
        ind <- mkArray
        let notify i x = writeArray ind x i
        prio <- newPrio vals
        forM_ list (pushPrio prio)
        forM_ [1,3..99] $ \i -> deletePrio prio i
        replicateM (length list `quot` 2) (pullPrio prio)
  print srt
  print (srt == reverse (sort list))
