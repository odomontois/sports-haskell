module IntegerSquareRoot where

sqrint::Integral a=>a->(a,a)
sqrint 0 = (0,0)
sqrint 1 = (1,0)
sqrint 2 = (1,1)
sqrint 3 = (1,2)
sqrint x  =
  let (d4, r4) = divMod x 4
      (q, r)   = sqrint d4
      r'  = 4*r + r4
      q'  = 2 * q
      r'' = r' - 2*q' - 1
  in if r'' >= 0 then (q' + 1, r'') else (q' , r')
