{-# LANGUAGE FlexibleContexts #-}
module ListArray where

import           Data.Array
import           Data.Array.ST.Safe

groupArray::Ix i=>(i, i)->[(i,e)]->Array i [e]
groupArray bs els = runSTArray $ do
    arr <- newArray bs []
    go arr els
    return arr
    where
    go _ [] = return ()
    go arr ((i, e): rest) = do
      lst <- readArray arr i
      writeArray arr i (e: lst)
      go arr rest
