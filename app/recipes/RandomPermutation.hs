{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TupleSections         #-}

import           Control.Monad.State
import           Data.Bits
import           Data.List           (partition)
import qualified Data.Map.Strict     as Map
import           System.Random

newtype BoolList = BoolList{ _getBoolList:: [Bool] } deriving (Eq, Show, Ord)

_insideBL :: ([Bool] -> [Bool]) -> BoolList -> BoolList
_insideBL = (BoolList .) . (. _getBoolList)

instance Random BoolList where
  random g = let (gl, gr) = split g
                 bs = finiteBitSize (0::Int) - 1
                 bools = (randoms gl::[Int]) >>= ((`fmap` [0..bs]) . testBit)
            in (BoolList bools, gr)
  randomR = const random

randPerm::RandomGen g=>[a]->g->([a], g)
randPerm = runState . go where
  go [] = return []
  go [x] = return [x]
  go xs = do
    (ls, rs) <- rpart xs
    lp <- go ls
    rp <- go rs
    return $ lp ++ rp
  rpart xs = do
    BoolList bl <- state random
    let (ls, rs) = partition snd $ zip xs bl
    return (fmap fst ls, fmap fst rs)

main:: IO ()
main = do
  g <- getStdGen
  let n::Num a=>a
      n = 30000
      (perms, _) = (replicateM n . state $ randPerm [1..4::Int]) `runState` g
      freqs = Map.fromListWith (+) . fmap (, 1 / n::Double) $ perms
      k = fromIntegral $ Map.size freqs
      e = sum freqs / k
      d = sum (fmap (** 2) freqs) - e ** 2
  print $ sqrt d
