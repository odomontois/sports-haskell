import           Data.Set      as Set
import           Utils.CodeJam

solution::(Show a, Num a , Eq a)=>a->Maybe a
solution 0 = Nothing
solution n = Just  (go n Set.empty) where
  full = Set.fromList "0123456789"
  repr = Set.fromList . show
  go m set =
    let set' = Set.union set (repr m)
    in if set' == full then m else go (m + n) set'

main::IO ()
main = runCases "A" pRead pWrite where
  pRead = fmap (solution . read) . hGetLine
  pWrite handle = hPutStrLn handle . maybe "INSOMNIA" show
