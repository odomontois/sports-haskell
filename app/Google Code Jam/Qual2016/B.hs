import           Data.Bool
import           Data.Map      ((!))
import qualified Data.Map      as Map
import           Data.Tuple
import           Utils.CodeJam

data Side = Happy | Blank deriving (Eq, Ord, Bounded, Enum, Show)

flips :: Side -> Side
flips Happy = Blank
flips Blank = Happy

solution :: String -> Integer
solution cs = result 1 n Happy False where
  n = length cs
  side '+' = Happy
  side '-' = Blank
  side _ = undefined
  cm = Map.fromDistinctAscList $ zip [1..] $ map side cs

  resm = Map.fromList $ do
    i <- [1..n]
    j <- [i..n]
    s <- [Happy, Blank]
    r <- [False, True]
    return ((i, j, s, r), calc i j s r)

  result i j s r =   resm ! (i,j,s,r)

  split i j s r =  do
    k <- [i..j - 1]
    let ((si, sj), (ei, ej)) = bool id swap r ((i,k),(k+1,j))
        start = result si sj (flips s) r
        end   = result ei ej (flips s) (not r)
    return $ start + end + 1

  recur i j s False = if cm ! j == s then Just $ result i (j - 1) s False else Nothing
  recur i j s True  = if cm ! i == s then Just $ result (i + 1) j s True else Nothing

  calc i j s r
    | i == j =  if s == cm ! i then 0 else 1
    | otherwise =
      let opt = minimum $ split i j s r
          rec = recur i j s r
      in  maybe opt (min opt) rec

main::IO ()
main = runCases "B" hGetLine ((.show.solution).hPutStrLn)
