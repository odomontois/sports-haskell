import qualified Data.Set as Set
import           Utils.CodeJam
import Control.Monad
import Data.Foldable

resorts :: Ord a=>[a]->Int
resorts cards = let
  order (found, missed) num = let
    found' = Set.insert num found
    missed' = missed + case Set.maxView found of
      Just (u, _) | u > num -> 1
      _                     -> 0
    in (found', missed')
  in snd $ foldl' order (Set.empty, 0) cards

main :: IO ()
main = runCases "C" pr pw where
  pr h = do
    n <- read <$> hGetLine h
    replicateM n $ hGetLine h
  pw = (. resorts) . hPrint
