import  Utils.CodeJam
import Text.Printf


g::Double
g = 9.8

theta::Double->Double->Double
theta s v = asin(min a 1) / pi * 90 where
  a = s * g / (v * v)

main :: IO ()
main = runCases "B" pRead pWrite where
  pRead handle = (map read . words) <$> hGetLine handle
  pWrite handle [v, s] = hPrintf handle "%0.10f\n" $ theta s v
  pWrite _ x = error $ "Bad input" ++ show x
