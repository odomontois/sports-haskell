{-#LANGUAGE ViewPatterns, TupleSections, ExplicitForAll#-}
module Main where

import Utils.CodeJam
import Control.Monad
import Data.Map.Strict (Map)
import Data.Set(Set)
import Data.Maybe
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.List

correct::forall a .Ord a=>Map a (Set a)->Bool
correct = process where
  clean keys m = foldl' (flip Map.delete) m keys
  process edges = case Map.minViewWithKey edges of
    Nothing -> True
    Just ((f, cs), next) -> go (Map.singleton f True) False (Set.toList cs) next
  go _ _ [] edges = process edges
  go found tag cs edges =
    let good = all (== tag) $ mapMaybe (`Map.lookup` found) cs
        cs'  = Set.toList $ Set.unions $ mapMaybe (`Map.lookup` edges) cs
        edges' = clean cs edges
        tag' = not tag
        found' = Map.union found $ Map.fromList $ map (, tag) cs
    in  good && go found' tag' cs' edges'

main :: IO ()
main = runCases "A" pRead pWrite where
  pRead handle = do
    n <- read <$> hGetLine handle
    names <- replicateM n $ hGetLine handle
    let pairs = names >>= (\(words->[x,y]) -> [(x,Set.singleton y),(y,Set.singleton x)] )
        edges = Map.fromListWith Set.union pairs
    return $ correct edges
  res True = "Yes"
  res False = "No"
  pWrite = (. res) . hPutStrLn
