{-# LANGUAGE TypeApplications, ScopedTypeVariables, KindSignatures, PolyKinds #-}

import Data.Typeable
import Text.Printf

data X

meh::forall (m:: * -> *) (a :: *).(Typeable a, Typeable m) => m a -> IO ()
meh _ = printf "a = %s, m = %s, m x = %s" 
    (show.typeRep $ Proxy @ a) 
    (show.typeRep $ Proxy @ m)
    (show.typeRep $ Proxy @ (m X))

main :: IO ()
main = meh (id :: Int -> Int)