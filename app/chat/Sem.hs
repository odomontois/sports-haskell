{-# LANGUAGE ConstraintKinds, TypeFamilies, KindSignatures #-}


import GHC.Exts (Constraint)
import Data.Monoid


newtype E a = E { eval :: a } deriving Show
newtype S a = S { showS :: String } deriving Show

class BaseSem sem where
  type Pre sem a :: Constraint
  val :: Pre sem a => a -> sem a
  add :: Pre sem a => sem a -> sem a -> sem a

-- eval (add (val 10) (add (val 20) (val 200)) :: E Int)
instance BaseSem E where
  type Pre E a = (Num a, Show a)
  val v = E v
  add x y = E $ eval x + eval y

-- showS (add (val 10) (add (val 20) (val 200)) :: S Int)
instance BaseSem S where
  type Pre S a = Show a
  val v = S $ show v
  add x y = S $ "(" <> showS x <> " + " <> showS y <> ")"

class BaseSem sem => AdvSem sem where
  mul :: Pre sem a => sem a -> sem a -> sem a

instance AdvSem E where
  mul x y = E $ eval x * eval y
