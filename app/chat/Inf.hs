import           Data.Functor.Const

data Pair a b = Pair{p1::a, p2::b} deriving (Eq, Ord, Show)

instance Functor (Pair a) where
    fmap f ~(Pair a b) = Pair a (f b)

instance Monoid a => Applicative (Pair a) where
    pure = Pair mempty
    ~(Pair x f) <*> ~(Pair y a) = Pair (x <> y) (f a)

xs :: Pair [Int] [Int]
xs = traverse (\a -> Pair [a] a) [1 ..]

main = do 
    print $ take 10 $ p1 xs
    print $ take 10 $ p2 xs
