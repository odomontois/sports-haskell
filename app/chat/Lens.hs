{-# LANGUAGE LambdaCase #-}

import Control.Lens
import Data.Char


data UUU = XX Lol | YY

newtype Lol = Lol{foo::Maybe String}

pr::Prism' UUU Lol
pr = prism' XX $ \case 
    XX y -> Just y
    _    -> Nothing

zzz::Fold UUU Int    
zzz = pr . to foo . _Just . to length



main::IO ()
main = do
    print $ YY ^? zzz
    print $ (XX $ Lol Nothing) ^? zzz
    print $ (XX $ Lol $ Just "lol") ^? zzz