{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE KindSignatures         #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE TypeOperators          #-}

module Chat.Indexes where

import           Data.Proxy

data GenIdx ix v = GenIdx ix v

infixr 5 :+:

data Indexes (ixs :: [*]) (v :: *) where
  Nil   :: Indexes '[] v
  (:+:) :: GenIdx ix v -> Indexes ixs v -> Indexes (ix ': ixs) v

class IdxFind i l b | i l -> b where find::Proxy i->Indexes l v->Maybe v
--
instance
