{-#LANGUAGE TupleSections #-}
import qualified Data.Map as Map
import Control.Monad

main = getContents >>= mapM_ print.Map.toList.Map.fromListWith (+).fmap(,1).(lines >=> words)