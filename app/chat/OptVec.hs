{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE RankNTypes #-}

module OptVec where

import Data.Kind (Type)
import GHC.TypeNats

data SubSeq :: Nat -> [Type] -> Type where
  SubNil :: SubSeq 0 '[]
  SubCons :: a -> SubSeq n l -> SubSeq (n + 1) (a :: l)
  SubSkip :: SubSeq n l -> SubSeq n (a :: l)

data TheseN l where
  SubSeq :: forall n l. 1 <=? n ~ True => SubSeq n l -> TheseN l