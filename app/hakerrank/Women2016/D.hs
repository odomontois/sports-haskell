{-# LANGUAGE ViewPatterns #-}

import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.IntMap.Strict    (IntMap)
import qualified Data.IntMap.Strict    as IntMap
import           Data.IntSet           (IntSet)
import qualified Data.IntSet           as IntSet
import           Data.List             (foldl', unfoldr)
import           Data.Maybe
import           Data.Monoid
import           Debug.Trace

_base::Int
_base = 1000

type Queue a = (IntMap a, IntMap Int)

pull::Show a=>Queue a->Maybe (Int, Int, a, Queue a)
pull (q, m) =    do
  ((c, x), q') <- IntMap.minViewWithKey q
  let (cost, dest) = divMod c _base
  return (cost, dest, x, (q', m ))

push::(Monoid a, Show a)=>Int->Int->a->Queue a->Queue a
push cost dest x (q,m) =
  let qIns   = IntMap.insert code x
      qAdd   = IntMap.insertWith mappend code x q
      code   = cost * _base + dest
      qDel c = IntMap.delete (c * _base + dest) q
      m'     = IntMap.insert dest cost m
  in case IntMap.lookup dest m of
    Nothing                  -> (qIns q, m')
    Just prev | prev <  cost -> (q, m)
              | prev == cost -> (qAdd, m)
              | otherwise    -> (qIns $ qDel prev, m')

initq::Queue a
initq = (IntMap.empty, IntMap.empty)

dijsktra::IntMap [(Int, Int, Int)]->IntMap (Int, Int)->IntSet->Int->Int->Maybe (Int, IntSet)
dijsktra wayMap roadIdx banned start end = result where
    result = do
      (best, crumbs) <- continue 0 start IntMap.empty startq
      let used = rollBack [end] IntSet.empty crumbs
      return (best, IntSet.fromList used)
    startq = push 0 start [] initq
    rollBack [] _ _ = []
    rollBack (x: xs) seen crumbs = edges <> rollBack (vs <> xs) seen' crumbs where
      seen' = IntSet.insert x seen
      edges = fromMaybe [] $ IntMap.lookup x crumbs
      vs = filter (not. flip IntSet.member seen) $ fmap (back x) edges
    back v edge = let (f, t) = roadIdx IntMap.! edge in if f == v then t else f
    go (pull -> Just (cost, dest, iss, q)) paths
      | dest == end = Just (cost, IntMap.insert dest iss paths)
      | otherwise = continue cost dest paths' q
        where paths' = IntMap.insert dest iss paths
    go _ _ = Nothing
    continue cost dest paths queue = go q' paths where
      q'  = foldl' entry queue destRoads
      destRoads = fromMaybe [] $ IntMap.lookup dest wayMap
      entry q (to, price, j)
        | IntSet.member j banned = q
        | otherwise = push (price + cost) to [j] q

bestN::Int->Int->[(Int, Int, Int, Int)]->Int->Maybe (Int, IntSet)
bestN from to roads = go IntSet.empty where
  wayMap = IntMap.fromListWith (<>) $ roads >>= mkWays
  mkWays (f, t, cost, i) = [(f, [(t, cost, i)]), (t, [(f, cost, i)])]
  roadIdx = IntMap.fromList $ fmap (\(f,t,_,i) -> (i,(f,t))) roads
  go banned n = do
    result <- dijsktra wayMap roadIdx banned from to
    if n == 1 then return result else
      let banned'= IntSet.union banned $ snd result
      in go banned' (n - 1)

main:: IO ()
main = do
  let getNums = fmap (unfoldr readOne) BS.getLine
      readOne s = do
        (x , s') <- BS.readInt s
        return (x, BS.drop 1 s')
      road (i, [x,y,l]) = (x, y, l, i)
      road _ = undefined
      idxs   = [0..]::[Int]
  t <- fmap read getLine
  replicateM_ t $ do
    [_n,m] <- getNums
    [b,e] <- getNums
    roads <- (fmap road . zip idxs) `fmap` replicateM m getNums
    print . maybe (-1) fst $ bestN b e roads 3
