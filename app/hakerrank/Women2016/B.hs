import           Control.Arrow
import           Control.Monad
import           Data.List
main = fmap read getLine>>=flip replicateM_(getLine>>=putStrLn.fmap snd.sort.uncurry (++).second(uncurry zip.second reverse.unzip).partition((`elem`"aeiou").snd).zip[1..])
