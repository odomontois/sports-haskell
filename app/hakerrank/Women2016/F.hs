{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies          #-}

import           Control.Monad
import           Control.Monad.ST
import           Data.Array.IArray
import           Data.Array.MArray
import           Data.Array.ST
import           Data.Array.Unboxed
import           Data.List          (sortBy)
import           Data.Ord
import           Data.Semigroup

class Semigroup (Measure a)=>Measured a where
  type Measure a
  measure::a->Measure a

data RangeTree a where
   RSingle::a->RangeTree a
   RNode::Measured a=>{-#UNPACK#-} !Int->Measure a->RangeTree a->RangeTree a->RangeTree a

data DisplayTree a where
  DisplayTree::(Measured a, Show a,Show (Measure a))=>RangeTree a->DisplayTree a

instance Show (DisplayTree a) where
  showsPrec _ (DisplayTree t)  = pref "" "  " t where
     pref s _ (RSingle x) = showString (s ++ "+:") . shows x
     pref s u (RNode l v tl tr) =
        showString (s ++ "+-[").shows l.showString "]".shows v.showString "\n".
          pref (s ++ u) "| " tl. showString "\n".
          pref (s ++ u) "  " tr

size::RangeTree a->Int
size (RSingle _) = 1
size (RNode l _ _ _ ) = l

instance Measured a=>Measured (RangeTree a) where
  type Measure (RangeTree a) = Measure a
  measure (RSingle x) = measure x
  measure (RNode _ v _ _) = v

instance Semigroup b=>Measured (a,b) where
  type Measure (a,b) = b
  measure = snd

rangeList::Measured a=>[a]->RangeTree a
rangeList = go . fmap RSingle where
  go [] = error "empty range list"
  go [x] = x
  go xs = go (collect xs)
  collect (x:y:xs) = RNode (size x + size y) (measure x <> measure y) x y : collect xs
  collect xs = xs

request::Measured a=>Int->Int->RangeTree a->Measure a
request from to (RSingle x)
  | from == 0 && to == 1  = measure x
  | otherwise = error $"single" ++ show from ++ show to
request from to (RNode sz v tl tr)
  | from >= sz || to <= 0 = error $ "empty request" ++ show from ++ show to ++show sz
  | from <= 0 && to >= sz = v
  | otherwise = res where
    szl = size tl
    res | from >= szl = rr
        | to   <= szl = rl
        | otherwise   = rl <> rr
    rl = request from (min to szl) tl
    rr = request (max 0 (from - szl)) (to - szl) tr

instance Ord a=>Measured (Min a) where
  type Measure (Min a) = Min a
  measure x = x

base::Int
base = 1000000007

solution::[Int]->[Int]
solution nums = elems $ runSTUArray result where
  idxlst =  fmap snd. sortBy (comparing fst) $ zip nums [0..]
  idxmap::UArray Int Int
  idxmap = array (0, n -1 ) $ zip idxlst [0..]
  tree = rangeList $ fmap Min idxlst
  n = length nums
  result::ST s (STUArray s Int Int)
  result = do
    arr <- newArray_ (0, n - 1)
    go arr 1 0 n
    return arr
  go::STUArray s Int Int->Int->Int->Int->ST s ()
  go arr x from to | from >= to = return ()
                   | otherwise  =  do
    let Min idx = request from to tree
        pos     = idxmap ! idx
        l   = (x + x) `mod` base
        r   = (x + x + 1) `mod` base
    writeArray arr idx x
    go arr l from pos
    go arr r (pos + 1) to

main::IO ()
main = do
  void getLine
  ns <- fmap (fmap read.words) getLine
  putStrLn . unwords . fmap show $ solution ns
