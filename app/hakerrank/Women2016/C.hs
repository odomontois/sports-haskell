import           Control.Applicative
import           Control.Monad
import           Data.List
import qualified Data.Map            as Map
import           Data.Ord
import qualified Data.Set            as Set
main:: IO ()
main = do
  n <- fmap read getLine
  ls <- replicateM n getLine
  let ss   = sortBy (comparing Set.findMax). Map.elems . Map.fromListWith Set.union $ fmap (((,) . sort) <*> Set.singleton) ls
      l    = maximum $ fmap Set.size ss
  print $ length ss
  mapM_ putStrLn . fmap (unwords.Set.toDescList) $ filter ((==l) . Set.size) ss
