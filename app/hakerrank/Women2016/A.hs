import           Control.Arrow
import           Data.Bool
main = getLine >> getLine >>= putStrLn. uncurry (++) . ( ( bool "No " "Yes ". (==0) . ( `mod` 2))  &&&  show ) . sum . fmap read . words
