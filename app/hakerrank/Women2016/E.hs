{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}

import           Control.Monad
import           Data.Array
import           Data.Array.MArray
import           Data.Array.ST

type Occurs = [(Char, [Int])]

getOccs::String->Occurs
getOccs str = reverse . filter (not.null.snd) $ assocs $ runSTArray $ do
  arr <- newArray ('a', 'z') []
  forM_ (zip [0..] str) $ \(i, c) -> do
    lst <- readArray arr c
    writeArray arr c (i: lst)
  forM_ ['a'..'z'] $ \c -> do
    lst <- readArray arr c
    writeArray arr c (reverse lst)
  return arr

cutOptim::String->Int->String
cutOptim str = go 0 [] occs_ where
  occs_ = getOccs str
  mlength = length str
  go pos stack occs m
    | pos + m >= mlength = ""
    | otherwise          = up pos stack occs m
  down pos stack ((c, occ: rest):next)  m
    | occ < pos      = down pos stack  (nextOccs c rest next) m
    | occ - pos <= m = let cut = occ - pos in c: go (pos + 1 + cut) stack (nextOccs c rest next) (m - cut)
    | otherwise      = down pos (pushStack c occ rest stack) next m
  down pos stack occs m =":out of chars " ++ show (pos, stack,occs,m )
  nextOccs c rest next | null rest = next
                       | otherwise = (c, rest): next

  pushStack c occ rest [] = [(c, occ, occ:rest)]
  pushStack c occ rest stack@((_, prev, _):_) = (c, min occ prev, occ:rest):stack
  up pos [] occs m = down pos [] occs m
  up pos stack@((c, mocc, o):next) occs m
    | mocc - pos <= m = up pos next ((c, o):occs) m
    | otherwise       = down pos stack occs m

main::IO ()
main = do
  n <- readLn
  replicateM_ n $ do
    [str, ms] <- fmap words getLine
    let m = read ms
    putStrLn $ cutOptim str m
