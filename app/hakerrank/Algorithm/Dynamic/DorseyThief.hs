module Main where

import           Control.Monad
import qualified Data.IntMap   as IntMap
import           Data.List     (foldl')

solution::Int->[(Int, Int)]->Maybe Int
solution count = IntMap.lookup count . foldl' addAll (IntMap.singleton 0 0) where
  addAll vals new = IntMap.unionWith max vals added where
    added = fst . IntMap.split (count + 1) . IntMap.fromList $ mapped
    mapped = map (add new) $ IntMap.toList vals
  add (v', a') (a,v)  = (a + a', v + v')

main :: IO ()
main = do
  let pair [x, y] = (x,y)
      pair _      = undefined
  let getPair = fmap (pair.map read.words) getLine
  (n, x) <- getPair
  pass <- replicateM n getPair
  maybe (putStrLn "Got caught!") print (solution x pass)
