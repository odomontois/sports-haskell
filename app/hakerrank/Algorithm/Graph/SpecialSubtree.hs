{-# LANGUAGE ConstraintKinds       #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TupleSections         #-}
module Main where

import           Control.Monad.State.Strict
import           Data.IntMap                (IntMap, (!))
import qualified Data.IntMap                as IntMap
import           Data.List                  (sortBy)
import           Data.Ord                   (comparing)

type DisjointSets = IntMap (Either Int Int)

type DM m = MonadState DisjointSets m

initial::Int->DisjointSets
initial n = IntMap.fromAscList $ fmap (, Right 1) [1..n]

group::DM m=>Int->m (Int, Int)
group num = do
  qual <- gets (! num)
  case qual of
    Right size -> return (num, size)
    Left parent -> do
      (g,size) <- group parent
      if g == parent then return (g, size) else do
        modify . IntMap.insert num $ Left g
        return (g, size)

assign::DM m=>Int->Int->m()
assign i j = do
  (gi,si) <- group i
  (gj,sj) <- group j
  unless (gi == gj) $ do
    let (small, large) = if si < sj then (gi, gj) else (gj, gi)
    modify . IntMap.insert small $ Left large
    modify . IntMap.insert large . Right $ si + sj

consider::DM m=>Int->Int->m Bool
consider i j = do
  (gi, _) <- group i
  (gj, _) <- group j
  if gi == gj then return False else do
    assign i j
    return True

main :: IO ()
main = do
  let getNums = fmap (fmap read.words) getLine
  let edge [i,j,s] = ((i,j),s)
      edge _       = undefined
  [n,m] <- getNums
  edges <- fmap edge <$> replicateM m getNums
  void getLine
  let frame = filterM (uncurry consider.fst) $ sortBy (comparing snd) edges
  let frameCost = sum. fmap snd . evalState frame $ initial n
  print frameCost
