import qualified Data.ByteString.Char8         as BS
import qualified Data.IntMap.Strict            as Map
import qualified Data.IntSet                   as Set
import           Control.Monad

getInts :: IO [Int]
getInts = go <$> BS.getLine
  where
    go s = case BS.readInt s of
        Nothing      -> []
        Just (x, s') -> x : go (BS.drop 1 s')

desc = Set.toDescList . Set.fromList
withIndex = Map.fromList . (`zip` [2 :: Int ..])
place i = maybe 1 snd . Map.lookupGT i

main = do
    getLine
    scores <- withIndex . desc <$> getInts
    getLine
    alice <- getInts
    forM_ alice $ \score -> print $ place score scores