import qualified Data.List                     as List
import qualified Data.ByteString.Char8         as BS
import           Control.Monad
import           Debug.Trace

getInts :: IO [Int]
getInts = go <$> BS.getLine
  where
    go s = case BS.readInt s of
        Nothing      -> []
        Just (x, s') -> x : go (BS.drop 1 s')

dist = ((sum . fmap abs) .) . zipWith (-)
magics =
    [ [2, 9, 4, 7, 5, 3, 6, 1, 8]
    , [2, 7, 6, 9, 5, 1, 4, 3, 8]
    , [8, 3, 4, 1, 5, 9, 6, 7, 2]
    , [8, 1, 6, 3, 5, 7, 4, 9, 2]
    , [4, 9, 2, 3, 5, 7, 8, 1, 6]
    , [4, 3, 8, 9, 5, 1, 2, 7, 6]
    , [6, 7, 2, 1, 5, 9, 8, 3, 4]
    , [6, 1, 8, 7, 5, 3, 2, 9, 4]
    ]

main = do
    xs <- join <$> replicateM 3 getInts
    print $ minimum $ fmap (dist xs) magics
