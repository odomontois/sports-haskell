import qualified Data.ByteString.Char8         as BS
import qualified Data.Vector.Unboxed           as UV
import qualified Data.Vector.Unboxed.Mutable   as UVM
import           Control.Monad

getInts :: IO [Int]
getInts = go <$> BS.getLine
  where
    go s = case BS.readInt s of
        Nothing      -> []
        Just (x, s') -> x : go (BS.drop 1 s')

solution :: [Int] -> Int -> Int
solution xs k = 
    let cs = UV.create $ do
            v <- UVM.new k
            forM_ xs $ \x -> do
                let i = mod x k
                u <- UVM.read v i
                UVM.write v i (u + 1 :: Int)
            return v
        best i = max (cs UV.! i) (cs UV.! (k - i))
        mid    = if mod k 2 == 0 && cs UV.! quot k 2 > 0 then 1 else 0
        zero   = min 1 $ cs UV.! 0
    in  zero + mid + sum (fmap best [1 .. quot (k - 1) 2])

main = do
    [n, k] <- getInts
    xs     <- getInts
    print $ solution xs k