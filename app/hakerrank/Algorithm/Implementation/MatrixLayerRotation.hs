import           Control.Monad
import           Data.List                     as List
import           Text.Printf
import qualified Data.Vector.Unboxed           as UVec
import qualified Data.Vector                   as Vec
import qualified Data.ByteString.Char8         as BS

data Side = U | R | D| L deriving (Eq, Ord, Show)
data Board a = Board !a !a
data Point a = Point !a !a deriving Show
data Polar a = Polar !a !a !a

polar :: Integral a => Board a -> Point a -> Polar a
polar (Board m n) (Point x y) =
    let x'        = m - 1 - x
        y'        = n - 1 - y
        (t, side) = minimum [(y, U), (y', D), (x', R), (x, L)]
        idx       = case side of
            U -> x - t
            R -> m - 3 * t + y - 1
            D -> m + n - 5 * t + x' - 2
            L -> 2 * m + n - 7 * t + y' - 3
        size | m == 2 * t + 1 = n - 2 * t
             | n == 2 * t + 1 = m - 2 * t
             | otherwise      = m * 2 + n * 2 - 4 * (2 * t + 1)
    in  Polar t idx size

unpolar :: Integral a => Board a -> Polar a -> Point a
unpolar (Board m n) (Polar t i q)
    | i < m - 2 * t             = Point (i + t) t
    | i < m + n - 4 * t - 1     = Point (m - 1 - t) (i - m + 3 * t + 1)
    | i < 2 * m + n - 6 * t - 2 = Point (2 * m + n - 5 * t - i - 3) (n - 1 - t)
    | otherwise                 = Point t (2 * m + 2 * n - i - 7 * t - 4)

rotate :: Integral a => Board a -> a -> Point a -> Point a
rotate b k p =
    let Polar t i q = polar b p
        i'          = mod (i + k) q
    in  unpolar b $ Polar t i' q

getVector :: IO (UVec.Vector Int)
getVector = UVec.unfoldr (BS.readInt . BS.dropWhile (== ' ')) <$> BS.getLine

main = do
    [m, n, r] <- UVec.toList <$> getVector
    nums      <- Vec.replicateM m getVector
    let b = Board n m
    forM_ [0 .. m - 1] $ \j -> do
        forM_ [0 .. n - 1] $ \i ->
            let Point x y = rotate b r $ Point i j
                num       = nums Vec.! y UVec.! x
            in  printf "%s " $ show num
        putStrLn ""