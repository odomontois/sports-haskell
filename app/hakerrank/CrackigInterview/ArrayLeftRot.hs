import qualified Data.ByteString.Char8 as BS

getInts::IO [Int]
getInts = go <$> BS.getLine where
  go s = case  BS.readInt s of
      Nothing      -> []
      Just (x, s') -> x : go (BS.drop 1 s')

main = do
    [_, n] <- getInts
    xs <- getInts
    let ys = drop n xs ++ take n xs
    putStrLn $ unwords $ fmap show ys