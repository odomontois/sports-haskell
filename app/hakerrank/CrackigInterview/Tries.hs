{-# LANGUAGE RecordWildCards #-}
import Prelude hiding (read)
import qualified Prelude 
import Data.Vector.Mutable
import Data.IORef
import Control.Monad

type Elems = IOVector (Maybe Node)
data Node = Node{
    nodeCount :: IORef Int,
    nodeElems :: Elems
}

newNode::IO Node 
newNode = do
    children <- new 2
    set children Nothing 
    cnt <- newIORef 0
    return $ Node cnt children

nodeElem::Char->Elems->IO Node
nodeElem c elems = do
    me <- read elems idx
    maybe ini return me where
        idx = fromEnum c - fromEnum 'a'
        ini = do
            node <- newNode
            write elems idx $ Just node
            return node                       

putName::String->Node->IO ()
putName []  Node{..} = modifyIORef' nodeCount (+1)
putName (c:str) Node{..} = do    
    modifyIORef' nodeCount (+1)
    e <- nodeElem c nodeElems
    putName str e

partialMatch::String->Node->IO Int
partialMatch [] Node {..} = readIORef nodeCount
partialMatch (c:str) Node {..} = nodeElem c nodeElems >>=partialMatch str           


main :: IO ()
main = do
    q <- Prelude.read <$> getLine
    tree <- newNode
    replicateM_ q $ do
        [cmd, word] <- words <$> getLine
        case cmd of 
            "add"  -> putName word tree
            "find" -> partialMatch word tree >>= print