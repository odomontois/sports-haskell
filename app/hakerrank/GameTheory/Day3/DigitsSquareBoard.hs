
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables   #-}

import           Control.Applicative
import           Control.Arrow
import           Control.Monad
import           Control.Monad.ST
import           Data.Array.IArray
import           Data.Array.MArray
import           Data.Bits
import           Data.Bool
import qualified Data.ByteString.Char8 as BS
import           Data.List             (sort, unfoldr)
import           Data.Semigroup
import qualified Data.Set              as Set
import           Data.Typeable

isPrime::Int->Bool
isPrime 2 = True
isPrime 3 = True
isPrime 5 = True
isPrime 7 = True
isPrime _ = False

arrr::(Ix i)=>(i,i)->[(i,a)]->Array i a
arrr = array

type Fix x m = (x->x)->m x

memo :: forall a i e s.(Ix i, MArray a e (ST s))=>Proxy a->(i,i)->[i]->Fix (i -> ST s e) (ST s)
memo _proxy xr xs f = do
  arr <- newArray_ xr::ST s (a i e)
  let result = readArray arr
  mapM_ (f result) xs
  return result



grundy :: [Int] -> Int
grundy = go 0 . sort where
  go x [] = x
  go x (y:ys) | x < y = x
              | x == y = go (x + 1) ys
              | otherwise = go x ys

game::[[Int]]->Bool
game xss = result (1,n, 1,n) /= 0 where
  n = length xss
  idx i = fmap fst . filter (isPrime.snd) . zipWith (\j x-> ((i,j), x)) [1..]
  primeNums = Set.fromList $ uncurry idx =<< zip [1..] xss
  numPrime = curry (`Set.member` primeNums)
  segms = ((1, 1, 1, 1), (n, n, n, n))
  onlyPrime = memo segms go where
    go recur (a,b,c,d)
      | a == b && c == d = numPrime b d
      | a == b           = numPrime b d       && recur (b, b, c, d - 1)
      | otherwise        = recur (b, b, c, d) && recur (a, b - 1, c, d)
  result = memo segms go where
    go recur (a, b, c, d)
      | onlyPrime (a,b, c,d) = 0
      | otherwise = grundy (vs <> hs) where
        hs = [recur (a, t, c, d) `xor` recur (t + 1, b, c, d)| t <- [a..b-1]]
        vs = [recur (a, b, c, t) `xor` recur (a, b, t + 1, d)| t <- [c..d-1]]

getInts::IO [Int]
getInts = fmap (unfoldr readOne) BS.getLine where
  readOne s = do
    (x , s') <- BS.readInt s
    return (x, BS.drop 1 s')

getInt::IO Int
getInt = do
  line <- BS.getLine
  let Just (x, _ ) = BS.readInt line
  return x

testCase::IO ()
testCase = do
  n <- getInt
  xss <- replicateM n getInts
  putStrLn . bool "Second" "First" $ game xss

main:: IO ()
main = getInt >>= flip replicateM_ testCase
