import           Control.Applicative
import           Control.Monad
import           Data.Bits
import           Data.Bool
import qualified Data.ByteString.Char8 as BS
import           Data.List             (find)
import qualified Data.Map              as Map
import           Data.Maybe

(&&&&) :: (a -> b -> c) -> (a -> b -> d) -> a ->b -> (c, d)
(f &&&& g) x y = (f x y, g x y)

game :: Int -> Int -> Int
game = curry (m Map.!)  where
  m = Map.fromList $ ((,) &&&& result) <$> [1..15] <*> [1..15]
  ind x y  | x < 1 || y < 1 || x > 15 || y > 15  = Nothing
           | otherwise                           =  Map.lookup (x,y) m
  grundy xs = fromJust $ find (not . flip elem (catMaybes xs)) [0..]
  result x y = grundy [
    ind (x + 1) (y - 2),
    ind (x - 2) (y + 1),
    ind (x - 1) (y - 2),
    ind (x - 2) (y - 1)
    ]

readInt::BS.ByteString->(Int, BS.ByteString)
readInt = fromJust . BS.readInt

readPair::IO (Int, Int)
readPair = do
  line <- BS.getLine
  let line' = BS.dropWhile (== ' ') rl
      (x , rl) = readInt line
      (y, _ )  = readInt line'
  return (x, y)

readOne::IO Int
readOne = do
  line <- BS.getLine
  let  (x , _) = readInt line
  return x

testCase:: IO ()
testCase = do
  n <- readOne
  xs <- replicateM n readPair
  let  nums = fmap (uncurry game) xs
       res = foldr xor 0 nums /= 0
  putStrLn $ bool "Second" "First" res

main :: IO ()
main = readOne >>= flip replicateM_ testCase
