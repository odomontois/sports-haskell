import           Control.Monad
main = readLn >>= flip replicateM (fmap (fmap read.words) getLine >>= \[n,m] -> print $ if mod n 2 == 0 || m == 1 then 2 else 1)
