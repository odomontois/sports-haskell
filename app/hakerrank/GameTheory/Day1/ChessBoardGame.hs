import           Control.Applicative
import           Control.Monad
import           Data.Bool
import qualified Data.Map            as Map

(&&&&) :: (a -> b -> c) -> (a -> b -> d) -> a ->b -> (c, d)
(f &&&& g) x y = (f x y, g x y)

game :: Integer -> Integer -> Bool
game = curry (m Map.!)  where
  m = Map.fromList $ ((,) &&&& result) <$> [1..15] <*> [1..15]
  ind x y  = x < 1 || y < 1 || x > 15 || y > 15 || m Map.! (x,y)
  result x y = not $
    ind (x + 1) (y - 2) &&
    ind (x - 2) (y + 1) &&
    ind (x - 1) (y - 2) &&
    ind (x - 2) (y - 1)

main :: IO ()
main = readLn >>= flip replicateM_ (fmap (fmap read.words) getLine >>= \[x,y] -> putStrLn $ bool "Second" "First" $ game x y)
