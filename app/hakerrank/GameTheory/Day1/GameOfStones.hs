import           Control.Arrow
import           Control.Monad
import           Data.Bool
import qualified Data.IntMap   as IntMap

game = (m IntMap.!)  where
  m = IntMap.fromList $ fmap (id &&& result) [0..100]
  ind n  = n < 0 || m IntMap.! n
  result n = not (ind (n - 2) && ind (n - 3) && ind (n - 5))

main = readLn >>= flip replicateM (readLn >>= putStrLn.bool "Second" "First".game)
