import           Control.Arrow
import           Control.Monad
import           Data.Bits
import           Data.Bool
import qualified Data.IntMap   as IntMap
import           Data.List     (find)
import           Data.Maybe

primes :: Integral n=>[n]
primes = 2:ps  where
  ps = 3:filter isPrime [5,7..]
  isPrime n = all (\p -> mod n p /= 0) $ takeWhile (\p->p*p<=n) ps

gameDivs :: Int->Int
gameDivs = calc where
  limit = 1000000
  m = IntMap.fromList $ fmap (id &&& go) [1..limit]
  go 1 = 0
  go n = let pLast = fromJust $ find (\p -> mod n p == 0) primes
             kPrev = n `quot` pLast
        in calc kPrev + 1
  calc n = m IntMap.! n

testCase = do
    void getLine
    xs <- fmap (fmap (gameDivs. read).words) getLine :: IO [Int]
    let res = foldl1 xor xs /= 0
    print $ bool 2 1 res
main = readLn >>= flip replicateM_ testCase
