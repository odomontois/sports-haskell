import           Control.Monad
import           Data.Bits
import           Data.Bool

testCase = do
    void getLine
    xs <- fmap (fmap read.words) getLine :: IO [Int]
    let res = foldl1 xor xs /= 0
    putStrLn $ bool "Second" "First" res
main = readLn >>= flip replicateM_ testCase
