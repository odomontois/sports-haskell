import           Control.Monad
import           Data.Bits
import           Data.Bool

testCase = do
    void getLine
    xs <- fmap (fmap read.words) getLine :: IO [Int]
    let counts = fmap snd.filter (odd.fst) $ zip xs [(0::Int)..]
        res    = foldr xor 0 counts /= 0
    putStrLn $ bool "Second" "First" res
main = readLn >>= flip replicateM_ testCase
