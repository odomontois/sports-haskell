module Main where

import qualified Data.ByteString.Char8 as BS
import           Data.Char
import           Data.List             (find)
import           Data.Maybe

getInt::IO Int
getInt = do
  line <- BS.getLine
  let Just (x, _ ) = BS.readInt line
  return x

isLucky::Int->Bool
isLucky x = let [a,b,c,d,e,f] = map (\cc -> ord cc - ord '0') $ show x in a+b+c == d+e+f

main :: IO ()
main = do
  x <- getInt
  print $ fromJust $ find isLucky [x+1..]
