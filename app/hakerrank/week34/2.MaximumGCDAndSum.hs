{-# LANGUAGE TypeFamilies #-}
import           Control.Monad
import qualified Data.ByteString.Char8       as BS
import           Data.Vector.Unboxed.Mutable
import           Prelude                     hiding (read)

main :: IO ()
main = do
  void BS.getLine
  al <- getInts
  bl <- getInts
  let msize = maximum (al ++ bl)
      getVec xs = do
        xv <- new (msize + 1)
        forM_ xs $ \x -> write xv x True
        return xv
  as <- getVec al
  bs <- getVec bl
  rs <- new (msize + 1)
  set rs (-1, -1)
  let check vec x = do
        has <- read vec x
        return $ if has then x else -1
      maxify (a, b) i = do
        (x, y) <- read rs i
        return (max a x, max b y)
      process u = do
        markA <- check as u
        markB <- check bs u
        let ini = (markA, markB)
            cs  = takeWhile (<=msize) . fmap (*u) $ primes
        res <- foldM maxify ini cs
        write rs u res
      see m i = do
        (x, y) <- read rs i
        return $ if x == -1 || y == -1 then m else max m (i, x + y)
  mapM_ process [msize, msize - 1..1]
  (_mi, mv) <- foldM see (-1, -1) [1..msize]
  print mv


primes::[Int]
primes = 2:3:filter isPrime [5,7..] where isPrime n = all (\p->mod n p/=0) $ takeWhile (\p->p*p<=n) primes

getInts::IO [Int]
getInts = go <$> BS.getLine where
  go s = case  BS.readInt s of
      Nothing      -> []
      Just (x, s') -> x : go (BS.drop 1 s')
