{-# LANGUAGE TupleSections #-}
import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import qualified Data.HashTable.IO     as H
import           Data.IntMap.Strict    (IntMap)
import qualified Data.IntMap.Strict    as IM
import           Data.Maybe
import           Data.Tuple

occurs::[Int]->IntMap [Int]
occurs xs = IM.fromListWith (++) $ zip xs $ fmap return [0..]

calcEqs::[Int]->[Int]->Int->Int
calcEqs = go 0 $ IM.empty where
   add m c k = IM.alter (Just. (+k).fromMaybe 0) c m
   go c m (x:xs) (y:ys) n | x > y     = go (c + 1) (add m c (n-x)) xs (y:ys) x
                          | otherwise = go (c - 1) (add m c (n-y)) (x:xs) ys y
   go c m (x:xs) [] n = go (c + 1) (add m c (n-x)) xs [] x
   go c m [] (y:ys) n = go (c - 1) (add m c (n-y)) [] ys y
   go c m [] []     n = sum $ c2 <$> IM.elems (add m c (n + 1))
   c2 x = x * (x - 1) `quot` 2

getInts::IO [Int]
getInts = go <$> BS.getLine where
  go s = case  BS.readInt s of
      Nothing      -> []
      Just (x, s') -> x : go (BS.drop 1 s')

getInt2::IO (Int,Int)
getInt2 = do
  line <- BS.getLine
  let Just (x, l') = BS.readInt line
      Just (y, _)  = BS.readInt $ BS.drop 1 l'
  return (x,y)

type Memo = H.BasicHashTable (Int, Int) Int
main :: IO ()
main = do
  memo <- H.new :: IO Memo
  (n, q) <- getInt2
  us <- getInts
  let ocs = occurs us
      oc x = maybe (-1 ,[]) (x,) $ IM.lookup x ocs
      ocp x y = let (xi,xs) = oc x
                    (yi,ys) = oc y
                    p = ((xi, xs), (yi, ys))
                in if xi < yi then p else swap p
      calc (xi,xs) (yi,ys) = do
        let res = calcEqs xs ys n
        H.insert memo (xi, yi) res
        return res
      query x y = do
        let ((xi, xs), (yi,ys)) = ocp x y
        saved <- H.lookup memo (x, y)
        res <- maybe (calc (xi, xs) (yi, ys)) return saved
        print res
  replicateM_ q $ do
    (x, y) <- getInt2
    query x y
