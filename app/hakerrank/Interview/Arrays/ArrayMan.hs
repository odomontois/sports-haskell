import qualified Data.ByteString.Char8 as BS
import Data.List (sort)
import Control.Monad 

getInts::IO [Int]
getInts = go <$> BS.getLine where
  go s = case  BS.readInt s of
      Nothing      -> []
      Just (x, s') -> x : go (BS.drop 1 s')

main = do
    [m,n] <- getInts
    ns <- replicateM n $ getInts
    print $ maximum $ scanl (+) 0 $ fmap snd $ sort $ do 
        [a, b, p] <- ns
        [(a, p), (b + 1, -p)]