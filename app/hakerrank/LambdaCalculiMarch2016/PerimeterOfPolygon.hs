module Main where
import           Control.Monad
main :: IO ()
main = do
  n <- fmap read getLine
  pairs <- replicateM n $ fmap (map read . words) getLine :: IO [[Double]]
  let dist [x1,y1] [x2, y2] = sqrt ( (x1 - x2)**2  + (y1 - y2)**2)
      dist _ _ = undefined
  print $ sum $ zipWith dist pairs $ tail pairs ++ [head pairs]
