import           Control.Monad
import           Data.Bool
import qualified Data.IntMap.Strict as IntMap
import           Data.List

main :: IO [()]
main = do
  t <- fmap read getLine
  replicateM t $ do
    n <- fmap read getLine
    pairs <- replicateM n $ fmap (map read.words) getLine
    let isFunc = and . snd $ mapAccumL unique IntMap.empty pairs
        unique seen [x, y] = (
          IntMap.insert x y seen,
          maybe True (== y) $ IntMap.lookup x seen )
        unique _ _ = undefined
    putStrLn $ bool "NO" "YES" isFunc
