module Main where
import           Control.Monad
import           Data.Bool
import           Data.List     (genericLength, sortBy)
import           Data.Ord      (comparing)
main :: IO ()
main = do
  n <- fmap read getLine
  let pair [x,y] = (x,y)
      pair _ = undefined
  pairs <- replicateM n $ fmap (pair . map read . words) getLine :: IO [(Double, Double)]
  let l = genericLength  pairs
      avg f = sum (map f pairs) / l
      x0 = avg fst
      y0 = avg snd
      side (x, y) = let s = signum (x - x0) in ( s, atan ((y - y0) / (x - x0)))
      opairs = sortBy (comparing side) pairs
      cross (x1, y1) (x2, y2) (x3, y3)  =
        (x1 - x2) * (y3 - y2) - (x3 - x2) * (y1 - y2)
      shift xs = tail xs ++ [head xs]
      crosses = zipWith3 cross opairs (shift opairs) ( shift $ shift opairs)
      signs = map signum crosses
      conv = maximum signs - minimum signs < 2
  forM_ opairs $ putStrLn . (++ ","). show
  print crosses
  putStrLn $ bool "YES" "NO" conv
