import           Control.Monad
import           Data.IntMap                (IntMap, (!))
import qualified Data.IntMap                as IntMap
import           Data.List                  (mapAccumL)
import           Data.Maybe
import           Data.Set                   (Set)
import qualified Data.Set                   as Set
import           Prelude                    hiding (head, null, tail)

data Command = Strongest | Died | Recruit | Merge deriving (Eq, Ord, Bounded, Enum)

type Armies = IntMap (Set (Int, Int))

command:: Armies->([Int], Int) ->(Armies, Maybe Int)
command armies = execute where
  transform f idx = (IntMap.adjust f idx armies, Nothing)
  execute (com:args, num) = case (toEnum (com - 1) , args) of
    (Strongest, [i]) ->
      let (strongest, _) = Set.findMax (armies ! i)
      in (armies, Just strongest)
    (Died, [i]) -> transform Set.deleteMax i
    (Recruit, [i, c]) -> transform (Set.insert (c, num)) i
    (Merge, [i, j]) -> do
      let other = armies ! j
      transform (Set.union other) i
    _ -> undefined
  execute _ = undefined

main :: IO ()
main = do
  let readNums = fmap (fmap read. words) getLine
  [n , q] <- readNums
  let initial = IntMap.fromDistinctAscList [(i,Set.empty) | i <- [1..n]]
  comms <- replicateM q readNums
  let output = catMaybes . snd . mapAccumL command initial$ zip comms [1..]
  mapM_ print output
