{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE TypeSynonymInstances   #-}
{-# LANGUAGE ViewPatterns           #-}
module Main where

import           Control.Monad.State.Strict
import           Data.Sequence              (Seq, ViewL (..), (<|), (><))
import qualified Data.Sequence              as Seq

class (MonadState (Zipper a) m, Show a)=>Debug m a | m -> a where
  debug::Show b=>String->b->m ()

instance Show a=>Debug (TreeWalk a) a where
  -- debug mes x = do
  --   lift . putStr $ mes `mappend` " "
  --   lift $ print x
    debug = const . const $ return ()


data Tree a = Tree a (Seq (Tree a)) deriving Show

newtype Zipper a = Zipper{getZipper :: [(Int, Tree a)]} deriving Show

type TreeWalk a = StateT (Zipper a) IO

goParent::Debug m a=>m ()
goParent = do
  debug "go parent" ()
  Zipper ((num, tree):(pnum, Tree val cs):rest) <- get
  put . Zipper $ (pnum, Tree val (Seq.update num tree cs)): rest

goChild::Debug m a=>Int->m ()
goChild idx = do
  debug "go child" idx
  Zipper z@((_, Tree _ cs):_) <- get
  put . Zipper $ (idx, Seq.index cs idx):z

insertChild::Debug m a=>Int->a->m ()
insertChild idx x = do
  Zipper ((num, Tree val cs):rest) <- get
  debug "inserting" (idx,x)
  let new = Tree x Seq.empty
      cs'  = insert idx new cs
      parent = (num, Tree val cs'): rest
  put $ Zipper parent

deleteCurrent::Debug m a=>m ()
deleteCurrent = do
  Zipper ((idx,_):(num, Tree val cs):rest) <- get
  debug "deleting" ()
  let cs'    = delete idx cs
      parent = (num, Tree val cs'): rest
  put . Zipper $ parent

curIdx::Debug m a=>m Int
curIdx = gets (fst.head.getZipper)

insert::Int->a->Seq a->Seq a
insert idx v xs = let (l,r) = Seq.splitAt idx xs in l >< v <| r

delete::Int->Seq a->Seq a
delete idx xs = let (l,Seq.viewl -> _ :< r) = Seq.splitAt idx xs in l >< r

command::[String]->TreeWalk String ()
command com = do
  z <- gets getZipper
  debug "command" com
  let (idx, Tree val cs): rest = z
  case com of
    ["change", x]                 -> put . Zipper $ (idx,Tree x cs):rest
    ["print"]                     -> lift $ putStrLn val
    ["visit", "left"]             -> goParent >> goChild (idx - 1)
    ["visit", "right"]            -> goParent >> goChild (idx + 1)
    ["visit", "parent"]           -> goParent
    ["visit", "child", read -> x] -> goChild (x - 1)
    ["insert", "left", x ]        -> do
      goParent
      insertChild idx x
      goChild (idx + 1)
    ["insert", "right", x ]       -> do
      goParent
      insertChild (idx + 1) x
      goChild idx
    ["insert", "child", x]        -> insertChild 0 x
    ["delete"]                    -> deleteCurrent
    _ -> undefined
  z' <- gets getZipper
  debug "result" z'

initial::a->Zipper a
initial val = Zipper [(-1, Tree val Seq.empty)]

main :: IO ()
main = do
  n <- fmap read getLine
  comms <- replicateM n $ fmap words getLine
  evalStateT (mapM_ command comms) (initial "0")
