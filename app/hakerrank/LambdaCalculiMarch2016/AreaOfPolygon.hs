module Main where
import           Control.Monad
import           Data.List     (genericLength)
main :: IO ()
main = do
  n <- fmap read getLine
  pairs <- replicateM n $ fmap (map read . words) getLine :: IO [[Double]]
  let segArea [x1, y1] [x2, y2] = x1 * y2 - x2 * y1
      segArea _ _ = undefined
  print . abs . (/ 2) . sum $ zipWith segArea pairs $ tail pairs ++ [head pairs]
