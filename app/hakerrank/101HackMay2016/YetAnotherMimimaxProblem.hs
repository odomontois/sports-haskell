module Main where

import           Control.Monad
import           Data.Bits
import           Data.List
main :: IO ()
main = do
  void getLine
  ns <- fmap (fmap read.words) getLine:: IO [Int]
  let bs = head $ fmap finiteBitSize ns
      differBit = find (not. and . bitsEq) [bs, bs-1..0]
      bitsEq i = zipWith (==) xs (tail xs) where xs = fmap (`testBit` i) ns
  print $ case differBit of
    Nothing -> 0
    Just i -> minimum [x `xor` y| x <- xs, y <- ys] where
      (xs, ys) = partition (`testBit` i) ns
