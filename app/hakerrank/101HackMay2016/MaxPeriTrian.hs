  module Main where
  import           Data.List (find, sortBy)

  main :: IO ()
  main = getLine >>
    fmap (fmap read.words) getLine >>=
    putStrLn.maybe "-1" tris.find triangle. triples.sortBy (flip compare)  where
        triples xs = zip3 xs (tail xs) (tail (tail xs))
        triangle (z,y,x) = z < x + y
        tris (z,y,x) = unwords $ fmap show [x,y,z]
