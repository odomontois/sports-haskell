{-# LANGUAGE GADTs #-}
import           Control.Monad
import qualified Data.ByteString.Char8       as BS
import qualified Data.Vector.Unboxed.Mutable as VM

getInt::IO Int
getInt = do
  line <- BS.getLine
  let Just (x, _ ) = BS.readInt line
  return x

main:: IO ()
main = do
  ds <- VM.new 10 :: IO (VM.IOVector Int)
  let m = 10000001
  friends <- VM.new m
  let go 0 = return ()
      go i = VM.modify ds (+1) (mod i 10) >> go (quot i 10)
      check 10 = return True
      check i = do
        u <- VM.read ds i
        if u == 4|| u == 6 then return False else check (i + 1)
      friendly n = do
          VM.set ds 0
          go n
          check 0
      fill i j = unless (i == m ) $ do
        f <- friendly j
        if f then VM.write friends i j >> fill (i+1) (j+1) else fill i (j+1)
  fill 0 0
  q <- getInt
  replicateM_ q $ do
    x <- getInt
    u <- VM.read friends x
    print u
