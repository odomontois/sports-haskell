import           Control.Monad
import qualified Data.ByteString.Char8 as BS

main :: IO ()
main = do
  void BS.getLine
  as <- getInts
  bs <- getInts
  let qualify xs = zipWith compare xs $ tail xs
      res = and $ zipWith (==) (qualify as) (qualify bs)
  putStrLn $ if res  then "Yes" else "No"

getInts::IO [Int]
getInts = go <$> BS.getLine where
  go s = case  BS.readInt s of
      Nothing      -> []
      Just (x, s') -> x : go (BS.drop 1 s')
