import           Control.Arrow
import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.Foldable
import           Data.IntMap           (IntMap)
import qualified Data.IntMap.Strict    as IM
import           Data.Ord
import           Text.Printf

sums::[Int]->IntMap Int
sums = foldr step $ IM.singleton 0 0 where
   step i = foldr (add i) <*> IM.assocs
   add i (j, k) = IM.alter (attempt (k+1)) (i+j)
   attempt k = (`mplus` Just k). mfilter (< k)

main:: IO ()
main = do
  void BS.getLine
  ds <- getInts
  cs <- getInts
  let diff = sum ds - sum cs
      ops = sums (fmap (*2) ds ++ fmap(* (-2)) cs)
      search f = toList (f diff ops)
      opts = search IM.lookupGE ++ search IM.lookupLE
      dopts = fmap (first $ abs. subtract diff) opts
      (best, steps) = minimumBy (comparing fst) dopts
  printf "%d %d\n" best steps

getInts::IO [Int]
getInts = go <$> BS.getLine where
  go s = case  BS.readInt s of
      Nothing      -> []
      Just (x, s') -> x : go (BS.drop 1 s')
