import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Text.Printf

main :: IO ()
main = do
  (n, x, m) <- getInt3
  let part = do
        (w,p) <- getInt2
        return $ fi w * fi p / 100
  win <- sum <$> replicateM n part
  let res = (fi x - win) * fi m
  printf "%0.7f" res
  where fi = fromIntegral :: Int -> Double

getInt2::IO (Int,Int)
getInt2 = do
  line <- BS.getLine
  let Just (x, l') = BS.readInt line
      Just (y, _)  = BS.readInt $ BS.drop 1 l'
  return (x,y)

getInt3::IO (Int,Int, Int)
getInt3 = do
  line <- BS.getLine
  let Just (x, l') = BS.readInt line
      Just (y, l'')  = BS.readInt $ BS.drop 1 l'
      Just (z, _) = BS.readInt $ BS.drop 1 l''
  return (x,y,z)
