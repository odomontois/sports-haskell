{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections       #-}

import           Control.Applicative
import           Control.Monad
import           Data.Array.MArray
import           Data.Array.ST
import           Data.Array.Unboxed
import           Data.Attoparsec.ByteString.Char8
import qualified Data.ByteString.Char8            as BS
import           Data.List                        (foldl')
import           Data.Maybe
import           Debug.Trace

fromListFold::forall i a b.Ix i=>(b->a->b)->b->(i,i)->[(i,a)]->Array i b
fromListFold comb z bs lst = runSTArray $ do
  arr <- newArray bs z
  forM_ lst $ \(i,x) -> do
    new <- (`comb` x) <$> readArray arr i
    writeArray arr i new
  return arr

fromListFoldDouble::forall i a.Ix i=>(Double->a->Double)->Double->(i,i)->[(i,a)]->UArray i Double
fromListFoldDouble comb z bs lst = runSTUArray $ do
  arr <- newArray bs z
  forM_ lst $ \(i,x) -> do
    new <- (`comb` x) <$> readArray arr i
    writeArray arr i new
  return arr

solution::[Double]->[(Int,Int)]->[(Int,Int)]->Double
solution ps ladders snakes = play 0 0 initial 1 where
  initial = fromListFoldDouble (+) 0 (1,100) [(1,1)]
  ports = fromListFold mplus Nothing (1,100) $ fmap (fmap Just) (ladders ++ snakes)
  ips   = zip [1..] ps
  play e p arr k = let arr' = fromListFoldDouble (+) 0 (1,100) $ do
                            (i, p1) <- assocs arr
                            guard $ i /= 100
                            (t, p2) <- ips
                            let j = if i + t <= 100 then i + t else i
                            return (fromMaybe j (ports ! j), p1 * p2)
                       p100 = arr' ! 100
                       p' = p + p100
                       e' = e + k * p100
                   in if p' > 0.999 then e' else play e' p' arr' (k + 1)

main :: IO ()
main = do
  n <- parseLine decimal
  replicateM_ n $ do
    ps <- parseLine $ double `sepBy` char ','
    void BS.getLine
    ladders <- parseLine $ pair `sepBy` char ' '
    snakes <- parseLine $ pair `sepBy` char ' '
    print $ solution ps ladders snakes
  where
  parseLine f = (either undefined id .  parseOnly f) <$> BS.getLine
  pair = (,) <$>  decimal <*> (char ',' *> decimal)
