{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

import           Control.Monad.State
import           Control.Monad.Writer
import           Data.ByteString.Char8 (ByteString)
import qualified Data.ByteString.Char8 as BS
import           Data.IntSet           (IntSet)
import qualified Data.IntSet           as IntSet
import           Data.Map.Strict       (Map)
import qualified Data.Map.Strict       as Map

data File = File ByteString Int
data Log = Add File | Delete File | Rename File File

instance Show File where
  show (File name 0)   = BS.unpack name
  show (File name idx) = BS.unpack name ++ "(" ++ show idx ++ ")"

instance Show Log where
  show (Add file)       = "+ " ++ show file
  show (Delete file)    = "- " ++ show file
  show (Rename from to) = "r " ++ show from ++ " -> " ++ show to

data FileIdx = FileIdx {
  fiCur    :: {-# UNPACK #-} !Int,
  fiVacant :: !IntSet,
  fiBusy   :: !IntSet
}

fileIdx::FileIdx
fileIdx = FileIdx 0 IntSet.empty IntSet.empty

type Dir = StateT (Map ByteString FileIdx) (Writer [Log])

register::File->Dir ()
register (File name idx) = modify (Map.alter (Just. insert) name) where
  insert Nothing | idx == 0  = fileIdx { fiCur = 1}
                 | otherwise = fileIdx { fiBusy = IntSet.singleton idx }
  insert (Just FileIdx{..}) | fiCur ==  idx = let (cur, busy) = unroll (fiCur + 1) fiBusy
                                              in FileIdx{ fiCur = cur, fiBusy = busy, ..}
                            | otherwise     = FileIdx{ fiBusy = IntSet.insert idx fiBusy, ..}
  unroll c s | IntSet.member (c + 1) s = unroll (c + 1) (IntSet.delete (c + 1) s)
             | otherwise               = (c, s)

unregister::File->Dir ()
unregister (File name idx) = modify (Map.adjust cut name) where
  cut FileIdx {..} | fiCur == 1 + idx = let (cur, vacant) = unroll idx fiVacant
                                        in FileIdx{ fiCur = cur, fiVacant = vacant, ..}
                   | otherwise  = FileIdx{ fiVacant = IntSet.insert idx fiVacant, ..}
  unroll c  s   | IntSet.member (c - 1) s = unroll (c - 1) (IntSet.delete (c - 1) s)
                | otherwise               = (c, s)

parse::ByteString->File
parse str = case BS.breakSubstring str "(" of
  (name, pidx) | BS.null pidx -> File name 0
               | otherwise -> let Just (idx, _) = BS.readInt $ BS.drop 1 pidx in File name idx

next::ByteString->Dir File
next name = do
  file <- File name . maybe 0 fiCur <$> gets (Map.lookup name)
  register file
  return file

remove::ByteString->Dir File
remove name = do
  let file = parse name
  unregister file
  return file

crt::ByteString->Dir ()
crt name = do
  file <- next name
  tell [Add file]

del::ByteString->Dir ()
del name = do
  file <- remove name
  tell [Delete file]

rnm::ByteString->ByteString->Dir ()
rnm from to = do
  fFrom  <- remove from
  fTo    <- next to
  tell [Rename fFrom fTo]

main :: IO ()
main = do
  Just (n, _) <- BS.readInt <$> BS.getLine
  commands <- replicateM n $ do
    line <- BS.getLine
    return $ case BS.words line of
      ["crt", name]     -> crt namezd
      ["del", name]     -> del name
      ["rnm", from, to] -> rnm from to
      _                 -> return ()
  let log' = execWriter $  evalStateT (sequence_ commands) Map.empty
  forM_ log' print
