import qualified Data.ByteString.Char8 as BS

getInts::IO [Int]
getInts = go <$> BS.getLine where
  go s = case  BS.readInt s of
      Nothing      -> []
      Just (x, s') -> x : go (BS.drop 1 s')

balance xs = abs $ sum xs - 2 * (sum $ take (length xs `quot` 2) xs)

main = getLine >> getInts >>= print . balance
