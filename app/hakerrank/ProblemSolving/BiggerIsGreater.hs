{-# LANGUAGE OverloadedStrings #-}

import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.List
import           Data.Ord

solution :: Ord a => [a] -> Maybe [a]
solution [] = Nothing
solution xs@(x:xs') = findIndex (uncurry (>)) (scanl' step (x, x) xs') >>= result
  where
    step (mx, prev) next = (max mx next, next)
    result i = do
      let (pref, y:rest) = splitAt i xs
      mxi <- elemIndex (minimum $ filter (> y) pref) pref
      let (p1, mx:p2) = splitAt mxi pref
      return $ sortOn Down (y : p1 <> p2) <> (mx : rest)

main = do
  Just (n, _) <- BS.readInt <$> BS.getLine
  replicateM n $
    BS.getLine >>= BS.putStrLn . maybe "no answer" (BS.reverse . BS.pack) . solution . BS.unpack . BS.reverse
