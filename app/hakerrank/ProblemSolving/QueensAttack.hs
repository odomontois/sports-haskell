module QueensAttack where

import qualified Data.ByteString.Char8 as BS
import qualified Data.Vector.Unboxed           as UVec
import Data.Vector.Unboxed.Mutable as V
import Control.Monad


getInt2::IO (Int,Int)
getInt2 = do
  line <- BS.getLine
  let Just (x, l') = BS.readInt line
      Just (y, _)  = BS.readInt $ BS.drop 1 l'
  return (x,y)

data Side = U | D | L | R | UR | UL | DR | DL deriving (Eq, Ord, Enum, Bounded)

sideOf :: Int -> Int -> Maybe (Side, Int)
sideOf 0 i             = Just $ if i < 0 then (L , -i) else (R , i)
sideOf i 0             = Just $ if i < 0 then (U , -i) else (D , i)
sideOf i j | i == j    = Just $ if i < 0 then (UL, -i) else (DR, i)
           | i == -j   = Just $ if i < 0 then (UR, -i) else (DL, i)
           | otherwise = Nothing


main = do
  (n, k) <- getInt2
  (qr, qc) <- getInt2
  state <- new 8
  set state n
  let put :: Side -> Int -> IO ()
      put s i = modify state (min $ i - 1) (fromEnum s) 
      qr'' = n - qr + 1
      qc'' = n - qc + 1
      go = do
        (r, c) <- getInt2
        forM_ (sideOf (r - qr) (c - qc)) (uncurry put)
  put U qr
  put D qr''
  put L qc
  put R qc''
  put UL $ min qr qc
  put UR $ min qr qc''
  put DL $ min qr'' qc
  put DR $ min qr'' qc''
  replicateM_ k go
  xs <- traverse (V.read state) [0..7]
  print $ sum xs




