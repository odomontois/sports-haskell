import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.List

main = do
  a <- read <$> getLine
  b <- read <$> getLine
  putStrLn . result $ filter carpecar [a .. b]
  where
    result [] = "INVALID RANGE"
    result xs = unwords $ show <$> xs
    carpecar n =
      let d = length $ show n
          s = show (n * n)
          (l, r) = splitAt (length s - d) s
       in read ('0' : l) + read r == n
