module Main where

import           Data.Ratio
import           Math.Seq

main :: IO ()
main = do
  let as = [(4,3),(5,4),(4,4)]
      urn x y = x % (x + y)
      urns = fmap (uncurry urn) as
      res = sum $ do
        c <- combinations 2 [0..2]
        let vs       =  indexes 3 c
            prob t p = if t then p else 1 - p
        return $ product $ zipWith prob vs urns
  print res
