module Main where

import           Data.Bits
import           Data.IntMap (IntMap)
import qualified Data.IntMap as IntMap

solution, simple, sum2, sum3, eq2, eq3::Int->Int->Int->Int->Integer
solution a b c d = simple a b c d - sum2 a b c d + 3*sum3 a b c d
sum2     a b c d = eq2 a b c d + eq2 a c b d + eq2 a d b c + eq2 b c a d + eq2 b d a c + eq2 c d a b
sum3     a b c d = eq3 a b c d + eq3 a b d c + eq3 a c d b + eq3 b c d a
simple   a b c d = sumnz (from a |+| from b |+| from c |+| from d )
eq2      a b c d = toInteger (a `min` b) * sumnz (from c |+| from d)
eq3      a b c d = sumnz ( from (a `min` b `min` c) |+| from d )

from :: Int -> IntMap Integer
from   x = IntMap.fromList . zip [1..x] $ repeat 1

sumnz :: IntMap Integer -> Integer
sumnz    = sum . IntMap.delete 0

(|+|) :: IntMap Integer -> IntMap Integer -> IntMap Integer
mx |+| my = IntMap.fromListWith (+) $ do
  (x, u) <- IntMap.toList mx
  (y, v) <- IntMap.toList my
  return (x `xor` y, u*v)



main :: IO ()
main = getLine >>= print. sol . fmap read. words where
  sol [a,b,c,d] = solution a b c d
  sol _ = undefined
