import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.Monoid

getInt2::IO (Int,Int)
getInt2 = do
  line <- BS.getLine
  let Just (x, l') = BS.readInt line
      Just (y, _)  = BS.readInt $ BS.drop 1 l'
  return (x,y)

getInt::IO Int
getInt = do
  line <- BS.getLine
  let Just (x, _ ) = BS.readInt line
  return x

solution::Int->Int->Maybe [Int]
solution n 0 = Just [1..n]
solution n k = let r = mod n (2*k) in if r /= 0 then Nothing else Just result where
  result = [0, 2*k..n-1] >>= sub
  sub i  = fmap (+i) sequ
  sequ   = [k+1, k+2..2*k] <> [1..k]

main :: IO ()
main = getInt >>= flip replicateM_ (getInt2 >>= putStrLn . maybe "-1" (unwords . fmap show) . uncurry solution )
