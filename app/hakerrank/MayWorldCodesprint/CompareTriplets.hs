import           Control.Applicative
import           Data.Monoid
readLst = fmap read.words <$> getLine :: IO [Int]
points x y = case compare x y of
  GT -> (Sum 1, Sum 0)
  LT -> (Sum 0, Sum 1)
  EQ -> (Sum 0, Sum 0)
ppair xs ys = unwords [ssum xs, ssum ys] where ssum = show.getSum
main = zipWith points <$> readLst <*> readLst >>= putStrLn . uncurry ppair  . mconcat
