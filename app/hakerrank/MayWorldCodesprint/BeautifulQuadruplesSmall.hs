import           Control.Monad
import           Data.Bits
import           Data.List     (genericLength, sort)

main :: IO ()
main = getLine >>= print. solution. sort . fmap read. words where
  solution::[Int]->Integer
  solution [a,b,c,d] = genericLength $ do
    w <- [1..a]
    x <- [w..b]
    y <- [x..c]
    z <- [y..d]
    guard $ w `xor` x `xor` y `xor` z /= 0
    return ()

  solution _ = undefined
