import           Control.Applicative
import           Data.List
import           Data.Maybe
import           Data.Monoid
solution::String->Int->Maybe String
solution cs k = if k >= ineq then Just result else Nothing where
  n = length cs
  m = n `quot` 2
  back = take m $ reverse cs
  ineq =  length . filter id $ zipWith (/=) cs back
  (rems, res) = mapAccumL step (k - ineq) $ zip cs back
  mid | even n     = []
      | rems > 0   = "9"
      | otherwise  = [cs !! m]
  result = res <> mid <> reverse res
  step rs ('9', '9')  = (rs, '9')
  step rest (x, y) = if x == y then eq else neq where
    neq | x == '9' || y == '9' = (rest , '9')
        | rest > 0             = (rest - 1, '9')
        | otherwise            = (rest , max x y)
    eq  | x == '9'             = (rest, '9')
        | rest > 1             = (rest - 2, '9')
        | otherwise            = (rest, x)



main :: IO ()
main = do
  [_m,k] <- fmap read. words <$> getLine
  digs <- getLine
  putStrLn . fromMaybe "-1" $ solution digs k
