import           Control.Applicative
import           Control.Monad
import           Data.Attoparsec.ByteString.Char8
import           Data.ByteString.Char8            (getLine)
import           Data.Semigroup
import           Data.Tuple
import           Prelude                          hiding (getLine)


data MulMod a = MulMod{mmVal :: a, _mmMod :: a}

instance Integral a=>Semigroup (MulMod a)
  where (MulMod x _ ) <> (MulMod y m) = MulMod ((x * y) `mod` m) m

parseLine:: Parser a->IO a
parseLine p = ((\(Right x) -> x) . parseOnly p) <$> getLine

pm::Parser Int
pm =  powMod <$> decimal <*> (space *> signed decimal) <*> (space *> decimal)

powMod ::  Integral a => a -> a -> a -> a
powMod =  result where
  calc a b x = mmVal . stimes b $ MulMod a x
  result _ 0 _ = 1
  result a b x  = case compare b 0 of
    EQ -> 1
    GT -> calc a b x
    LT -> calc (inverse a x) (abs b) x

inverse::Integral a=>a->a->a
inverse = inv where
  inv x m = let (k, _) = euclid x m in mod k m
  euclid a b | b > a = swap $ euclid b a
  euclid _ 1         = (0, 1)
  euclid a b = let (d,r) = divMod a b
                   (k, l) = euclid b r
                in (l, k-d*l)

main :: IO ()
main = do
  t <- parseLine decimal
  replicateM_ t (parseLine pm >>= print)
