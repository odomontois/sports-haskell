import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.List             (unfoldr)
import           Text.Printf

getInts::IO [Int]
getInts = fmap (unfoldr readOne) BS.getLine where
  readOne s = do
    (x , s') <- BS.readInt s
    return (x, BS.drop 1 s')

getInt::IO Int
getInt = do
  line <- BS.getLine
  let Just (x, _ ) = BS.readInt line
  return x


main:: IO()
main = do
  n <- getInt
  replicateM_ n $ do
    [px, py, qx ,qy ] <- getInts
    let rx = 2 * qx - px
        ry = 2 * qy - py
    printf "%d %d\n" rx ry
