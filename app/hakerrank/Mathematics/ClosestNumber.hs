import           Control.Applicative
import           Control.Monad
import           Data.Attoparsec.ByteString.Char8
import           Data.ByteString.Char8            (getLine)
import           Prelude                          hiding (getLine)


int3::Parser (Int,Int, Int)
int3 =  let int = signed decimal in (,,) <$> int <*> (space *> int) <*> (space *> int)

parseLine:: Parser a->IO a
parseLine p = do
  Right x <- parseOnly p <$> getLine
  return x

solution 1 _ 1         = 1
solution _ b _ | b < 0 = 0
solution a b x = let ab = a ^ b
                     r = mod ab x
                     l = ab - r
                     h = ab + x - r
                     in if abs (ab - l) < abs (ab - h) then l else h

main = (parseLine decimal >>=) . flip replicateM_ $ do
  (a,b,x) <- parseLine int3
  print (solution a b x)
