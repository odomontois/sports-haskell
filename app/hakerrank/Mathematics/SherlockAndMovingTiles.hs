import           Control.Applicative
import           Control.Monad
import           Data.Attoparsec.Text
import qualified Data.Text.IO         as TIO
import           Text.Printf

doubles:: Parser (Double, Double, Double)
doubles = (,,) <$> double <*> (space *> double) <*> (space *>double)

parseLine:: Parser a->IO a
parseLine p = do
  Right x <- parseOnly p <$> TIO.getLine
  return x

main:: IO()
main = do
  (l, s1, s2) <- parseLine doubles
  n <- parseLine decimal
  let dif = abs (s1 - s2) / sqrt 2
  replicateM_ n $ do
    q <- parseLine double
    printf "%.10f\n" $ (l - sqrt q) / dif
