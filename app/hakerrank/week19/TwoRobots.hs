module Main where
import           Control.Applicative
import           Control.Monad
import           Data.Foldable           (foldl')
import           Data.List.NonEmpty      (NonEmpty (..))
import           Data.Semigroup
import           Data.Semigroup.Foldable

merge::(Ord a, Semigroup b)=>NonEmpty (a,b)-> NonEmpty (a,b)-> NonEmpty (a,b)
merge (x:| xs) (y :| ys) = inject (:|) x y xs ys

inject::(Ord a, Semigroup b)=>((a,b)->[(a,b)]->c)->(a,b)->(a,b)->[(a,b)]->[(a,b)]->c
inject cs (a,x) (b,y) xs ys = case compare a b of
  LT -> (a,x)      `cs` go xs         ((b,y):ys)
  GT -> (b,y)      `cs` go ((a,x):xs) ys
  EQ -> (a,x <> y) `cs` go xs         ys

go ::(Ord a, Semigroup b)=>[(a, b)] -> [(a, b)] -> [(a, b)]
go [] ys = ys
go xs [] = xs
go (x:xs) (y:ys) = inject (:) x y xs ys

data Pair = Pair {
  pfst::{-# UNPACK #-} !Int,
  psnd::{-# UNPACK #-} !Int
} deriving (Show, Eq, Ord)

type PSet = Int -> Pair -> Pair
type PGet = Pair -> Int
data PLens = PLens {
  pget:: !PGet,
  pset:: !PSet
}
plfst, plsnd::PLens
plfst = PLens pfst putfst
plsnd = PLens psnd putsnd

putfst, putsnd::PSet
putfst z (Pair _ y) = Pair z y
putsnd z (Pair x _) = Pair x z

getList::Read a=>IO [a]
getList = map read . words <$> getLine

solution::[(Int,Int)]->Int
solution = getMin . foldMap1 snd . foldl' step initial where
  initial = (Pair 0 0, Min 0) :| []
  dist = (abs.). (-)
  command from to robo (robs, track) =
    let current = pget robo robs
        realloc = if current == 0 then 0 else dist current from
        move    = dist from to
        next    = pset robo to robs
    in (next, track + Min (realloc + move))
  step variants (from, to) =
    let com = (<$> variants) . command from to
    in  merge (com plfst) (com plsnd)

pairs::[a]->[(a,a)]
pairs (a:b:xs) = (a,b) : pairs xs
pairs _        = []

output::Int->[Int]->IO ()
output 0 _ = return ()
output t ns =
  let _m: n: rest  = ns
      (qns , rest') = splitAt (2 * n) rest
  in print (solution $ pairs qns) >> output (t - 1) rest'

main::IO ()
main = do
  nums <- words <$> getContents
  let t:rest = map read nums in output t  rest
