{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE FunctionalDependencies     #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}

module Main where

import           Control.Applicative
import           Control.Monad
import           Data.Foldable
import           Data.List.NonEmpty      (NonEmpty (..))
import qualified Data.List.NonEmpty      as NE
import           Data.Maybe
import           Data.Semigroup
import           Data.Semigroup.Foldable

class Semigroup v => Measured a v | a -> v where measure::a->v

class Multiply a where
  zero::a
  (*~) ::a->a->a

data SegInfo a = SegInfo{
  segInit  :: !a,
  segTail  :: !a,
  segBound :: !a,
  segFull  :: !a
}

instance Show a => Show (SegInfo a) where
  show x = show (segFull x) ++ "(" ++ show (segInit x) ++ "<" ++ show (segBound x) ++ ">" ++ show (segTail x) ++ ")"

data PairSegInfo a b = PairSegInfo {
  psiVal    :: !(Option b),
  psiSingle :: !(SegInfo a),
  psiPair   :: !(SegInfo a)
}

instance (Show a)=>Show (PSIMin a) where
  show (PairSegInfo (Option (Just (Min a))) s p) = "min[" ++ show a ++ "]" ++ "I:" ++ show s ++ "II:" ++ show p
  show _ = undefined


instance Semigroup b => Measured (PairSegInfo a b) b where measure = fromJust . getOption . psiVal

instance Num a =>Semigroup (SegInfo a) where
  SegInfo li lt lb lf <> SegInfo ri rt rb rf =
    SegInfo (li + ri) (lt + rt) (lb + rb) (lf + rf)

instance (Num a, Semigroup b)  => Semigroup (PairSegInfo a b) where
  PairSegInfo lv ls lp <> PairSegInfo rv rs rp =
    PairSegInfo (lv <> rv) (ls <> rs) (lp <> rp)

instance (Num a, Semigroup b, Eq b) => Multiply (PairSegInfo a b) where
  zero = PairSegInfo (Option Nothing) (segInfo 0) (segInfo 0)

  PairSegInfo lVal lSingle lPair *~ PairSegInfo rVal rSingle rPair = let
    val    = lVal <> rVal
    add  x = if val == x  then 1 else 0
    rEmpty = add lVal
    lEmpty = add rVal

    sBound = segBound lSingle * segBound rSingle

    sTail  =          lEmpty  * segTail  rSingle  +
             segTail  lSingle * segBound rSingle

    sInit  = segBound lSingle * segInit  rSingle  +
             segInit  lSingle *          rEmpty

    sFull  =          lEmpty  * segFull  rSingle  +
             segTail  lSingle * segInit  rSingle  +
             segFull  lSingle *          rEmpty

    pBound = segBound lSingle * segBound rPair   +
             segInit  lSingle * segTail  rSingle  +
             segBound lPair   * segBound rSingle

    pTail  =          lEmpty  * segTail  rPair    +
             segTail  lSingle * segBound rPair    +
             segFull  lSingle * segTail  rSingle  +
             segTail  lPair   * segBound rSingle

    pInit  = segBound lSingle * segInit  rPair    +
             segInit  lSingle * segFull  rSingle  +
             segBound lPair   * segInit  rSingle  +
             segInit  lPair   * rEmpty

    pFull  = lEmpty           * segFull  rPair    +
             segTail  lSingle * segInit  rPair    +
             segFull  lSingle * segFull  rSingle  +
             segTail  lPair   * segInit  rSingle  +
             segFull  lPair   *          rEmpty


    single = SegInfo sInit sTail sBound sFull
    pair   = SegInfo pInit pTail pBound pFull
    in  PairSegInfo val single pair


newtype Calc a v = Calc{getCalc::NonEmpty a} deriving (Show)

instance (Measured a v, Multiply a, Semigroup a, Ord v)=>Semigroup (Calc a v) where
  (<>)  = merge where
    Calc xs `merge` Calc ys =  Calc $ NE.fromList $ go zero zero (NE.toList xs) (NE.toList ys)
    go _    _    []      []    = []
    go _    yacc xs      []    = map (*~ yacc)  xs
    go xacc _    []      ys    = map (xacc *~) ys
    go xacc yacc (x:xs) (y:ys) = let
      xacc' = xacc <> x
      yacc' = yacc <> y
      in case measure x `compare` measure y of
        GT -> (x *~ yacc)               : go xacc' yacc  xs     (y:ys)
        LT -> (xacc *~ y)               : go yacc  yacc' (x:xs) ys
        EQ -> (x *~ yacc' <> xacc *~ y) : go xacc' yacc' xs     ys

newtype Sized a = Sized{getSized::NonEmpty a} deriving (Functor, Show)

instance Foldable Sized where foldMap = foldMapDefault1

instance Foldable1 Sized where
  foldMap1 f = fold1 . fmap f
  fold1 = fold1Impl where
    fold1Impl (Sized (x:|xs)) = stackup ((0::Int,x):|[]) xs
    stackup heap (x: xs) = stackup (put (0,x) heap) xs
    stackup heap []      = foldMap1 snd heap
    put (xlev, xval) ((ylev, yval):|ys)
      | xlev < ylev = (xlev, xval) :| ((ylev, yval):ys)
      | otherwise   = further (xlev + 1, xval <> yval) ys
    further x [] = x :| []
    further x (y:ys) = put x (y:|ys)

newtype Verbose a = Verbose{getVerbose::a}
instance (Show a, Semigroup a)=>Semigroup (IO (Verbose a)) where
  ma <> mb = do
    (Verbose a) <- ma
    (Verbose b) <- mb
    let c = a <> b
    putStrLn $ show a ++ " <> " ++ show b ++ " = " ++ show c
    return $ Verbose c
verbose::a->IO (Verbose a)
verbose = return . Verbose

type PSIMin a = PairSegInfo a (Min a)
type CalcPSIMin a = Calc (PSIMin a) (Min a)

segInfo::a->SegInfo a
segInfo a = SegInfo a a a a

psiMinCalc::Num a=>a->CalcPSIMin a
psiMinCalc m = Calc (PairSegInfo (Option $ Just $ Min m) (segInfo 1) (segInfo 0) :| [])


getPsiMinCount::Num a=>PSIMin a->a
getPsiMinCount ps = getMin (option 0 id $ psiVal ps) * segFull (psiPair ps)

calcPsiMin::Num a=>CalcPSIMin a->a
calcPsiMin (Calc psis) = getSum $ foldMap1 (Sum . getPsiMinCount) psis

modulus::Integer
modulus = 10^(9::Int) + 7

main::IO ()
main = do
  void getLine
  nums <- Sized . NE.fromList . map (psiMinCalc . read) . words <$> getLine
  -- let nums = Sized. NE.fromList . map psiMinCalc $ [1..100000::Integer]
  print $ calcPsiMin (fold1 nums) `mod` modulus
