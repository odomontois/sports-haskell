import Data.List (genericTake)
import qualified Data.IntSet as Set
import Control.Applicative

solution::Integral a=>a->a->a->Int
solution c m n =
  let a +% b = mod (a + b) m
      c'     = mod (c * c) m
      fibs = 0:mod c' m: zipWith (+%) fibs (tail fibs)
      odds (x:xs) = x : odds (tail xs)
      odds _      = undefined
      efib = genericTake (n * 2 - 3) $  odds $ drop 7 fibs
      in  Set.size $ Set.fromList $ map (fromIntegral) efib

main::IO ()
main = (map read . words <$> getLine::IO [Integer]) >>= \[c,m,n] -> print (solution c m n)
