import           Control.Applicative

prefixate::Eq a=>[a]->[a]->([a],[a],[a])
prefixate = go [] where
  go acc (x:xs) (y:ys) | x == y = go (x:acc) xs ys
  go acc xs ys = (reverse acc, xs, ys)

putString::String->IO ()
putString xs =  putStrLn $ show  (length xs) ++ ' ':xs

main::IO ()
main = do
  (pref, tx, ty) <- prefixate <$> getLine <*> getLine
  putString pref
  putString tx
  putString ty
