{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Main where

import           Control.Applicative
import           Control.Monad.State.Strict
import           Control.Monad.Writer.Lazy
import           Data.Bool
import           Data.IntMap                (IntMap, (!))
import qualified Data.IntMap                as IntMap
import           Data.Maybe

kmp::(Monad m, Ord a)=>m (IntMap (a,Int))->(Int->Int->m ())->[a]->m ()
kmp pat report = go 0 0 where
  go _ _ [] = return ()
  go i j (x:xs) = do
    m <- pat
    let eq = (== x) . fst <$> IntMap.lookup i m
    if fromMaybe False eq then do
      report (i + 1) j
      go (i + 1) (j + 1) xs
    else if i == 0 then do
      report 0 j
      go 0 (j + 1) xs
    else do
      let (_, fallback) = m ! (i - 1)
      go fallback j (x:xs)

analyze::(Ord a)=>[a]->IntMap (a, Int)
analyze [] = IntMap.empty
analyze (x:xs) = execState (kmp get report xs) initial where
  initial = IntMap.singleton 0 (x, 0)
  report i j = modify $ IntMap.insert (j + 1) (xm ! j, i)
  xm = IntMap.fromAscList $ zip [0..] xs

search::Ord a=>IntMap (a, Int)->[a]->[Int]
search pat xs = execWriter (kmp (return pat) report xs) where
  size = IntMap.size pat
  report i j | i == size = tell [j-i+1]
             | otherwise = return ()

findAll::Ord a=>[a]->[a]->[Int]
findAll = search . analyze

main :: IO ()
main = do
  n <- read <$> getLine
  replicateM_ n $ do
    found <- flip findAll <$> getLine <*>  getLine
    putStrLn . bool "YES" "NO" $ null found
