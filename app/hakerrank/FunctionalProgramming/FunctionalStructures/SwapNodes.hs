{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables   #-}

module Main where

import           Data.Array.IArray
import           Data.List         (genericLength)
import           Data.Monoid
import Control.Monad
import Data.Foldable (toList, Foldable, foldMap)


data Tree a = Node a (Tree a) (Tree a) | Nil deriving (Eq, Ord, Show)

build::forall i.(Ix i, Num i)=>[(i,i)]->Tree i
build is = go 1 where
  a = listArray (1, genericLength is) is::Array i (i,i)
  go (-1) = Nil
  go i = let (l,r) = a ! i in Node i (go l) (go r)

instance Foldable Tree where
  foldMap _ Nil = mempty
  foldMap f (Node x l r) = foldMap f l <> f x <> foldMap f r

rotate::(Num a, Eq a)=>a->Tree b->Tree b
rotate i = go 1 where
  go _ Nil = Nil
  go j (Node x l r)
    | i == j    = Node x (go 1 r) (go 1 l)
    | otherwise = Node x (go j' l) (go j' r) where j' = j + 1

main :: IO ()
main = do
  n <- fmap read getLine
  let pair [x,y] = (x,y)
      pair _     = undefined
  tree <- fmap build $ replicateM n $ fmap (pair. fmap read. words) getLine :: IO (Tree Int)
  q <- fmap read getLine
  let step t i = do
      let tree' = rotate i t
      putStrLn $ unwords $ map show $ toList tree'
      return tree'
  qs <- replicateM q $ fmap read getLine :: IO [Int]
  foldM_ step tree qs
