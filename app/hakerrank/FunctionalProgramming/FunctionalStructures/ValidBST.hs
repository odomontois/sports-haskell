module Main where

import           Control.Arrow
import           Control.Monad
import           Data.Bool
import           Data.Map      (Map, (!))
import qualified Data.Map      as Map
import           Data.Maybe

construct::Ord a=>[a]->Map (Int,Int) (Maybe (a,a))
construct xs = m where
  vals = Map.fromAscList $ zip [1..] xs
  m = Map.fromAscList $ fmap (id &&& uncurry calc) idxs
  idxs = [(i,j)| i <- [1..n], j <- [i..n]]
  n = length xs
  calc i j | i == j    = let x = vals ! i in Just (x,x)
           | otherwise = msum $ fmap search [i..j] where
               that = vals ! i
               search k
                | k == i  = do
                   (minR, maxR) <- m ! (i+1,j)
                   guard $ minR > that
                   return (that, maxR)
                | k == j  = do
                   (minL, maxL) <- m ! (i+1, j)
                   guard $ maxL < that
                   return (minL, that)
                | otherwise = do
                   (minL, maxL) <- m ! (i+1,k)
                   (minR, maxR) <- m ! (k+1,j)
                   guard $ maxL < that && minR > that
                   return (minL, maxR)

main :: IO ()
main = do
  t <- fmap read getLine
  replicateM_ t $ do
    void getLine
    ns <- fmap (fmap read.words) getLine:: IO [Int]
    let m = construct ns
    -- mapM_ print $ Map.assocs m
    putStrLn . bool "NO" "YES" . isJust $ m ! (1, length ns)
