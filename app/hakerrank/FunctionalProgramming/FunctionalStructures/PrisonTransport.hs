{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TupleSections         #-}
module Main where
import           Control.Monad
import           Control.Monad.State.Strict
import           Data.Either
import           Data.IntMap                (IntMap, (!))
import qualified Data.IntMap                as IntMap

type DisjointSets = IntMap (Either Int Int)

initial::Int->DisjointSets
initial n = IntMap.fromAscList $ fmap (, Right 1) [1..n]

group::MonadState DisjointSets m=>Int->m (Int, Int)
group num = do
  qual <- gets (! num)
  case qual of
    Right size -> return (num, size)
    Left parent -> do
      (g,size) <- group parent
      if g == parent then return (g, size) else do
        modify $ IntMap.insert num $ Left g
        return (g, size)

assign::MonadState DisjointSets m=>Int->Int->m()
assign i j = do
  (gi,si) <- group i
  (gj,sj) <- group j
  unless (gi == gj) $ do
    let (small, large) = if si < sj then (gi, gj) else (gj, gi)
    modify $ IntMap.insert small $ Left large
    modify $ IntMap.insert large $ Right $ si + sj

sqri::Int->Int
sqri = ceiling . (sqrt::Double->Double) . fromIntegral

main :: IO ()
main = do
  n <- fmap read getLine
  q <- fmap read getLine
  let pair [x,y] = (x,y)
      pair _= undefined
  qs <- replicateM q $ fmap (pair. fmap read. words) getLine
  let result = execState (sequence_ $ fmap (uncurry assign) qs) $ initial n
  print . sum . fmap sqri . rights $ IntMap.elems result
