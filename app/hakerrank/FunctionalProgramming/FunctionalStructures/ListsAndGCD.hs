module Main where

import           Control.Arrow
import           Control.Monad
import qualified Data.IntMap   as IntMap
import           Data.List

main :: IO ()
main = let  pairs = unfoldr pair
            pair (x:y:rest) = Just ((x,y), rest)
            pair _          = Nothing
            repr     = IntMap.fromList. pairs.fmap read.words
            spaced   = unwords . fmap show . uncurry (:) . second return
            unrepr   = unwords . fmap spaced . IntMap.toAscList
            red      = foldl1' $ IntMap.intersectionWith min
            readRepr = fmap repr getLine
            n        = fmap read getLine
            reprs    = flip replicateM readRepr
            result   = putStrLn . unrepr . red
      in n >>= reprs >>= result
