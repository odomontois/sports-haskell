{-# LANGUAGE TupleSections #-}

module Main where

import           Control.Monad
import           Data.Array.IArray
import           Data.Array.Unboxed

mapping::Int->Int->[[(Int,Int)]]
mapping m n = fmap go [0..(min m n - 1) `div` 2] where
  go i = fmap ((i+1,) . (i+1+))  [0..n-2*i-2] ++
         fmap ((,n-i) . (i+1+))  [0..m-2*i-2] ++
         fmap ((m-i,) . (n-i-))  [0..n-2*i-2] ++
         fmap ((,i+1) . (m-i-))  [0..m-2*i-2]

shift::Int->[a]->[a]
shift _ [] = []
shift n xs = let n' = n `mod` length xs in drop n' xs ++ take n' xs

main :: IO ()
main = do
  [m,n,r] <- fmap (fmap read. words) getLine
  let mp = mapping m n
      sm = map (shift r) mp
      iline = fmap (fmap read.words) getLine
  ls <- replicateM m iline
  let ass = concat ls
      ela = listArray ((1,1), (m,n)) ass :: UArray (Int,Int) Int
      sel = fmap (fmap (ela !) ) sm
      sar = array ((1,1), (m,n)) $ concat $ zipWith zip mp sel :: UArray (Int, Int) Int
      sls = [[sar ! (i,j) | j <- [1..n]] | i <- [1..m] ]
  forM_ sls $ putStrLn. unwords. fmap show
