{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE ViewPatterns               #-}

import           Control.Monad
import           Data.Monoid
import           Data.Tagged

class Monoid (Measure a) => Measured a where
  type Measure a :: *
  measure::a->Measure a

instance (Measured a, v ~ Measure a) => Measured (Node v a) where
  type Measure (Node v a) = Measure a

  measure (Node2 m _ _ ) = m
  measure (Node1 x) = measure x

instance (Measured a, v ~ Measure a) => Measured (Tree v a) where
  type Measure (Tree v a) = Measure a

  measure (Single a) = measure a
  measure (Branch tree) = measure tree

instance Monoid b=>Measured (Tagged a b)where
  type Measure (Tagged a b) = b

  measure (Tagged x) = x

instance (Measured a , Measured b)=>Measured (a,b) where
  type Measure (a,b) = (Measure a,  Measure b)
  measure (x, y) = (measure x, measure y)

data Node v a = Node2 v a a | Node1 a deriving (Eq, Ord, Show)
data Tree v a = Single a | Branch (Tree v (Node v a) ) deriving (Eq, Ord, Show)

build::(Measured a, v ~ Measure a) =>[a]->Tree v a
build [x] = Single x
build xs = Branch . build $ pairwise xs

pairwise::(Measured a, v ~ Measure a)=>[a]->[Node v a]
pairwise [] = []
pairwise [x] = [Node1 x]
pairwise (x:y:rest) = Node2 (measure x <> measure y) x y : pairwise rest

instance Num a=>Measured (Sum a) where
  type Measure (Sum a) = Sum a
  measure = id

instance Functor (Node v) where
  fmap f (Node1 x) = Node1 (f x)
  fmap f (Node2 v x y) = Node2 v (f x) (f y)

instance Functor (Tree v) where
  fmap f (Single x) = Single (f x)
  fmap f (Branch t) = Branch (fmap (fmap f) t)

mapStructN::(a->b)->(v->w)->Node v a->Node w b
mapStructN f _ (Node1 x) = Node1 (f x)
mapStructN f g (Node2 v x y) = Node2 (g v) (f x) (f y)

mapStructT::(a->b)->(v->w)->Tree v a->Tree w b
mapStructT f _ (Single x) = Single (f x)
mapStructT f g (Branch tree) = Branch (mapStructT f' g tree) where
  f' = mapStructN f g

request::(Measured a, v ~ Measure a, Show v)=>(v->Bool)->(v->Bool)->Tree v a->v
request ptake pdrop  = go where
  err v = error $ "incomplete comparison" ++ show v

  report v = v

  filt v rec
    | ptake v   = seq  (report v) v
    | pdrop v   = mempty
    | otherwise = rec

  filterr v = filt v (err v)

  go (Single (measure -> v)) = filterr v

  go (Branch tree) = request ptake pdrop $ fmap recur tree where
    recur (Node1 x) = tagSelf $ filterr (measure x)
    recur (Node2 v x y) = tagSelf $ filt v $ filterr (measure x) <> filterr (measure y)

update::(Measured a, v ~ Measure a) => (v->Bool)->(a->a)->Tree v a->Tree v a
update p f old@(Single x) = if p (measure x)  then Single (f x)  else old
update p f (Branch tree) = Branch $ update p f' tree where
  f' old@(Node1 x) = let m = measure x in if p m then Node1 (f x) else old
  f' old@(Node2 v x y) = if p v then Node2 v' x' y' else old
    where x' = if p (measure x) then f x else x
          y' = if p (measure y) then f y else y
          v' = measure x' <> measure y'

data IndexItem a = IndexItem a Int deriving (Eq, Ord, Show)
instance Measured a=>Measured (IndexItem a) where
  type Measure (IndexItem a) = (First Int, Last Int, Measure a)

  measure (IndexItem a idx) = (First (Just idx), Last (Just idx), measure a)


newtype IndexedTree v a = IndexedTree { getIndexedTree:: Tree (First Int, Last Int, v) (IndexItem a) }

deriving instance (Show a, Show v, v ~ Measure a) => Show (IndexedTree v a)
deriving instance (Eq a, Eq v, v ~ Measure a) => Eq (IndexedTree v a)
deriving instance (Ord a, Ord v, v ~ Measure a) => Ord (IndexedTree v a)

buildIndex::(Measured a, v ~ Measure a) =>[a]->IndexedTree v a
buildIndex xs = IndexedTree . build $ zipWith IndexItem xs [0..]

rangeRequest::(Measured a, v ~ Measure  a, Show v)=>Int->Int->IndexedTree v a->v
rangeRequest from to =
  let ptake (First (Just x), Last (Just y), _ ) =  x >= from &&  y <= to
      ptake _ = False
      pdrop (First (Just x), Last (Just y), _ ) =  y < from || x > to
      pdrop _ = True
      third (_,_,x) = x
  in third. request ptake pdrop. getIndexedTree

indexUpdate::(Measured a, v ~ Measure  a)=>Int->(a->a)->IndexedTree v a->IndexedTree v a
indexUpdate idx u =
  let pcontains (First (Just x), Last (Just y), _ ) =  x <= idx &&  y >= idx
      pcontains _ = False
      f (IndexItem val i) =  IndexItem (u val) i
  in IndexedTree . update pcontains f . getIndexedTree

newtype DebugEl v a = DebugEl a deriving (Eq, Ord, Show)
instance (Measured a, v ~ Measure a) => Measured (DebugEl v a) where
  type Measure (DebugEl v a) = (v, [v])
  measure (DebugEl x) = let m = measure x in (m, [m])

indexDebug::IndexedTree v a->IndexedTree (v, [v]) (DebugEl v a)
indexDebug (IndexedTree tree) = IndexedTree (mapStructT f g tree) where
  g (start, end, v) = (start, end, (v, [v]))
  f (IndexItem x idx) = IndexItem (DebugEl x) idx

showm::Tree String [String]->[String]
showm (Single x) = x
showm (Branch tree) = showm $ fmap ss tree where
  ss (Node1 x) = x
  ss (Node2 v x y) =  v :  node "| " x ++ node "  " y
  node p (s:rest) = ("+-" ++ s): map (p ++) rest
  node _ [] = []

showStruct::(Show a, Show v)=>Tree v a->String
showStruct = unlines . showm . mapStructT (return. show) show

newtype LCM x = LCM {getLCM :: x} deriving (Eq, Ord, Show, Num, Enum, Real, Integral)

instance Integral a=>Monoid (LCM a) where
  mempty = LCM 1
  mappend (LCM x) (LCM y) = LCM $ lcm x y

instance Integral a=>Measured (LCM a) where
  type Measure (LCM a) = LCM a
  measure x = x

data Request = Query Int Int | Update Int Integer deriving Show


base::Integer
base = 1000000007

main::IO ()
main = do
  void getLine
  ns <- fmap (fmap read. words) getLine
  q <- fmap read getLine
  let parse ["Q", read -> i, read -> j] = Query i j
      parse ["U", read -> i, read -> x] = Update i x
      parse _ = undefined
      -- debug = putStrLn . showStruct . getIndexedTree
  qs <- replicateM q $ fmap (parse. words) getLine
  let tree =  buildIndex $ map LCM ns
  -- debug tree
  let handle t c@(Query i j) =  do
        -- print c
        print $  mod (getLCM $ rangeRequest i j t) base
        -- print $ rangeRequest i j $ indexDebug t
        return t
      handle t c@(Update i x) = do
        -- print c
        let t' =  indexUpdate i (* LCM x) t
        -- debug t'
        return t'
  foldM_ handle tree qs
