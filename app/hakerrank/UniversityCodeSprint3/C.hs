module Main where
import           Control.Arrow
import           Control.Monad
import           Control.Monad.State.Strict
import           Data.Map.Strict            (Map)
import qualified Data.Map.Strict            as Map
import           Data.Maybe
import           Data.Tuple
-- import Debug.Trace

steps::Char -> [(Int, Int)]
steps = fromJust . flip lookup [('n', n), ('w', w), ('s', s), ('e', e)] where
    n = [(-1, 0), (0, 1), (0, -1), (1, 0)]
    s = map (first negate) n
    w = map swap n
    e = map (second negate) w

walk::Int->Char->(Int, Int)->[[Int]]
walk n wind (sy, sx)= maze where
  ss = steps wind
  border u = u == (-1) || u == n
  blocked m (y, x) = not (border y || border x || Map.member (y, x) m)
  step x y (t, u)  = (x + t, y + u)
  go i y x m =
    let u = filter (blocked m) $ fmap (step y x) ss
    in case u of
      []        -> m
      (y',x'):_ -> go (i+1) y' x' $ Map.insert (y', x') i m
  result = go 2 sy sx $ Map.singleton (sy, sx) 1
  for = flip fmap
  maze = for [0..n-1] $ \i ->
    for [0..n-1] $ \j ->
      result Map.! (i, j)


res::[([Int], ([[Int]]->[[Int]], String))]
res = [
    ([0,0], (id, "sw")),
    ([0,1], (fmap reverse, "se")),
    ([1,0], (reverse, "nw")),
    ([1,1], (fmap reverse.reverse, "ne"))
 ]

main :: IO ()
main = do
    n <- read <$> getLine
    wind: [] <- getLine
    [y, x] <- fmap read. words <$> getLine
    let maze = walk n wind (y, x)
    forM_ maze $ putStrLn . unwords. fmap show
