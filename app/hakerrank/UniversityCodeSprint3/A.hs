main::IO ()
main = do
    x:o:y:[] <- getLine
    let Just op = lookup o [('+', (+)), ('-', (-))]
    print $ op (read $ x: []) (read$ y: [])