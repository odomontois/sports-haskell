import Control.Monad

main::IO()
main = do
    n <- read <$> getLine
    q <- read <$> getLine
    vs <- replicateM q $ (fmap read . words) <$> getLine
    print $ maximum $ do
        i <- [0..n - 1]
        j <- [0..n - 1]
        return $ sum $ do
            [x, y, w] <- vs
            let r = abs (x - i) `max` abs (y - j)
            return $ max (w - r) 0 