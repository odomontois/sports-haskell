import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.List             (unfoldr)

getInts::IO [Int]
getInts = fmap (unfoldr readOne) BS.getLine where
  readOne s = do
    (x , s') <- BS.readInt s
    return (x, BS.drop 1 s')

getInt2::IO (Int,Int)
getInt2 = do
  line <- BS.getLine
  let Just (x, l') = BS.readInt line
      Just (y, _)  = BS.readInt $ BS.drop 1 l'
  return (x,y)

fruits::(Int, Int, Int)->[Int]->Int
fruits = (length . ) . filter . bounded where
  bounded (tree, from, to) shift =
    let pos = tree + shift in from <= pos && pos <= to
main :: IO ()
main = do
  (s, t) <- getInt2
  (a, b) <- getInt2
  void BS.getLine
  apples <- getInts
  print $ fruits (a, s, t) apples
  oranges <- getInts
  print $ fruits (b, s, t) oranges
