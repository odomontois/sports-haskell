{-# LANGUAGE TupleSections #-}

import           Control.Applicative
import           Control.Arrow
import           Control.Monad
import           Data.Bool
import           Data.Char
import qualified Data.IntMap.Strict  as IntMap

game::String->Bool
game = (||) <$> happy <*> canBeHappy
    where happy = and . (zipWith3 eqs <$> (Nothing :) <*> id  <*> tail ) . prepare
          prepare =  (++ [Nothing]) . fmap Just
          eqs = (flip .) . curry $ uncurry (<*>) . ((((||) <$>) . (==)) *** (==))
          nonUnique = all ( > 1) . count . fmap ord . filter (/= '_')
          canBeHappy = (&&) <$> elem '_' <*> nonUnique
          count = IntMap.elems . IntMap.fromListWith (+) . fmap (, 1::Int)

main::IO ()
main = (read <$> getLine >>= ) . flip replicateM_ $
  getLine >> getLine >>= (putStrLn . bool "NO" "YES" . game)
