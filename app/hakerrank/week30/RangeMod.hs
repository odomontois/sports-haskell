import           Control.Applicative
import           Control.Arrow
import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.List             (find, unfoldr)
import           Data.Maybe
import qualified Data.IntMap.Strict as IntMap
import qualified Data.IntSet

data SmallReq = SmallReq !Int !Int !Int

getInts::IO [Int]
getInts = fmap (unfoldr readOne) BS.getLine where
  readOne s = do
    (x , s') <- BS.readInt s
    return (x, BS.drop 1 s')

getInt::IO Int
getInt = do
  line <- BS.getLine
  let Just (x, _ ) = BS.readInt line
  return x


getInt2::IO (Int,Int)
getInt2 = do
  line <- BS.getLine
  let Just (x, l') = BS.readInt line
      Just (y, _)  = BS.readInt $ BS.drop 1 l'
  return (x,y)

getInt4::IO (Int,Int,Int,Int)
getInt4 = do
  line <- BS.getLine
  let Just (x, l'  ) = BS.readInt line
      Just (y, l'' ) = BS.readInt $ BS.drop 1 l'
      Just (z, l''') = BS.readInt $ BS.drop 1 l''
      Just (w, _)    = BS.readInt $ BS.drop 1 l'''
  return (x,y,z,w)


main = do
 -- (_n, q) <- getInt2
 -- as <- getInts
 -- qs <- replicateM q getInt4
 putStrLn "adasd"
