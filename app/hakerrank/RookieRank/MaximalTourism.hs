{-# LANGUAGE FlexibleContexts #-}
import           Control.Applicative
import           Control.Monad
import           Data.Array.IO
import qualified Data.ByteString.Char8 as BS

getInt2::IO (Int,Int)
getInt2 = do
  line <- BS.getLine
  let Just (x, l') = BS.readInt line
      Just (y, _)  = BS.readInt $ BS.drop 1 l'
  return (x,y)

root::IOUArray Int Int->Int->IO (Int, Int)
root arr i = do
  p <- readArray arr i
  if p < 0 then return (i, p) else do
    (pt, sz) <- root arr p
    when (pt /= p) $ writeArray arr i pt
    return (pt, sz)

merge::IOUArray Int Int->Int->Int->IO ()
merge arr i j = do
  (pi, si) <- root arr i
  (pj, sj) <- root arr j
  unless (pi == pj) $ do
    let assign u v = do
          writeArray arr v u
          writeArray arr u (si + sj)
    if si < sj then assign pi pj else assign pj pi

ini::Int->IO (IOUArray Int Int)
ini n = newArray (1, n) (-1)

main::IO ()
main = do
  (n, m) <- getInt2
  arr <- ini n
  replicateM_ m $ do
    (i, j) <- getInt2
    merge arr i j
  ms <- negate . minimum . filter (< 0) <$> getElems arr
  print ms
