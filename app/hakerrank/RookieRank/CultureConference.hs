import           Control.Monad
import           Data.Array.IArray
import           Data.Array.MArray
import           Data.Array.ST

fromListFold::Ix i=>(a->b->b)->b->(i,i)->[(i,a)]->Array i b
fromListFold comb z bs lst = runSTArray $ do
  arr <- newArray bs z
  forM_ lst $ \(i,x) -> do
    new <- comb x <$> readArray arr i
    writeArray arr i new
  return arr

main :: IO ()
main = undefined
