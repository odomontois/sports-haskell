import           Control.Applicative
import           Control.Monad
import           Data.List           (elemIndex)




main :: IO ()
main = do
  n <- read <$> getLine
  forM_ [0..n-1] $ \i -> do
    l <- elemIndex 'X' <$> getLine
    forM_ l $ \j -> putStrLn $ shows i $ "," ++ show j
