import           Control.Applicative
import           Data.Array.IArray
import           Data.Bits

bestScores::[Integer]->Integer
bestScores xs =
  let n = length xs - 1
      m = 2 ^ length xs - 1
      as = listArray (0, n) xs :: Array Int Integer
      res = listArray (0, m) $ fmap calc [0..m] :: Array Int Integer
      sums = listArray (0, m) $ fmap summ [0..m] :: Array Int Integer
      calc 0 = 0
      calc i = maximum $ trybit i <$> bits i
      trybit i x = let prev = clearBit i x in res ! prev + mod (sums ! prev) (as ! x)
      summ 0 = 0
      summ i = let x = head $ bits i in sums ! clearBit i x + as ! x
      bits i = filter (testBit i) [0..n]
  in res ! m


main :: IO ()
main = do
  _n <- getLine
  xs <- fmap read. words <$> getLine
  print $ bestScores xs
