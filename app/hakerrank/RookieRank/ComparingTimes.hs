{-# LANGUAGE OverloadedStrings #-}

import           Control.Applicative
import           Control.Monad
import           Data.Attoparsec.ByteString.Char8
import qualified Data.ByteString.Char8            as BS

which::BS.ByteString->BS.ByteString
which s = let Right (first, second) = parseOnly pair s in if first < second then "First" else "Second" where
  pair = (,) <$> time <*> (char ' ' *> time)
  time = mkTime <$> decimal <*> (char ':' *> decimal) <*> choice [string "AM", string "PM"]
  mkTime hrs mns pd = (pd, mod hrs 12, mns)

main::IO ()
main = do
  Right n <- parseOnly decimal <$> BS.getLine
  replicateM_ n $ BS.getLine >>= BS.putStrLn . which
