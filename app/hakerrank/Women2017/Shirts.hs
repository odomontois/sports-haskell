{-# LANGUAGE TupleSections #-}

import           Control.Applicative
import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.Monoid
import           Data.List             (foldl', sort, unfoldr)
data Event = Begin | Shirt | End deriving (Eq, Ord, Enum, Bounded)

getInts::IO [Int]
getInts = fmap (unfoldr readOne) BS.getLine where
  readOne s = do
    (x , s') <- BS.readInt s
    return (x, BS.drop 1 s')

getInt::IO Int
getInt = do
  line <- BS.getLine
  let Just (x, _ ) = BS.readInt line
  return x


getInt2::IO (Int,Int)
getInt2 = do
  line <- BS.getLine
  let Just (x, l') = BS.readInt line
      Just (y, _)  = BS.readInt $ BS.drop 1 l'
  return (x,y)

solution = do
  void BS.getLine
  ps <- fmap (, Shirt) <$> getInts
  v <- getInt
  ss <- (>>= sizes) <$> replicateM v getInt2
  let es = snd <$> sort (ps <> ss)
  print . snd $ foldl' count (0, 0) es
  where sizes (begin, end) = [(begin, Begin), (end, End)]
        count (0  , acc) Shirt = (0, acc)
        count (man, acc) evt   = case evt of
          Shirt -> (man    , acc + 1)
          Begin -> (man + 1, acc    )
          End   -> (man - 1, acc    )

main = do
    q <- getInt
    replicateM_ q solution
