import           Control.Monad
import           Data.Bits
import           Data.Set      (fromList, toList)


cycles::[a]->[[a]]
cycles = take . length <*> iterate ((++) <$> tail <*> return. head)

semiPanDigital::Int->Int->[Int]
semiPanDigital n = go [1..n] where go _ 0 = [0]
                                   go ds k = do
                                     (d: ds') <- cycles ds
                                     num <- go ds' (k - 1)
                                     return (num * 10 + d)

digitset::Int->Int
digitset 0 = 0
digitset n = digitset (div n 10) `setBit` mod n 10

unusual::Int->[((Int, Int), Int)]
unusual n = do
    let k = (n + 1) `quot` 2
        rlow = 10 ^ (n - k - 1)
        rhigh = 10 ^ (n - k)
        full = 2 ^ (n + 1) - 2
        ps = take (quot k 2) $ iterate (*10) 10
    ab <- semiPanDigital n k
    p <- ps
    let (a, b) = divMod ab p
        c = a * b
    guard $ c >= rlow && c < rhigh && (digitset ab `xor` digitset c) == full
    return ((a, b), c)

unique::Ord a=>[a]->[a]
unique = toList . fromList

main::IO ()
main = read <$> getLine >>= print . sum . unique . fmap snd . unusual
