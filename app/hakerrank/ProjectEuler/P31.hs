import           Control.Monad
import qualified Data.ByteString.Char8       as BS
import           Data.Vector.Unboxed.Mutable
import           Prelude                     hiding (read)

import Data.

getInt::IO Int
getInt = do
  line <- BS.getLine
  let Just (x, _ ) = BS.readInt line
  return x

coinWays::IO (IOVector Int)
coinWays = do
  v <- new $ bound + 1
  write v 0 1
  forM_ coins $ \coin ->
    forM_ [0..bound - coin] $ \i -> do
      cur <- read v i
      modify v (plus cur) (i + coin)
  return v
  where bound = 10^(5 :: Int)
        coins = [1,2,5,10,20,50,100,200]
        base  = 10^(9 :: Int) + 7
        plus a b = mod (a + b) base

main :: IO ()
main = do
  t <- getInt
  ways <- coinWays
  replicateM_ t $ do
    n <- getInt
    x <- read ways n
    print x
