{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies        #-}

import           Control.Monad
import           Control.Monad.ST
import           Control.Monad.State.Strict
import           Data.Int
import           Data.Vector.Unboxed.Mutable (MVector)
import qualified Data.Vector.Unboxed.Mutable as MVec
import           Debug.Trace
import           Prelude                     hiding (read)
import           Text.Printf

type Finding s = StateT (MVector s (Int64, Int), Int) (ST s)

findElems::Int->Int64->Finding s ()
findElems = go where
  go::Int->Int64->Finding s ()
  go 0 _ = return ()
  go k x = do
    (xx, v) <- get
    u <- count v (MVec.length xx) x 0
    -- traceM $ printf "counted %i for %i starting from %i" u x v
    if u == 1 then do
      push x
      findElems (k - 1) (x + 1)
    else do
      -- traceShowM (x, u)
      findElems k (x + 1)

count::Int->Int->Int64->Int->Finding s Int
count i n x k | i == n || k > 1 = return k
              | otherwise       = do
  (xx, v) <- get
  (y, j) <- lift $ MVec.read xx i
  -- traceM $ printf "count [%i] = %i from %i in %i .. %i have %i" i y j v n k
  s <- lift $ search i j (x - y) xx
  t <- case s of
    Nothing -> do
      put (xx, v + 1)
      return 0
    Just (z, j') -> do
      MVec.write xx i (y, j')
      return $ if y + z > x then 0 else 1
  count (i + 1) n x (k + t)

search::Int->Int->Int64->MVector s (Int64, Int)->ST s (Maybe (Int64, Int))
search i j x xx| i == j = do
    -- traceM $ printf "dumped %i searching for %i" i j
    return Nothing
               | otherwise = do
    (y, _) <- MVec.read xx j
    -- traceM $ printf "checking if [%i] = %i >= %i" j y x
    if y >= x
      then return $ Just (y, j)
      else search i (j + 1) x xx

push::Int64->Finding s ()
push y = do
  (xx, v) <- get
  let l = MVec.length xx
  xx' <- lift $ MVec.grow xx 1
  lift $  MVec.write xx' l (y, 0)
  put (xx', v)
  -- traceM $ printf "push [%i] = %i " l y


--
--
--
ulam::[Int64]->Int->[Int64]
ulam xs n = runST $ do
  let m = length xs
  xx <- MVec.new m
  zipWithM_ (MVec.write xx) [0..] $ zip xs (repeat 0)
  let y = maximum xs + 1
      finding = findElems (n - m) y
  (xx', _ ) <- execStateT finding (xx, 1)
  ys <- traverse (MVec.read xx' ) [0..n - 1]
  return $ fmap fst ys

main :: IO ()
main = do
  let ls = ulam [2,9] 400
  let us = zipWith (-) (tail ls) ls
  print ls
  print us
