{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TupleSections    #-}

import           Control.Applicative
import           Control.Monad.ST.Safe
import           Control.Monad.State.Strict
import           Data.Array.ST.Safe
import           Data.Array.Unboxed
import qualified Data.ByteString.Char8      as BS
import           Data.List                  (unfoldr)
import           Data.STRef

type Cost = Integer

data Edge      = Edge !Cost !Int deriving (Eq, Ord, Show)
type EdgeArr s = STArray s Int [Edge]
type Graph     = Array Int [Edge]
type SegArr  s = STUArray s Int Int


data Prio  s   = Prio (STArray s Int Cost) (STRef s Int) (STUArray s Int Int) (STUArray s Int Int)

putPrio::Prio s->Int->Int->ST s ()
putPrio (Prio _ _ arr idx) i x = do
  writeArray arr i x
  writeArray idx x i

newPrio::STArray s Int Cost->ST s (Prio s)
newPrio values = do
    bs <-  getBounds values
    let n  = rangeSize bs
    len <- newSTRef 0
    arr <- newArray_ (0, n)
    idx <- newArray  bs (-1)
    return $ Prio values len arr idx

pushPrio::Prio s->Int->ST s ()
pushPrio prio@(Prio _ ref _ _) x = do
  n <- readSTRef ref
  writeSTRef ref (n + 1)
  putPrio prio n x
  fixUp prio n

deletePrio::Prio s->Int->ST s ()
deletePrio prio@(Prio val ref arr idx) x = do
  n <- readSTRef ref
  writeSTRef ref (n - 1)
  i <- readArray idx x
  xv <- readArray val x
  w <- readArray arr (n - 1)
  wv <- readArray val w
  putPrio prio i w
  case compare xv wv of
    EQ -> return ()
    LT -> fixDown prio i
    GT -> void $ fixUp prio i

pullPrio::Prio s->ST s Int
pullPrio prio@(Prio _ ref arr _) = do
  n <- readSTRef ref
  writeSTRef ref (n - 1)
  x <- readArray arr 0
  w <- readArray arr (n - 1)
  putPrio prio 0 w
  fixDown prio 0
  return x

nullPrio::Prio s->ST s Bool
nullPrio (Prio _ ref _ _) = (== 0) <$> readSTRef ref

fixUp::Prio s->Int->ST s ()
fixUp _ 0     = return ()
fixUp prio@(Prio val _ arr _) start  = do
  x <- readArray arr start
  xv <- readArray val x
  let go 0 = return 0
      go i = do
        let p = quot (i - 1)  2
        px <- readArray arr p
        pv <- readArray val px
        if xv >= pv then return i else putPrio prio i px >> go p
  top <- go start
  unless (top == start) $ putPrio prio top x


fixDown::Prio s->Int->ST s ()
fixDown prio@(Prio val ref arr _) start = do
  size <- readSTRef ref
  x <- readArray arr start
  xv <- readArray val x
  let go i | 2 * i + 1 >= size = return i
           | 2 * i + 2 == size = do
             let c = 2 * i + 1
             cx <- readArray arr c
             cv <- readArray val cx
             if xv <= cv then return i else putPrio prio i cx >> return c
           | otherwise  = do
            let l = 2 * i + 1
                r = 2 * i + 2
            lx <- readArray arr l
            rx <- readArray arr r
            lv <- readArray val lx
            rv <- readArray val rx
            let goLeft  = putPrio prio i lx >> go l
                goRight = putPrio prio i rx >> go r
            if xv <= lv then
              if xv <= rv then return i else goRight
            else
              if lv <= rv then goLeft else goRight
  bottom <- go start
  unless(bottom == start) $ putPrio prio bottom x


getInts::IO [Int]
getInts = fmap (unfoldr readOne) BS.getLine where
  readOne s = do
    (x , s') <- BS.readInt s
    return (x, BS.drop 1 s')

addEdge::Int->Int->Cost->EdgeArr s->ST s ()
addEdge a b p arr = do
  lst <- readArray arr a
  writeArray arr a $ Edge p b : lst


buildSegTree::Int->ST s (SegArr s, EdgeArr s)
buildSegTree n = do
  edges <- newArray (-maxIdx, maxIdx) []
  segs  <- newArray_ (0, n)
  let link l r i j = do
        addEdge i j 0 edges
        addEdge (-j) (-i) 0 edges
        go l r j
      go l r idx | l+1>=r    = do
                    writeArray segs r idx
                    addEdge idx (-idx) 0 edges
                 | otherwise = do
                   let mid = quot (l + r + 1) 2
                   link l mid idx (2*idx)
                   link mid r idx (2*idx + 1)
  go 0 n 1
  return (segs, edges)
  where
    calcMax l r u | l + 1 >= r = u
                  | otherwise = let m = quot (l+r+1) 2
                                in calcMax l m (2*u) `max` calcMax m r (2*u+1)
    maxIdx = calcMax 0 n 1

multiEdge::Bool->Int->Int->Int->Int->Cost->SegArr s->EdgeArr s->ST s ()
multiEdge inverse n start endL endR c seg edge = go 0 n 1 where
  go l r idx | l >= endR || r < endL   = return () -- fully outside
             | l >= endL - 1 && r <= endR = do        -- fully inside
                 u <- readArray seg start
                 unless inverse $ addEdge (-u) idx c edge
                 when   inverse $ addEdge (-idx) u c edge
             | otherwise              = do
                let mid = quot (l + r + 1) 2
                go l mid  (idx*2)
                go mid r  (idx*2 + 1)

buildGraph::Int->[[Int]]->(Array Int [Edge], UArray Int Int)
buildGraph n queries = runST $ do
  (seg, edge) <- buildSegTree n
  let runQuery [1, v, u, w]    = do
        f <- readArray seg v
        t <- readArray seg u
        addEdge (- f) t (fromIntegral w) edge
      runQuery [2, v, l, r, w] = multiEdge False n v l r (fromIntegral w) seg edge
      runQuery [3, v, l, r, w] = multiEdge True  n v l r (fromIntegral w) seg edge
      runQuery _               = return ()
  forM_ queries runQuery
  graph <- freeze edge
  segMap <- freeze seg
  return (graph, segMap)

dijkstra::Graph->Int->Array Int Cost
dijkstra graph s = runSTArray $ do
  best <- newArray (bounds graph) (-1)
  prio <- newPrio best
  writeArray best s 0
  pushPrio prio s
  let go = do
         end <- nullPrio prio
         unless end $ do
           num  <- pullPrio prio
           dist <- readArray best num
           let edges = graph ! num
           check dist edges
           go
      better _ (-1)  = True
      better price x = price < x
      check _   [] = return ()
      check acc (Edge cost to: rest) = do
        let price = acc + cost
        current <- readArray best to
        when (price `better` current) $ do
          when (current /= -1) $ deletePrio prio to
          writeArray best to price
          void $ pushPrio prio to
        check acc rest
  go
  return best

main :: IO ()
main = do
  [n,q,s] <- getInts
  qs <- replicateM q getInts
  let (graph, segMap)  = buildGraph n qs
  -- forM_ (assocs graph) $ \(i, es) -> putStrLn $ show i <> ": " <> show es
  let best = dijkstra graph $ segMap ! s
  putStrLn $ unwords  [show $ best ! (segMap ! i) | i <- [1..n]]
--
-- validatePrio::Prio s->ST s Bool
-- validatePrio (Prio cmp ref arr _) = do
--   size <- readSTRef ref
--   let go i | 2 *i + 1>= size = return True
--            | 2* i + 2 == size = do
--         x <- readArray arr i
--         c <- readArray arr (2 * i + 1)
--         res <- cmp x c
--         unless res $ traceShowM $ show (x, c)
--         return res
--            | otherwise = do
--         x <- readArray arr i
--         l <- readArray arr (2 * i + 1)
--         r <- readArray arr (2 * i + 2)
--         vl <- cmp x l
--         vr <- cmp x r
--         unless vl $ traceShowM $ show (x, l)
--         unless vr $ traceShowM $ show (x, r)
--         vvl <- go (2 * i + 1)
--         vvr <- go (2 * i + 2)
--         return $ vl && vr && vvl && vvr
--   go 0
