{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ViewPatterns     #-}

import           Control.Applicative
import           Control.Monad.ST.Safe
import           Control.Monad.State.Strict
import           Data.Array.ST.Safe
import           Data.Array.Unboxed
import qualified Data.ByteString.Char8      as BS
import           Data.Int
import           Data.List                  (unfoldr)
import           Data.Maybe
import qualified Data.Set                   as Set

type Cost = Int64

data Edge      = Edge {-# UNPACK #-} !Cost {-# UNPACK #-}  !Int deriving (Eq, Ord, Show)
type Graph  s  = STArray s Int EdgeList
type SegArr  s = STUArray s Int Int
data EdgeList  = EdgeCons {-# UNPACK #-}  !Cost {-# UNPACK #-} !Int !EdgeList | EdgeNil deriving (Eq, Ord)

type CostQue = Set.Set Edge

queInit::Int->CostQue
queInit s = Set.singleton $ Edge 0 s

quePull::CostQue->Maybe (Edge, CostQue)
quePull = Set.minView

quePush::Edge->CostQue->CostQue
quePush = Set.insert

queDel::Edge->CostQue->CostQue
queDel = Set.delete

getInts::IO [Int]
getInts = fmap (unfoldr readOne) BS.getLine where
  readOne s = do
    (x , s') <- BS.readInt s
    return (x, BS.drop 1 s')

addEdge::Int->Int->Cost->Graph s->ST s ()
addEdge a b p arr = do
  lst <- readArray arr a
  writeArray arr a $ EdgeCons p b lst


buildSegTree::Int->ST s (SegArr s, Graph s)
buildSegTree n = do
  edges <- newArray (-maxIdx, maxIdx) EdgeNil
  segs  <- newArray_ (0, n)
  let link l r i j = do
        addEdge i j 0 edges
        addEdge (-j) (-i) 0 edges
        go l r j
      go l r idx | l+1>=r    = do
                    writeArray segs r idx
                    addEdge idx (-idx) 0 edges
                 | otherwise = do
                   let mid = quot (l + r + 1) 2
                   link l mid idx (2*idx)
                   link mid r idx (2*idx + 1)
  go 0 n 1
  return (segs, edges)
  where
    calcMax l r u | l + 1 >= r = u
                  | otherwise = let m = quot (l+r+1) 2
                                in calcMax l m (2*u) `max` calcMax m r (2*u+1)
    maxIdx = calcMax 0 n 1

multiEdge::Bool->Int->Int->Int->Int->Cost->SegArr s->Graph s->ST s ()
multiEdge inverse n start endL endR c seg edge = go 0 n 1 where
  go l r idx | l >= endR || r < endL   = return () -- fully outside
             | l >= endL - 1 && r <= endR = do        -- fully inside
                 u <- readArray seg start
                 unless inverse $ addEdge (-u) idx c edge
                 when   inverse $ addEdge (-idx) u c edge
             | otherwise              = do
                let mid = quot (l + r + 1) 2
                go l mid  (idx*2)
                go mid r  (idx*2 + 1)

buildGraph::Int->[[Int]]->ST s (Graph s, SegArr s)
buildGraph n queries = do
  (seg, edge) <- buildSegTree n
  let runQuery [1, v, u, w]    = do
        f <- readArray seg v
        t <- readArray seg u
        addEdge (- f) t (fromIntegral w) edge
      runQuery [2, v, l, r, w] = multiEdge False n v l r (fromIntegral w) seg edge
      runQuery [3, v, l, r, w] = multiEdge True  n v l r (fromIntegral w) seg edge
      runQuery _               = return ()
  forM_ queries runQuery
  return (edge, seg)

dijkstra::Graph s->Int->ST s (STArray s Int (Maybe Cost))
dijkstra graph s = do
  bs <- getBounds graph
  best <- newArray bs Nothing
  writeArray best s (Just 0)
  let go (quePull -> Just (Edge dist num, rest)) = do
           edges <- readArray graph num
           dists <- check dist rest edges
           go dists
      go _ = return ()
      check acc dists (EdgeCons cost to rest) = do
        let price = acc + cost
        current <- readArray best to
        if maybe True (price < ) current then do
          let dists' = maybe dists remove current
              remove u = queDel (Edge u to) dists
          writeArray best to (Just price)
          let dists'' = quePush (Edge price to) dists'
          check acc dists'' rest
        else check acc dists rest
      check _   dists  EdgeNil = return dists
  go $ queInit s
  return best

main :: IO ()
main = do
  [n,q,s] <- getInts
  qs <- replicateM q getInts
  let best = runSTArray $ do
        (graph, segMap)  <- buildGraph n qs
        si <- readArray segMap s
        res <- dijkstra graph si
        arr <- newArray_ (1, n)
        forM_ [1..n] $ \i -> do
           segI <- readArray segMap i
           resI <- readArray res segI
           writeArray arr i resI
        return arr
  putStrLn $ unwords [show $ fromMaybe (-1) $ best ! i | i <- [1..n]]
