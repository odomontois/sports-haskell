import           Control.Applicative
import           Control.Monad
import           Data.List           (intersect)

main = replicateM 2 (fmap read . words <$> getLine) >>= \[[a,b],[c,d]]-> print $ head $ intersect [d, d+c..d+ 100*c] [b,b+a..b+100*a] ++[-1]
