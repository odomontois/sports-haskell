{-# LANGUAGE ViewPatterns #-}
import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.IntSet           (fromList, intersection, null)
import           Data.List             (partition, unfoldr)
import           Prelude               hiding (null)

getInts::IO [Int]
getInts = fmap (unfoldr readOne) BS.getLine where
  readOne s = do
    (x , s') <- BS.readInt s
    return (x, BS.drop 1 s')

main :: IO ()
main = do
  [_, m] <- getInts
  groups <- replicateM m getInts
  putStrLn $ if any traitor groups then "YES" else "NO" where
    traitor nums = let (pos, fmap negate -> neg) = partition (> 0) $ tail nums
                   in null (fromList pos `intersection` fromList neg)
