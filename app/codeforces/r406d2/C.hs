{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TupleSections         #-}

import           Control.Applicative
import           Control.Monad
import           Control.Monad.ST.Safe
import           Data.Array.ST.Safe
import           Data.Array.Unboxed
import qualified Data.ByteString.Char8 as BS
import           Data.Int
import           Data.List             (genericLength, unfoldr)
import           Data.Monoid

getInts::IO [Int]
getInts = fmap (unfoldr readOne) BS.getLine where
  readOne s = do
    (x , s') <- BS.readInt s
    return (x, BS.drop 1 s')

getInt::IO Int
getInt = do
  line <- BS.getLine
  let Just (x, _ ) = BS.readInt line
  return x


data Player = Rick|Morty deriving (Eq, Ord, Show, Ix)
type Arr s a = STUArray s (Player, Int) a
type STArr a = forall s. ST s (Arr s a)

result::Int8->String
result 1    = "Win"
result (-1) = "Lose"
result 0    = "Loop"
result _    = undefined

next::Player->Player
next Rick  = Morty
next Morty = Rick

decArray::(Num b, MArray a b m, Ix i)=>a i b->i->m b
decArray arr i = do
  x <- readArray arr i
  writeArray arr i (x - 1)
  return (x - 1)

calcGame::Int->[Int]->[Int]->UArray (Player, Int) Int8
calcGame n ricks mortys =
  let bnd   = ((Rick, 0), (Morty, n - 1))
      moves Rick  = ricks
      moves Morty = mortys
      mkGames = newArray_ bnd :: STArr Int8
      losesList = replicate n . genericLength
      mkLoses = newListArray bnd $ losesList ricks <> losesList mortys:: STArr Int16
      move i k = mod (i - k) n
 in runSTUArray $ do
   games <- mkGames
   loses <- mkLoses
   let win p lost = do
         let findWin won x = do
               state <- readArray games (p, x)
               if state /= 0 || x == 0 then return won else do
                 writeArray games (p, x) 1
                 return (x: won)
         won <- foldM findWin [] $ move <$> lost <*> moves p
         unless (null won) $ lose (next p) won
       lose p won = do
         let findLose lost x = do
              current <- decArray loses (p, x)
              state <- readArray games (p, x)
              if current /= 0 || x == 0 || state /= 0 then return lost else do
                writeArray games (p, x) (-1)
                return (x : lost)
         lost <- foldM findLose [] $ move <$> won <*> moves p
         unless (null lost) $ win (next p) lost
   win Morty [0]
   win Rick  [0]
   return games



main :: IO ()
main = do
  n <- getInt
  (_: ricks) <- getInts
  (_: mortys) <- getInts
  let game = calcGame n ricks mortys
      output p = putStrLn $ unwords $ (result.(game !).(p, )) <$> [1..n-1]
  output Rick
  output Morty
