{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}

import           Control.Applicative      ((<$>))
import           Control.Monad
import           Control.Monad.RWS.Strict
import           Data.Foldable            (toList)
import           Data.Sequence            (Seq, ViewL (..), (|>))
import qualified Data.Sequence            as Seq

type Index = Int
type Time = Integer
type Queue = Seq (Index, Time)
type Serve = (Time, Queue)
type Events a = RWS Int Queue Serve  a

dispatch::MonadWriter Queue m=>Int->Integer->m ()
dispatch = (tell.). (Seq.singleton.). (,)

rollout::(MonadWriter Queue m, MonadState Serve m)=>Maybe Time->m ()
rollout now = do
  (time, q) <- get
  let happen d = maybe True (>= time + d) now
  case Seq.viewl q of
    Seq.EmptyL -> case now of
      Nothing -> return ()
      Just time' -> put (time' ,q)
    (idx, duration) :< rest
      | happen duration -> do
        let time' = time + duration
        dispatch idx time'
        put (time', rest)
        rollout now
      | otherwise  -> return ()

handle::(MonadReader Int m, MonadWriter Queue m, MonadState Serve m)=>((Time,Time),Index)->m ()
handle ((time, duration), idx) = do
  rollout (Just time)
  b  <- ask
  (t', q') <- get
  if Seq.length q' > b then dispatch idx (-1)
  else put (t',  q' |> (idx, duration) )

walk::Int->[((Time,Time),Index)]->[Time]
walk b queries = let
  go = mapM_ handle queries >> rollout Nothing::Events ()
  res = snd $ execRWS go b (0, Seq.empty)
  in toList $ snd <$> Seq.sort res

readPair::Read a=>IO (a,a)
readPair = fmap (unlist . map read . words) getLine where
    unlist [x,y] = (x,y)
    unlist _     = error "trouble input"

main::IO ()
main = do
  (n,b) <- readPair
  nums <- replicateM n readPair
  let queries =  nums `zip` [1..]
  putStrLn $ unwords $ show <$> walk b queries
