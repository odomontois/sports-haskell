import Control.Applicative
import Control.Monad
import Data.List

splitBy::Int->[a]->[[a]]
splitBy _ [] = []
splitBy n xs = let (pref, rest) = splitAt n xs in pref: splitBy n rest


solution::Int->Int->Int->[[Int]]
solution n a b
  | n > a * b = [[-1]]
  | otherwise = let
    nums    = splitBy b $ take (a*b)  $ [1..n] ++ repeat 0
    rev _ []          = []
    rev False (x: xs) = x: rev True xs
    rev True (x:xs)   = reverse x : rev False xs
    in case mod b 2 of
      1 -> nums
      _ -> rev False nums

main :: IO ()
main = do
  [n,a,b] <- map read . words <$> getLine
  forM_ (solution n a b) $ putStrLn . intercalate " ". map show
