{-# LANGUAGE BlockArguments #-}

import Control.Monad
import qualified Data.ByteString.Char8 as BS
import Data.List

main :: IO ()
main = do
  [t] <- getInts
  replicateM_ t do
    [n, _] <- getInts
    matrix <- replicateM n getInts
    let steps = free matrix `min` free (transpose matrix)
        winner = ["Vivek", "Ashish"] !! mod steps 2
    putStrLn winner
  where
    free = length . filter (not . elem 1)

getInts :: IO [Int]
getInts = go <$> BS.getLine
  where
    go s = case BS.readInt s of
      Nothing -> []
      Just (x, s') -> x : go (BS.drop 1 s')
