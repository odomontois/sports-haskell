{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}

import Control.Monad
import Data.Bits
import qualified Data.ByteString.Char8 as BS
import Data.List (foldl', scanl', tails)

main :: IO ()
main = do
  BS.getLine
  ns <- getInts
  let ns' = 0:0:ns
      net = init . tails
      res = maximum [x .|. y .|. z | (x : tx) <- net ns', (y : ty) <- net tx, z <- ty]
  print res

getInts :: IO [Integer]
getInts = go <$> BS.getLine
  where
    go s = case BS.readInteger s of
      Nothing -> []
      Just (x, s') -> x : go (BS.drop 1 s')
