{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeApplications #-}

import Control.Monad
import qualified Data.ByteString.Char8 as BS
import Data.Array.Unboxed (array, elems, UArray)
import Data.IntMap.Strict (fromListWith)
import Data.Foldable (toList)

main :: IO ()
main = do
  [n] <- getInts
  a <- readPerm n <$> getInts
  b <- readPerm n <$> getInts
  let dist a b | a >= b = a - b
               | otherwise = n + a - b
      diffs = zipWith dist a b
      counts = fromListWith (+) $ zip diffs $ repeat 1
  print $ maximum $ toList counts

readPerm :: Int -> [Int] -> [Int]
readPerm n xs@(x : t) = elems $ array @UArray (1, n) $ zip xs [1 .. n]

getInts :: IO [Int]
getInts = go <$> BS.getLine
  where
    go s = case BS.readInt s of
      Nothing -> []
      Just (x, s') -> x : go (BS.drop 1 s')
