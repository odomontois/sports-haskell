{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}

import Control.Monad
import Control.Monad.Reader
import Control.Monad.State
import Data.Array.Unboxed
import qualified Data.ByteString.Char8 as BS
import Data.Set as S hiding (elems, filter)

type Lab = UArray (Int, Int) Char

type Walk = ReaderT Lab (State (Set (Int, Int)))

neighbors, neighbors' :: (Int, Int) -> [(Int, Int)]
neighbors (x, y) = [(x + 1, y), (x, y + 1), (x - 1, y), (x, y - 1)]
neighbors' p = p : neighbors p

go :: (Int, Int) -> Walk Int
go p = do
  lab <- ask
  seen <- get
  let good = lab ! p == 'G'
      stop = not (bounds lab `inRange` p) || member p seen || lab ! p == '#'
  if stop
    then return 0
    else do
      modify' $ insert p
      res <- mapM go $ neighbors p
      return $ sum res + fromEnum good


main :: IO ()
main = do
  [t] <- getInts
  replicateM_ t do
    [n, m] <- getInts
    lines <- replicateM n BS.getLine
    let lab = listArray @UArray ((1, 1), (n, m)) $ lines >>= filter (`elem` "#.GB"). BS.unpack
        blacklist = fmap (,'#') . filter (inRange $ bounds lab) . (neighbors' =<<) . fmap fst . filter ((==) 'B' . snd) $ assocs lab
        lab' = lab // blacklist
        goods = length $ filter ((==) 'G') $ elems lab
        found =  go (n, m) `runReaderT` lab' `evalState` empty
        ok = goods == 0 || found == goods
    putStrLn $ if ok then "Yes" else "No"

getInts :: IO [Int]
getInts = go <$> BS.getLine
  where
    go s = case BS.readInt s of
      Nothing -> []
      Just (x, s') -> x : go (BS.drop 1 s')
