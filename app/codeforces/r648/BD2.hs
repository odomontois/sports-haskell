{-# LANGUAGE BlockArguments #-}

import Control.Monad
import qualified Data.ByteString.Char8 as BS
import Data.List

main = do
  [t] <- getInts
  replicateM_ t do
    [n] <- getInts
    a <- getInts
    b <- getInts
    let result = sorted a || (elem 0 b && elem 1 b)
    putStrLn if result then "Yes" else "No"
    where
    sorted xs = all id  $ zipWith (<=) xs (tail xs)

getInts :: IO [Int]
getInts = go <$> BS.getLine
  where
    go s = case BS.readInt s of
      Nothing -> []
      Just (x, s') -> x : go (BS.drop 1 s')