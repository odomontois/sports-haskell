{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}

import Control.Monad
import Control.Monad.State
import qualified Data.ByteString.Char8 as BS
import Data.Set as S
import Data.List as L

perm xs n =
  let (i, t) = L.splitAt n xs
      (j, k) = L.splitAt (length xs - 2 * n) t
   in k ++ j ++ i

go :: [Int] -> State (Set [Int]) ()
go l = do
  seen <- get
  unless (member l seen) do
    modify' $ S.insert l
    mapM_ go [perm l i | i <- [1 .. quot (length l) 2]]

varl :: Int -> Integer
varl = genericLength . S.toList . flip execState S.empty . go . enumFromTo 1

pairs :: Int -> [Int] -> [[Int]]
pairs k xs = let u = L.take k xs
                 v = L.take k $ reverse xs
                 sp x y = sort [x, y]
                 ps = zipWith sp u v
             in sort ps

main :: IO ()
main = do
  [t] <- getInts
  replicateM_ t do
    [n] <- getInts
    a <- getInts
    b <- getInts
    let k = n `quot` 2
        good = pairs k a == pairs k b && (even n || a !! k == b !! k)
    putStrLn $ if good then "Yes" else "No"

getInts :: IO [Int]
getInts = go <$> BS.getLine
  where
    go s = case BS.readInt s of
      Nothing -> []
      Just (x, s') -> x : go (BS.drop 1 s')
