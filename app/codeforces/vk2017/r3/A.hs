import           Control.Monad
import qualified Data.ByteString.Char8 as BS

getInt4::IO (Int,Int, Int, Int)
getInt4 = do
  line <- BS.getLine
  let Just (x1, l1) = BS.readInt line
      Just (x2, l2)  = BS.readInt $ BS.drop 1 l1
      Just (x3, l3) = BS.readInt $ BS.drop 1 l2
      Just (x4, _) = BS.readInt $ BS.drop 1 l3
  return (x1, x2, x3, x4)

getInt::IO Int
getInt = do
  line <- BS.getLine
  let Just (x, _ ) = BS.readInt line
  return x

euler::Integral a=>a->a->a->Maybe (a,a)
euler x 0 u | mod u x == 0 = Just (div u x, 0)
            | otherwise    = Nothing
euler x y u = do
  let (d,r) = divMod x y
  (k, l) <- euler y r u
  return (l, k - d * l)

main:: IO ()
main = do
  t <- getInt
  replicateM_  t $ do
    (x,y,p,q) <- getInt4
    let q' = q - p
        solution u v | u >= 0 && v >= 0 = u + v
                     | otherwise        = solution (u + p) (v + q')
    print $ maybe (-1) (uncurry solution) $ euler q' (-p) (p*y - q'*x)
