{-# LANGUAGE ConstraintKinds  #-}
{-# LANGUAGE FlexibleContexts #-}
import           Control.Applicative
import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.List             (scanl', sortBy)
import           Data.Monoid
import           Data.Ord              (Down (..))
import           Data.Tuple

getInt2::IO (Int,Int)
getInt2 = do
  line <- BS.getLine
  let Just (x, l') = BS.readInt line
      Just (y, _)  = BS.readInt $ BS.drop 1 l'
  return (x,y)

getInt3::IO (Int,Int, Int)
getInt3 = do
  line <- BS.getLine
  let Just (x, l') = BS.readInt line
      Just (y, l'')  = BS.readInt $ BS.drop 1 l'
      Just (z, _) = BS.readInt $ BS.drop 1 l''
  return (x,y,z)

clean::(a->a->Bool)->[(a, b)]->[(a, b)]
clean op = go where
  go ((x1, y1) : (x2, y2) : rest) | op x1 x2  = go ((x1, y1): rest)
                                  | otherwise = (x1, y1):go ((x2, y2): rest)
  go xs = xs

monotone::(Ord a, Ord b)=>[(a, b)]->[(a, b)]
monotone = turn (<=).turn (==) where turn op = reverse. fmap swap .clean op

sums::Monoid a=>[a]->[a]
sums = tail. scanl' mappend mempty

merge::Monoid a=>(a->a->Bool)->[a]->[a]->[a]
merge good = go where
  go [] _ = []
  go _ [] = []
  go (x:xs) (y:ys) | good x y  = x <> y : go xs (y: ys)
                   | otherwise = go (x: xs) ys

main::IO ()
main = do
  (n, m, d') <- getInt3
  let sump (x, y) = (sumi x, sumi y)
      sumi = Sum . toInteger
      best (x1, y1) (x2, y2) = compare (Down x1) (Down x2) <> compare y1 y2
      readOlymp x =  monotone . sums . sortBy best. (sump <$>) <$> replicateM x getInt2
      d = Sum $ toInteger d'
      good (_, w1) (_, w2) = w1 <> w2 <= d
  phs <- readOlymp n
  inf <- readOlymp m
  let results = merge good phs (reverse inf)
      res = getSum . maximum . (0 :) $ fst <$> results
  print res
