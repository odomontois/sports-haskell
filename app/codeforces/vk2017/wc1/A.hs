import qualified Data.ByteString.Char8 as BS
import           Data.List             (genericLength, unfoldr)

getInts::IO [Integer]
getInts = fmap (unfoldr readOne) BS.getLine where
  readOne s = do
    (x , s') <- BS.readInteger s
    return (x, BS.drop 1 s')

main :: IO ()
main = do
  [_,c1,c2] <- getInts
  cs <- getLine
  let adult = genericLength $ filter (== '1') cs
      total = genericLength cs
      price k = let
        (size, large) = divMod total k
        small = k - large
        in cost size * small + cost (size + 1) * large
      cost x = c1 + c2 * (x - 1) ^ (2 :: Int)
      result = minimum $ fmap price [1..adult]
  print result
