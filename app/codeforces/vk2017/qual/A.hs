import           Control.Applicative
import qualified Data.ByteString.Char8 as BS
import           Data.List             (unfoldr)

getInts::IO [Int]
getInts = unfoldr readOne <$> BS.getLine where
  readOne s = do
    (x , s') <- BS.readInt s
    return (x, BS.dropWhile (== ' ') s')

main = do
  getLine
  xs <- getInts
  print $ (maximum xs + minimum xs) `quot` 2
