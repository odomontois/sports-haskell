import           Control.Applicative
import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.List             (length, mapAccumL, sortBy, unfoldr)


getInts::IO [Int]
getInts = unfoldr readOne <$> BS.getLine where
  readOne s = do
    (x , s') <- BS.readInt s
    return (x, BS.dropWhile (== ' ') s')

data Step = Remains Int [(Int, Int)] | Failed deriving Eq

assign::Step->(Int, Int)->(Step, (Int, Int))
assign (Remains cnt knows) (stud, soc) | cnt > 0  =
  let (guy, gsoc) : rest = dropWhile ((==0).snd) knows
      cleared = (guy, gsoc - 1) : rest
      newCnt  = cnt + soc - 1
  in (Remains newCnt cleared,  (guy, stud))
                                       | otherwise = (Failed, undefined)
assign Failed _ = (Failed, undefined)


main :: IO ()
main = do
  void getLine
  (pol: socs) <- getInts
  let studs = filter ((/= 0).snd) $ zip [2..] socs
      start = Remains pol ((1, pol):studs)
      (result, pairs) = mapAccumL assign start studs
  if result == Failed then putStrLn "-1"
    else do
      print $ length pairs
      forM_ pairs (\(from, to) -> putStrLn $ show from ++ " " ++ show to)
