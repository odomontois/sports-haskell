{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}

import           Control.Applicative
import           Control.Monad
import           Data.Array.Unboxed
import           Data.Bits
import qualified Data.ByteString.Char8 as BS
import qualified Data.IntMap.Strict    as IntMap
import           Data.List             (unfoldr)

getInts::IO [Int]
getInts = unfoldr readOne <$> BS.getLine where
  readOne s = do
    (x , s') <- BS.readInt s
    return (x, BS.dropWhile (== ' ') s')

main :: IO ()
main = do
  let top = 2^(14 :: Int) - 1
  [_, k] <- getInts
  xs <- getInts
  let nums = zip xs [1,1..] ++ zip [0..top] [0,0..]
      xm = array (0, top) $ IntMap.toList $ IntMap.fromListWith (+) nums :: Array Int Integer
  if k == 0 then
    print $ sum $ (\x -> x * (x - 1) `quot` 2) <$> elems xm
  else do
    let mutators = filter ((k==).popCount) [0..top] :: [Int]
    print $ sum $ do
        (x, cx) <- assocs xm
        guard $ cx > 0
        mut <- mutators
        let y = xor x mut
        guard $ x > y
        let cy = xm ! y
        return $ cx * cy
