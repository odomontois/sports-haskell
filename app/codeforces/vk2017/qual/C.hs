{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RecordWildCards       #-}

import           Control.Applicative
import           Control.Arrow
import           Control.Monad
import           Data.Array.IArray
import           Data.Array.Unboxed
import qualified Data.IntMap.Strict  as IntMap
import           Data.List           (find, null)

type Pos = (Int, Int)
type Lab = UArray Pos

data Board = Board {lab::Lab Char, start::(Int, Int)}

free, robo::Char->Bool
free = (/= '*')
robo = (== 'X')

base::Int
base = 10000
encode::(Int, Int)->Int
encode (x,y) = x * base + y
decode::Int->Pos
decode u = (quot u base, rem u base)

ways::Board->Pos->[Pos]
ways (Board lab _) (x, y) = let vacant p = inRange (bounds lab) p && free (lab ! p)
                            in filter vacant [(x+1,y),(x,y-1),(x, y+1),(x-1, y)]

command::Pos->Pos->Char
command (x, y) (a, b) = case (a - x, b - y) of
  ( 1, 0) -> 'D'
  ( 0,-1) -> 'L'
  ( 0, 1) -> 'R'
  (-1, 0) -> 'U'

dists::Board->Lab Int
dists board@Board{..} = array (bounds lab) result  where
  initial = IntMap.singleton (encode start) 0
  layer = fmap encode . ways board
  result = fmap (first decode) . IntMap.toList $ go 1 [start] initial
  go k prevs acc =
    let nextSteps = zip (prevs >>= layer) (repeat k)
        nextMap = IntMap.fromList nextSteps `IntMap.difference` acc
        nextAcc = IntMap.union acc nextMap
        nextLst = decode <$> IntMap.keys nextMap
    in if null nextLst then acc else go (k + 1) nextLst nextAcc

cyclic::Board->Int->String
cyclic board = go (start board) where
  dst = dists board
  go _ 0 = ""
  go pos cnt = let Just next = find ((cnt >=).(dst !)) $ ways board pos
               in command pos next : go next (cnt - 1)

printLab::(Show a, IArray UArray a)=>Lab a->IO ()
printLab lab = let ((fx,fy),(tx,ty)) = bounds lab
               in forM_ [fx..tx] $ \x-> print [lab ! (x, y) | y <- [fy..ty]]

main :: IO ()
main = do
  [n,m,k] <- (fmap read . words) <$> getLine
  lab <- listArray ((1,1), (n,m)) . join <$> replicateM n getLine :: IO (Lab Char)
  let Just (start, _) = find (robo.snd) $ assocs lab
      board = Board lab start
      dst = dists board
      possible = mod k 2 == 0 && not (null $ ways board start)
  putStrLn $ if possible then cyclic board k else "IMPOSSIBLE"

  -- printLab lab
  -- print start
  -- print (lab ! start)
  -- printLab dst
