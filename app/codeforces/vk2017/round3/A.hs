module Main where

import           Control.Applicative
import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.Maybe

getInts::IO [Integer]
getInts = go <$> BS.getLine where
  go s = case  BS.readInteger s of
      Nothing      -> []
      Just (x, s') -> x : go (BS.drop 1 s')

getInt::IO Int
getInt = do
  line <- BS.getLine
  let Just (x, _ ) = BS.readInt line
  return x


search::Integer->Integer->Integer->Integer->Maybe Integer
search x y p q = go 0 (y + 1) where
  go u v | u + 1 >= v   = fmap result $ mfilter ok $ Just v
         | ok (mid u v) = go u (mid u v)
         | otherwise    = go (mid u v) v
  ok t = p * t >= x && (q - p) * t >= (y - x)
  mid u v = quot (u + v) 2
  result t = q * t - y



main :: IO ()
main = do
  t <- getInt
  replicateM_ t $ do
    [x,y,p,q] <- getInts
    print $ fromMaybe (-1) $ search x y p q
