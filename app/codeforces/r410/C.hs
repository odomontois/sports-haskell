import           Control.Applicative
import           Control.Arrow
import qualified Data.ByteString.Char8 as BS

getInts::IO [Int]
getInts = go <$> BS.getLine where
  go s = case  BS.readInt s of
      Nothing      -> []
      Just (x, s') -> x : go (BS.drop 1 s')

main = let True  `add` Right c = Left c
           False `add` Right c = Right c
           True  `add` Left  c = Right (c + 1)
           False `add` Left  c = Right (c + 2)
           op = liftA2 (.) (second. add. odd) (first. gcd)
           finish 1 = either (+2) id
           finish _ = const 0
           calc = print. uncurry finish . foldr op (0, Right 0)
       in putStrLn "YES" >> getLine >> getInts >>= calc
