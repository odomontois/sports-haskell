import           Control.Applicative
import           Control.Arrow
import           Control.Monad
import           Data.ByteString.Char8 (ByteString)
import qualified Data.ByteString.Char8 as BS
import           Data.Char
import           Data.Foldable
import           Data.Monoid
import           Data.Traversable

getInt::IO Int
getInt = do
  line <- BS.getLine
  let Just (x, _ ) = BS.readInt line
  return x

shift::Int->ByteString->ByteString
shift i = BS.append <$> BS.drop i <*> BS.take i

newtype Min a= Min{getMin::a} deriving (Eq, Ord, Show)
instance (Bounded a, Ord a) => Monoid (Min a) where
  Min a `mappend` Min b = Min (min a b)
  mempty = Min maxBound

main = do
  n <- getInt
  l1:ls <- replicateM n readLine
  let shifts l = (,) . Min <*> (`shift` l) <$>  [0..BS.length l - 1]
      lastRes = foldM minRevAll (shifts l1) ls
      minRevAll prev l = (fmap . flip (,) . snd <*>  minRev prev) `traverse` shifts l
      minRev prev (i, l) = (+~i)  <$> foldMap (check l) prev
      check l (j, p) = if p == l then Just j else Nothing
      result = maybe (-1) (getMin . foldMap fst) lastRes
      Min a +~ Min b = Min (a + b)
  print result where readLine = BS.takeWhile isAlpha <$> BS.getLine
