import           Control.Applicative
main = getLine >>= putStrLn . (["NO","YES"] !!).fromEnum. ((((||) <$> (==2) <*>).(.(== 0)).(&&).odd.length) <*> (length.filter id.(zipWith (/=)  <*> reverse)))
