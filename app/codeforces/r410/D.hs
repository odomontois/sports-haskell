import           Control.Applicative
import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.List             (sortBy)
import           Data.Ord

getInts::IO [Int]
getInts = go <$> BS.getLine where
  go s = case  BS.readInt s of
      Nothing      -> []
      Just (x, s') -> x : go (BS.drop 1 s')

solution::(Ord a, Ord b)=>[a]->[b]->[Int]
solution as bs = let ((_, x):xs) = sortBy cmpA $ as `zip` bs `zip` [1..]
                     cmpA = comparing $ Down .fst .fst
                     res = x: go xs
                     go (((_, b1), i1): ((_, b2), i2): rest )
                        | b1 > b2   = i1 : go rest
                        | otherwise = i2 : go rest
                     go [(_, i)] = [i]
                     go []       = []
                 in res

main = do
  void getLine
  sol <- solution <$> getInts <*> getInts
  print $ length sol
  putStrLn $ unwords $ show <$> sol
