fact = (m !!) where m =  scanl (*) 1 [1..]
comb n k = fact n  `div` fact k `div` fact (n - k)
solution k = comb k 7 + comb k 6 + comb k 5
main = getLine >>= print . solution. read
