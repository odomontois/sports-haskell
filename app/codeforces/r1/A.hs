main = fmap (fmap read . words) getLine >>= \[n,m,a] -> print $ (n^/a)*(m^/a) where x^/y = quot (x + y - 1) y
