import           Data.List
import           Data.Maybe
pos (x:y) = (fromEnum x - fromEnum 'a' + 1, read y)
pos _ = undefined

main = do
  color <- getLine
  (kx, ky) <- fmap pos getLine
  (cx, cy) <- fmap pos getLine
  let white = color == "white"
      black = color == "black"
      kEats = sort [abs (kx - cx), abs (ky - cy)] == [1,2]
      jumpX = kx + kx - cx
      cEats = ky - cy == 1 &&
              abs (kx - cx) == 1 &&
              ky < 8 &&
              jumpX >= 1 && jumpX <= 8
      full   = ((kx, ky), (cx, cy))
      block  = full == ((2,1),(1,2)) || full == ((7,1),(8,2))
      dame   = cy == 2
  return ()
