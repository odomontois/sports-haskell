import qualified Data.ByteString.Char8 as BS
import           Data.List             (unfoldr)

readOne s = do
      (x , s') <- BS.readInt s
      return (x, BS.dropWhile (== ' ') s')

main = do
  _ <- getLine
  ns <- fmap (unfoldr readOne) BS.getLine
  print $ maximum ns - minimum ns
