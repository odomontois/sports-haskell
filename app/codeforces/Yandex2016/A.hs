import           Data.List

main = do
  name <- getLine
  let (v, c) = partition (`elem` "aieouy") name
  putStrLn $ if v > c then "Vowel" else "Consonant"
