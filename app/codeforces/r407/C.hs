import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.List             (mapAccumL, unfoldr)

getInts::IO [Integer]
getInts = fmap (unfoldr readOne) BS.getLine where
  readOne s = do
    (x , s') <- BS.readInteger s
    return (x, BS.drop 1 s')

odds::[a]->[a]
odds (x:_:rest) = x: odds rest
odds lst        = lst

main :: IO ()
main = do
  void getLine
  xs <- getInts
  let sums = scanl1 (+) xs
      diff (q, s) x = let t = s + q*x in ((-q, t), t)
      diffs =  snd $ mapAccumL diff (-1, 0) $ tail xs
      pairMins = (>>= replicate 2) . scanl1 min . odds
      oddres  = zipWith (+) sums diffs
      evenres = zipWith (-) sums diffs
      oddmins = pairMins oddres
      evenmins = pairMins $ tail evenres
      oddMax = maximum $ zipWith (-) oddres oddmins
      evenMax = maximum $ zipWith (-) evenres evenmins
  print sums
  print diffs
  print oddres
  print evenres
  print oddmins
  print evenmins
  print $ max oddMax evenMax
