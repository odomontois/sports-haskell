import qualified Data.ByteString.Char8 as BS
import           Data.List             (unfoldr)

main :: IO ()
main = do
  [b,q,l,_m] <- getInts
  allow <-fmap ((not.) .flip elem) getInts
  let good num = abs num <= l && allow num
      inf num | good num = "inf"
              | otherwise = "0"
      result | abs b > l = "0"
             | q == 0 && good 0 = "inf"
             | q == 0 && good b = "1"
             | q == 0           = "0"
             | q == 1  = inf b
             | q == -1 = inf b `max` inf (-b)
             | b == 0  = inf 0
             | otherwise = show $ length $ filter allow $ takeWhile ((<= l).abs) $ iterate (* q) b
  putStrLn result
  where
  getInts = fmap (unfoldr readOne) BS.getLine where
    readOne s = do
      (x , s') <- BS.readInteger s
      return (x, BS.drop 1 s')
