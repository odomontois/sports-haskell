{-# LANGUAGE FlexibleContexts #-}
module Main where
import           Control.Applicative
import           Control.Monad
import           Data.Array.IO.Safe
import           Data.Array.Unboxed
import qualified Data.ByteString.Char8 as BS
-- import           Debug.Trace

getInt2::IO (Int,Int)
getInt2 = do
  line <- BS.getLine
  let Just (x, l') = BS.readInt line
      Just (y, _)  = BS.readInt $ BS.drop 1 l'
  return (x,y)

main :: IO ()
main = do
  (n, m) <- getInt2
  let intArray::Int -> IO (IOUArray Int Int)
      intArray = newArray (1, n)
  degree <- intArray 0
  dsParent <- intArray 0
  dsSize <- intArray 1
  seen <- newArray (1, n) False :: IO (IOUArray Int Bool)
  let inc i = do
        cnt <- readArray degree i
        writeArray degree i (cnt + 1)
      parent i = do
        p <- readArray dsParent i
        if p == 0 then return i else do
          p1 <- parent p
          unless (p == p1) $ writeArray dsParent i p1
          return p1
      merge i j = do
        ip  <- parent i
        jp  <- parent j
        unless (ip == jp) $ do
          si  <- readArray dsSize ip
          sj  <- readArray dsSize jp
          let assign a b = do
                writeArray dsParent a b
                writeArray dsSize b (si + sj)
          if si > sj then assign jp ip else assign ip jp
      go 0 loops = return loops
      go x loops = do
        (i, j) <- getInt2
        writeArray seen i True
        writeArray seen j True
        if i /= j
          then do
            inc i
            inc j
            merge i j
            go (x - 1) loops
          else go (x - 1) (loops + 1)
      c2 x = let xi = toInteger x in xi * (xi - 1) `quot` 2
      cntSqs i acc = if  i > n then return acc else do
        cnt <- readArray degree i
        cntSqs (i + 1) (acc + c2 cnt)
      linked i k u | i > n =  return $ k == u
                   | otherwise = do
                     s <- readArray seen i
                     if not s then linked (i + 1) k u else do
                       u' <- if u /= 0 then return u else do
                              p <- parent i
                              readArray dsSize p
                       linked (i + 1) (k + 1) u'
  loops <- go m 0
  lnk <- linked 1 0 0
  if not lnk then print 0 else do
    sqs <- cntSqs 1 0
    print $ sqs + loops * (toInteger m - 1) - c2 loops
