import qualified Data.ByteString.Char8 as BS
import           Data.List             (unfoldr)

main :: IO ()
main = getInts >>= \[n, k] -> getInts >>= print . ql 2.  sum . fmap (ql k) where
  ql k x = quot (x + k - 1) k
  getInts = fmap (unfoldr readOne) BS.getLine where
    readOne s = do
      (x , s') <- BS.readInt s
      return (x, BS.drop 1 s')
