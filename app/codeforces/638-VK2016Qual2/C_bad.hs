{-# LANGUAGE TupleSections #-}

import           Control.Monad      (replicateM)
import           Data.Foldable      (forM_)
import qualified Data.IntMap.Strict as Map
import           Data.List          (mapAccumL)

newtype DI = DI{ getDI ::[(Int, Int)] } deriving Show

empty::DI
empty = DI []

singleton::Int->DI
singleton x = DI [(x,x)]

union::DI->DI->DI
union = (DI .) . (. getDI) . go . getDI where
  go [] xs = xs
  go xs [] = xs
  go xs'@((sx, ex):xs) ys'@((sy, ey):ys)
    | sx > ey + 1 = (sy, ey) : go xs' ys
    | sy > ex + 1 = (sx, ex) : go xs  ys'
    | otherwise   = go ((min sx sy, max ex ey):xs) ys

insert::Int->DI->DI
insert = union . singleton

vacant::DI->Int
vacant (DI [])  = 1
vacant (DI ((1,xe):_)) = xe + 1
vacant _ = 1

reorder::[(Int,Int)]->[((Int,Int), Int)]
reorder xs = go (-1) 1 where
  children = Map.fromListWith (++) $ concat $ zipWith child xs [1..]
  child (p, c) idx = [(p, [(c, idx)]), (c, [(p, idx)])]
  go parent city = let cs = filter notParent $ children Map.! city
                       edge (c, idx) =  ((city, c), idx)
                       notParent (c, _) = c /= parent
                       es  = map edge cs
                       next = concatMap (go city.fst) cs
                   in  es ++ next
main::IO ()
main = do
  n <- fmap read getLine
  let pair [x,y] = (x,y)
      pair _     = undefined
  roads <- replicateM (n-1) $ fmap (pair. map read. words) getLine :: IO [(Int, Int)]
  -- print $ reorder roads
  let (_, days) = mapAccumL repair Map.empty $ reorder roads
      busy  = Map.findWithDefault empty
      repair sched ((x, y), idx) =
        let dx = busy x sched
            dy = busy y sched
            day = vacant $ union dx dy
            dx' = insert day dx
            dy' = insert day dy
        in (Map.insert x dx' $ Map.insert y dy' sched, (day, [idx]))
      sch = Map.fromListWith (++) days
  print $ Map.size sch
  forM_ sch $ \xs -> putStrLn . unwords $ map show $ length xs : xs
