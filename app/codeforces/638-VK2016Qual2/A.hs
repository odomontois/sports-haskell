main::IO ()
main = fmap (map read . words) getLine >>= print . solution where
  solution [n,a] | mod a 2 == 0 =  1 + (n - a) `div` 2
                 | otherwise    =  (a + 1) `div` 2
