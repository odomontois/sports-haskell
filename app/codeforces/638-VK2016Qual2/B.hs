import           Control.Monad
import qualified Data.Map.Strict as Map
import qualified Data.Set        as Set

main :: IO ()
main = do
  n <- fmap read getLine
  strs <- replicateM n getLine
  let seconds = Set.fromList $ concatMap tail strs
      firsts = Set.toList $ Set.difference (Set.fromList $ map head strs) seconds
      conts  = Map.fromList $ concatMap (\xs -> zip xs $ tail xs) strs
      start [] _ = []
      start (f:fs) cs = continue f fs cs
      continue cur fs cs = cur: case Map.lookup cur cs of
        Just next -> continue next fs cs
        Nothing -> start fs cs
  putStrLn $ start firsts conts
