{-# LANGUAGE TupleSections #-}

import           Control.Monad      (replicateM)
import           Data.Foldable      (forM_)
import qualified Data.IntMap.Strict as Map
import           Data.List          (mapAccumL)

newtype DI = DI [(Int, Int)]  deriving Show

empty::DI
empty = DI []

singleton::Int->DI
singleton x = DI [(x,x)]

union::DI->DI->DI
union = run where
  run (DI a) (DI b) = DI (go a b)
  go [] xs = xs
  go xs [] = xs
  go xs'@((sx, ex):xs) ys'@((sy, ey):ys)
    | sx > ey + 1 = (sy, ey) : go xs' ys
    | sy > ex + 1 = (sx, ex) : go xs  ys'
    | otherwise   = collapse (min sx sy, max ex ey) xs ys
  collapse (s, e) ((sx, ex):xs) ys | sx < e = collapse (s, max e ex) xs ys
  collapse (s, e) xs ((sy, ey):ys) | sy < e = collapse (s, max e ey) xs ys
  collapse int xs ys = int : go xs ys

insert::Int->DI->DI
insert = union . singleton

vacant::DI->Int
vacant (DI [])  = 1
vacant (DI ((1,xe):_)) = xe + 1
vacant _ = 1

main::IO ()
main = do
  n <- fmap read getLine
  let pair [x,y] = (x,y)
      pair _     = undefined
  roads <- replicateM (n-1) $ fmap (pair. map read. words) getLine :: IO [(Int, Int)]
  let (_, days) = mapAccumL repair Map.empty roads
      busy  = Map.findWithDefault empty
      repair sched (x, y) =
        let dx = busy x sched
            dy = busy y sched
            day = vacant $ union dx dy
            dx' = insert day dx
            dy' = insert day dy
        in (Map.insert x dx' $ Map.insert y dy' sched, day)
      sch = Map.fromListWith (++) $ zip days $ map return [1..]
  print $ Map.size sch
  forM_ sch $ \xs -> putStrLn . unwords $ map show $ length xs : xs
