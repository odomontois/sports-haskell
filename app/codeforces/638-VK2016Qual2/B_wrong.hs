import           Control.Monad
import           Data.List
import qualified Data.Map.Strict as Map
import qualified Data.Set        as Set


main :: IO ()
main = do
  n <- fmap read getLine
  strs <- replicateM n getLine
  let prereq =  Map.unionsWith Set.union $ map req strs
      req = Map.fromList . snd . mapAccumL prev Set.empty
      prev prevs x = (Set.insert x prevs, (x, prevs))
      go prer | Map.null prer = []
              | otherwise     =
        let (first, _) = Map.findMin $ Map.filter Set.null prer
            prer'      = Map.map (Set.delete first) $  Map.delete first prer
            in first : go prer'
  putStrLn $ go prereq
