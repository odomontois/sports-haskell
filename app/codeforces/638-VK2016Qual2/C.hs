{-# LANGUAGE TupleSections #-}

import           Control.Monad      (join, replicateM)
import           Data.Foldable      (forM_, toList)
import           Data.IntMap.Strict (IntMap, (!))
import qualified Data.IntMap.Strict as Map
import           Data.Sequence      (Seq, (<|), (><))
import qualified Data.Sequence      as Seq

ascSeq::(Bounded a, Enum a, Num a)=>Seq a
ascSeq = Seq.fromList [1..]

fromFoldableWith::Foldable t => (a->a->a)->t (Int, a)-> IntMap a
fromFoldableWith = (.toList) . Map.fromListWith

repair::[(Int,Int)]->IntMap (Seq Int)
repair xs = fromFoldableWith (><)  $ go (-1) (-1) 1 where
  seqx = Seq.fromList xs
  children = fromFoldableWith (><) . join $ Seq.zipWith child seqx ascSeq
  child (p, c) idx = Seq.fromList [point p c idx, point c p idx]
  point a b c = (a, Seq.singleton (b,c))
  go parent pday city =
    let cs   = Seq.filter ((/= parent). fst) $ children ! city
        days = Seq.filter (/= pday) ascSeq
        es   = Seq.zip days $ fmap (return.snd) cs
        next = join $ Seq.zipWith (go city) days $ fmap fst cs
    in  es >< next

main::IO ()
main = do
  n <- fmap read getLine
  let pair [x,y] = (x,y)
      pair _     = undefined
  roads <- replicateM (n-1) $ fmap (pair. map read. words) getLine :: IO [(Int, Int)]
  let sched = repair roads
  print $ Map.size sched
  forM_ sched $ \xs -> putStrLn . unwords . toList . fmap show $ Seq.length xs <| xs
