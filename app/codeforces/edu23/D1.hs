import           Control.Applicative
import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.Int

data Tier = Tier {-# UNPACK #-} !Int {-# UNPACK #-}  !Int64 {-# UNPACK #-} !Int64

getInts::IO [Int]
getInts = go <$> BS.getLine where
  go s = case  BS.readInt s of
      Nothing      -> []
      Just (x, s') -> x : go (BS.drop 1 s')

maxSums::[Int]->Int64
maxSums = go 1 0 [] where
  go _  acc _ [] = acc
  go cx acc l xs@(x: rest) = let x' = fromIntegral x in case l of
    [] -> go 1 (acc + cx * x') [Tier x cx (x' * cx)] rest
    Tier y cy my : ys
      | x >= y    -> go (cx + cy) acc ys xs
      | otherwise -> go 1 (acc + cx * x' + my) (Tier x cx (x' * cx + my) : l) rest

main :: IO ()
main = do
  void getLine
  xs <- getInts
  print $ maxSums xs + maxSums (negate <$> xs)
