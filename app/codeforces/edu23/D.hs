import           Control.Applicative
import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.List

getInts::IO [Integer]
getInts = go <$> BS.getLine where
  go s = case  BS.readInteger s of
      Nothing      -> []
      Just (x, s') -> x : go (BS.drop 1 s')

maxSums::[Integer]->Integer
maxSums = fst. foldl' (next 1) initial where
  initial = (0, [])
  next cx (acc, (y, cy) : ys) x
    | x >= y    = next (cx + cy) (acc, ys) x
    | otherwise = (acc + cx * x + (cx * cy) * y, (x, cx) : (y, cy) : ys)
  next cx (acc, []) x =  (acc + cx * x, [(x, cx)])

main :: IO ()
main = do
  void getLine
  xs <- getInts
  print $  maxSums xs + maxSums (fmap negate xs)
