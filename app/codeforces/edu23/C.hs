import           Control.Applicative
import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.List
import           Data.Tuple

getInt2::IO (Integer,Integer)
getInt2 = do
  line <- BS.getLine
  let Just (x, l') = BS.readInteger line
      Just (y, _)  = BS.readInteger $ BS.drop 1 l'
  return (x,y)


digSum::Integer->Integer
digSum = sum . unfoldr dig where
  dig 0 = Nothing
  dig n = Just (mod n 10, quot n 10)

solution :: Integer -> Integer -> Integer
solution n s = let check u = u >= s + digSum u
                   brute = genericLength $ filter check [s..min n (s + 200)]
                   rest  = max 0 $ n - s - 200
               in  rest + brute


main :: IO ()
main = getInt2 >>= print.uncurry solution
