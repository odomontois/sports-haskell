{-# LANGUAGE TupleSections #-}
import           Control.Applicative
import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.List

getInts::IO [Integer]
getInts = go <$> BS.getLine where
  go s = case  BS.readInteger s of
      Nothing      -> []
      Just (x, s') -> x : go (BS.drop 1 s')

minCnt::(Integer, Integer)->(Integer, Integer)->(Integer, Integer)
minCnt (x, v) (y, w) = case compare x y of
                          EQ -> (x, v + w)
                          LT -> (x, v)
                          GT -> (y, w)

minProds::[Integer]->[(Integer, Integer)]->[(Integer, Integer)]
minProds xs us = let vs = reverse $ scanl1 minCnt $ reverse us
                     prod x (y, v) = (x * y, v)
                 in  zipWith prod xs $ tail vs

solution::[Integer]->Integer
solution xs = let mps1   = minProds  xs $ fmap (, 1) xs
                  mps2   = minProds  xs mps1
                  mm     = foldl1' minCnt mps2
              in snd mm



main :: IO ()
main = do
  void getLine
  xs <- getInts
  print $ solution xs
