import           Control.Applicative
import           Control.Monad
import qualified Data.ByteString.Char8 as BS
import           Data.Maybe


getInts::IO [Int]
getInts = go <$> BS.getLine where
  go s = case  BS.readInt s of
      Nothing      -> []
      Just (x, s') -> x : go (BS.drop 1 s')


main :: IO ()
main = do
  [x1,y1,x2,y2] <- getInts
  [x,y] <- getInts
  let  dx = x1 - x2
       dy = y1 - y2
       xx = steps (x1 - x2) x
       yy = steps (y1 - y2) y
       xy = (-) <$> xx <*> yy
       res  = zeros dx x && isJust xx ||
              zeros dy y && isJust yy ||
              maybe False even xy
  putStrLn  (if res then "YES" else "NO")
  where steps 0 0 = Just 0
        steps _ 0 = Nothing
        steps x y | mod x y == 0 = Just $  quot x y
                  | otherwise    = Nothing
        zeros x y = x == 0 && y == 0
