import Data.List

main::IO ()
main = getLine >> do
  us <- fmap (map read. words) getLine :: IO [Int]
  print $ snd $ foldl' go (Nothing, 0::Int) $ sort us where
    go (Nothing , acc) el   = (Just (el, 1::Int, 0::Int), acc)
    go (Just (p, reps, racc), acc) el
      | p == el && racc > 0 = (Just (el, reps + 1, racc - 1), acc + 1)
      | p == el             = (Just (el, reps + 1, racc    ), acc)
      | otherwise           = (Just (el, 1, racc + reps - 1), acc + 1)
