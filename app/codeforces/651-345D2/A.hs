main :: IO ()
main = getLine >>= print .playtime. map read .words where
  playtime [x,y] = go x y
  playtime _     = undefined
  go::Integer->Integer->Integer
  go c1 c2 | c1 < 2  && c2 < 2  = 0
           | c1 ==0  || c2 == 0 = 0
           | c1 < c2            = 1 + go (c1 + 1) (c2 - 2)
           | otherwise          = 1 + go (c1 - 2) (c2 + 1)
