{-# LANGUAGE TupleSections #-}

import           Control.Monad
import Data.List
import Data.Ord

data SP = SP{
  sfst::{-# UNPACK #-} !Int,
  ssnd::{-# UNPACK #-} !Int
}deriving (Eq, Ord)

data ST a = ST !a {-# UNPACK #-} !Int Integer

countBy::Eq a=>(b->a)->[b]->Integer
countBy f = walk where
  thrd (ST _ _ x) = x
  walk (x:xs) = thrd $ foldl' go (ST x 1 0) xs
  walk []     = 0
  go (ST y c acc) x
    | f x == f y = ST x (c+1) (acc + fromIntegral c)
    | otherwise  = ST x 1   acc

main::IO ()
main = do
  n <- fmap read getLine
  let lsp [x,y] = SP x y
      lsp _     = undefined
  es <- replicateM n $ fmap  (lsp .map read . words)  getLine::IO [SP]
  let xs = sort                    es
      ys = sortBy (comparing ssnd) xs
  print (countBy sfst xs + countBy ssnd ys - countBy id xs)
