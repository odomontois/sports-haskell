import           Data.Int
dav s = u where u = s: s +1: zipWith (+) u (tail u)

main = getLine >>= print . ((2::Int32) ^) . read
