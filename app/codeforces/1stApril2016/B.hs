import           Control.Monad
import           Data.Function
import qualified Data.IntSet   as IntSet
import           Data.List
import           Text.Printf
import Control.Applicative

check::Integral a=>a->a->[(a,a)]->[a]
check b d lst = nub $ elems ++ promote where
  elems =  map fst nice
  (nice, bad) = partition ((== 0).(`mod` b).snd) lst
  promote | b <= d = []
          | otherwise = [j* quot b d + i | i <- check (quot b d) d bad, j <- [0..d-1]]

checkAll::Integral a=>[(a,a)]->Double
checkAll xs = go 16 2 ** go 9 3 ** go 5 5 ** go 7 7 ** go 11 11 ** go 13 13 where
  x ** y = x + y - x * y
  (//) = (/) `on` fromIntegral
  go i j = genericLength (check i j xs) // i

main::IO ()
main = do
  void getLine
  let readNums = fmap (fmap read . words) getLine
  xs <- flip zip <$> readNums <*> readNums
  putStrLn $ printf "%0.6f" $ checkAll xs
