game "8<" "[]" = 1
game "()" "8<" = 1
game "[]" "()" = 1
game a b | a == b = 0
         | otherwise = - game b a

pairs [] = []
pairs (x:y:rest) = [x,y]:pairs rest

main = do
  a <- fmap pairs getLine
  b <- fmap pairs getLine
  putStrLn $ case sum (zipWith game a b) `compare` 0  of
    GT -> "TEAM 1 WINS"
    EQ -> "TIE"
    LT -> "TEAM 2 WINS"
