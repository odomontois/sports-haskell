import           Control.Applicative
import           Control.Monad
import           Data.Either

main:: IO ()
main = do
  ls <- replicateM 4 (fmap read. words <$> getLine)
  let (ped, car) = partitionEithers $ do
        (i, l) <- zip [0..] ls
        (j, c) <- zip [1..] l
        case (c, j) of
          (1, 4) -> [Right i]
          (1, _) -> [Left i, Left $ mod (i - j) 4]
          _      -> []
  putStrLn $ if or [p == c | p <- ped, c <- car] then "YES" else "NO"
