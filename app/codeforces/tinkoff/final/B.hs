import           Text.Printf

main = (fmap (map read. words) getLine :: IO [Double]) >>= \[n, h]->  putStrLn $ unwords $ map (printf "%0.10f".(h *).sqrt.(/ n)) [1, 2 .. n - 1]
