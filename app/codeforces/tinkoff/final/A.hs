import           Control.Applicative
import qualified Data.ByteString.Char8 as BS

getInts::IO [Int]
getInts = go <$> BS.getLine where
  go s = case  BS.readInt s of
      Nothing      -> []
      Just (x, s') -> x : go (BS.drop 1 s')

main = getInts >>= \[_, l, r] -> getLine >> getInts >>= print . length . filter (liftA2 (&&) (>l) (<r))
