import           Control.Applicative
import           Data.Array.IArray

fib::Array Int Integer
fib = array (0, 100000) $ fmap ((,) <*> go) [0..100000] where
  go 0 = 1
  go 1 = 1
  go i = fib ! (i - 1) + fib ! (i - 2)

main::IO ()
main = print $ fib ! 100000
