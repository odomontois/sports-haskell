{-# LANGUAGE TupleSections #-}

module Main where

import           Data.Foldable (foldMap)
import           Data.List     (sort, sortBy)
import           Data.Monoid
import           Data.Ord

cols::String->[(Char,Integer)]
cols = sortBy (comparing snd) . uncolor . foldMap color where
  color 'R' = (Sum 1, Sum 0, Sum 0)
  color 'G' = (Sum 0, Sum 1, Sum 0)
  color 'B' = (Sum 0, Sum 0, Sum 1)
  color _   = undefined
  uncolor (Sum x,Sum y,Sum z) = [('R',x), ('G', y), ('B', z)]

result::[(Char, Integer)]->String
result [(a, x), (b, y), (c, z)] = sort $ case (x,y,z) of
  (0, 0, _) -> [c]
  (0, 1, 1) -> [a]
  (0, 1, _) -> [a,b]
  _         -> [a,b,c]
result _ = ""


main::IO ()
main = getLine >> getLine >>= putStrLn . result. cols
