{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections       #-}

module Main where
import qualified Data.Map    as Map
import           Data.Monoid

move ::Num a=>Char -> (a, a)
move 'D' = ( 0,-1)
move 'U' = ( 0, 1)
move 'L' = (-1, 0)
move 'R' = ( 1, 0)
move _   = ( 0, 0)

solution::forall a.Integral a=>String->a
solution = combine where
  psum (a,b)           = (Sum a, Sum b)
  unsum (Sum a, Sum b) = (a, b)
  start = psum ((0, 0)::(a,a))
  points = map ((,1) .unsum) . scanl (<>) start . map (psum . move)
  comb2 a = a * (a - 1) `div` 2::a
  combine = sum . map comb2 . Map.elems . Map.fromListWith (+) . points


main:: IO ()
main = getLine >> getLine >>= print . (solution::String->Integer)
