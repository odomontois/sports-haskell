import Data.Ratio
import Data.List (maximumBy, sort)
import Data.Ord
import Control.Monad
import Control.Arrow

find, find2::[Integer]->[Integer]->(Rational, [Integer])

find xs ys = let
  sums   = zipWith (+) (scanl1 (+) ys ) $ tail $  scanl1 (+) xs
  counts = [3,5..]
  avs    = zipWith (%) sums counts
  meds   = map fromInteger $ tail xs
  difs   = zipWith (-) avs meds
  (r,k)  =  maximum $ zip difs [1..]
  in (r, take (k + 1) xs ++ take k ys)

find2 xs ys = let
  sums   = zipWith (+) (scanl1 (+) ys ) $ drop 2 $  scanl1 (+) xs
  counts  = [4,6..]
  avs      = zipWith (%) sums counts
  avg  x y = (x + y) % 2
  meds   = zipWith avg (tail xs ) $ drop 2 xs
  difs   = zipWith (-) avs meds
  (r,k)  =  maximum $ zip difs [1..]
  in (r, take (k + 2) xs ++ take k ys)

solution :: [Integer] -> (Rational, [Integer])
solution as = let
  bs = reverse as
  n = length as
  u  = n `div` 2
  v  = (n + 1) `div` 2
  triv = [(0, take 1 as)]
  f  = guard (n > 2) >> [find (take v as) (take u bs)]
  f2 = guard (n > 3) >> [find2 (take (u+1) as) (take (v-1) bs)]
  in maximumBy (comparing fst) $ triv ++ f ++ f2

output :: [Integer] -> IO ()
output = void . runKleisli (Kleisli ( print . length) &&&  Kleisli (putStrLn .unwords . map show))

main:: IO ()
main = do
  void getLine
  xs <- fmap (map read. words) getLine
  output $ snd $ solution $ sort xs
