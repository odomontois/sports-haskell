calc::Int->Int->Int
calc = go 0 where
  go acc 1 y | y < 1 = acc + 2
  go acc 2 y | y < 2 = acc + 4
  go acc x 1 | x < 2 = acc + 3
  go acc x 2 | x < 3 = acc + 6
  go acc 3 y | y < 2 = acc + 6
  go acc x y = (uncurry .) go (acc + 6) $ if x * 2 > y * 3 then (x - 3,y - 1) else (x - 2 , y - 2)

main:: IO ()
main = fmap (map read . words) getLine >>= \[x,y] -> print $ calc x y
