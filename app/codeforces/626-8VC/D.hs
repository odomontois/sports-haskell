{-# LANGUAGE TupleSections #-}

import           Data.List     (genericLength, sort, foldl1')
import           Data.Ratio
import           Text.Printf

type Ps = [Pair]
data Pair = Pair {-# UNPACK #-} !Int {-# UNPACK #-} !Integer

merge::[Ps]->Ps
merge = foldl1' go where
  go xs [] = xs
  go [] ys = ys
  go xs'@(Pair x a:xs) ys'@(Pair y b:ys) = case compare x y of
    LT -> Pair x a      : go xs  ys'
    GT -> Pair y b      : go xs' ys
    EQ -> Pair x (a + b): go xs  ys


diffs::[Int]->Ps
diffs =  merge . go . sort where
  go [] = []
  go (x: xs) = map (sub x) xs : go xs
  sub x y = Pair (y - x) 1


summs2::Ps->Ps
summs2 xs = merge $ map (flip map xs . sumtup) xs where
  sumtup (Pair a1 b1) (Pair a2 b2) = Pair (a1 + a2) (b1 * b2)

count::[Int]->Double
count xs = fromRational $ marked % (cmb ^ (3::Int)) where
  n = genericLength xs
  cmb = n * (n - 1) `div` 2
  marked = go 0 0 sums difs
  difs   = diffs xs
  sums   = summs2 difs
  psnd (Pair _ x) = x
  go _ total _ []    = total
  go acc total [] ds = total + acc * sum (map psnd ds)
  go acc total ss'@(Pair s sc :ss) ds'@(Pair d dc:ds)
    | s < d     = go (acc + sc) total ss ds'
    | otherwise = go acc (total + dc * acc) ss' ds

main::IO ()
main = getLine >> getLine >>= printf "%.10f" . count . map read .words
