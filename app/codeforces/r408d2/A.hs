import           Control.Applicative
import           Control.Arrow
import qualified Data.ByteString.Char8 as BS
import           Data.List             (minimum, unfoldr)

getInts::IO [Int]
getInts = unfoldr (liftA (second (BS.drop 1)). BS.readInt) <$> BS.getLine

main :: IO ()
main = do
  [_,m,k] <-getInts
  cs <- getInts
  let solution=(10*).minimum.fmap(abs.(m-).fst).filter(liftA2(&&)(<=k)(>0).snd).zip[1..]
  print (solution cs)
