import           Control.Applicative
import           Data.Array.IArray
import           Data.Array.Unboxed
import qualified Data.ByteString.Char8 as BS

getInts::IO [Int]
getInts = go <$> BS.getLine where
  go s = case  BS.readInt s of
      Nothing      -> []
      Just (x, s') -> x : go (BS.drop 1 s')

getInt2::IO (Int,Int)
getInt2 = do
  line <- BS.getLine
  let Just (x, l') = BS.readInt line
      Just (y, _)  = BS.readInt $ BS.drop 1 l'
  return (x,y)

main :: IO ()
main = do
  [n, _, k] <- getInts
  holes <- getInts
  let holeMap::UArray Int Bool
      holeMap = array (1, n) $ zip holes $ repeat True
      swap pos (i, j) | holeMap ! pos = pos
                      | pos == i = j
                      | pos == j = i
                      | otherwise = pos
      doSwaps pos 0    = return pos
      doSwaps pos x    = do
        p <- getInt2
        doSwaps (swap pos p) (x - 1)
  pos <- doSwaps 1 k
  print pos
