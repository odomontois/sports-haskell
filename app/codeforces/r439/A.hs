import Control.Monad

main = do 
    x <- read <$> getLine :: IO Int
    ys <- replicateM x $ (read <$> getLine :: IO Int)
    print (x, ys)
    putStrLn "hello!!!"