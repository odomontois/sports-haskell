main = do
  [m, n, p] <- (read <$>) . words <$> getLine
  print $ mod (- (quot (m * n - m - n + gcd m n) 2)) p
