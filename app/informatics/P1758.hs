import Data.Array

main = do
  [n, a, b] <- fmap read . words <$> getLine
  let dp = listArray (1, n) $ fmap calc [1 ..] :: Array Int Int
      calc 1 = 0
      calc i = minimum $ divide i <$> [1 .. i - 1]
      divide i k = max (a + dp ! k) (b + dp ! (i - k))
  print $ dp ! n