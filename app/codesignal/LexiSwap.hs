import qualified Data.IntSet as S
import qualified Data.IntMap.Strict as M


data DSCell s = Member Int | Root s
type DS s = M.IntMap (DSCell s)

class Sizeable x where size::x -> Int
instance Sizeable S.IntSet where size = S.size

dsGet::Monoid s=>Int->DS s->s
dsGet i m = case M.lookup i m of 
        Just (Root s)   -> s
        Just (Member j) -> dsGet j m
        Nothing         -> mempty

dsMerge::(Monoid s, Sizeable s)=>Int->Int->DS s->DS s
dsMerge = walkl []  where
    walkl acc i j m = case M.lookup i m of 
        Just (Root s)    -> walkr acc (i,s) j m
        Just (Member i') -> walkl (i:acc) i' j m
        Nothing          -> m
    walkr acc s j m = case M.lookup j m of 
        Just (Root s')    -> chk acc s (j,s') m
        Just (Member j') -> walkr (j:acc) s j' m
        Nothing          -> m
    chk acc (i, si) (j, sj) m = 
        let s = mappend si sj
        in if size si > size sj 
              then mer (j:acc) i s m
              else mer (i:acc) j s m
    mer acc i s m = foldr (flip M.insert (Member i)) (M.insert i (Root s) m) acc

dsInit::Int->(Int->s)->DS s
dsInit i= fromList [0..i]

    