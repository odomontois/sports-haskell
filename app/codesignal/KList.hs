import Data.Traversable
import Data.Foldable
import qualified Data.Map.Strict as S
import Debug.Trace

data ListNode a = ListNode { val :: a
                           , next :: ListNode a
                           } | Nil deriving Show

instance Functor ListNode where fmap = fmapDefault 
instance Foldable ListNode where 
    foldMap f Nil = mempty
    foldMap f (ListNode x xs) = f x `mempty` foldMap f xs
instance Traversable ListNode where
    traverse f (ListNode h t) = ListNode <$> f h <*> traverse f t
    traverse _ Nil            = pure Nil

-- reverseNL = foldl' (flip ListNode) Nil 

instance Eq a => Eq (ListNode a) where
    ListNode a x == ListNode b y = a == b && x == y
    Nil == Nil  = True
    _ == _      = False

indexate::Ord a => [a] -> [Int]
indexate = traceShowId . snd . mapAccumL mark (S.empty, 1) where
    mark (m, i) e = maybe ((S.insert e i m, i + 1), 0) ((,) (m, i)) $ S.lookup e m
    

    
fromList::[a]->ListNode a
fromList = foldr ListNode Nil

xx= ["re", 
    "jjinh", 
    "rnz", 
    "frok", 
    "frok", 
    "hxytef", 
    "hxytef", 
    "frok"]
yy = ["kzfzmjwe", 
    "fgbugiomo", 
    "ocuijka", 
    "gafdrts", 
    "gafdrts", 
    "ebdva", 
    "ebdva", 
    "gafdrts"]