import Control.Monad
import Data.List
import Data.Set (fromList)

by n [] = []
by n xs = let (pref, rest) = splitAt 3 xs in pref: by n rest
good::[[Char]] -> Bool
good = all (unique. filter ('.' /=))

unique xs = length xs == length (fromList xs) 

block n =  join. fmap (fmap join. transpose. fmap (by n)) . by n

grid = [['.', '.', '.', '1', '4', '.', '.', '2', '.'],
        ['.', '.', '6', '.', '.', '.', '.', '.', '.'],
        ['.', '.', '.', '.', '.', '.', '.', '.', '.'],
        ['.', '.', '1', '.', '.', '.', '.', '.', '.'],
        ['.', '6', '7', '.', '.', '.', '.', '.', '9'],
        ['.', '.', '.', '.', '.', '.', '8', '1', '.'],
        ['.', '3', '.', '.', '.', '.', '.', '.', '6'],
        ['.', '.', '.', '.', '.', '7', '.', '.', '.'],
        ['.', '.', '.', '5', '.', '.', '.', '7', '.']]

-- main = putStrLn $ block 3 grid
