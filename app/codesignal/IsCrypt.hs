import qualified Data.Map as M
import Data.Maybe
import Control.Monad




isCryptSolution crypt solution =  maybe False (\[x,y,z] -> x + y == z) $ traverse decode crypt where
    decode::String -> Maybe Int
    decode = fmap read . mfilter (\xs -> head xs /= '0') . traverse (`M.lookup` dict)
    dict   = M.fromList $ fmap (\[k,v] -> (k, v)) solution

solution = [['O', '0'],
    ['M', '1'],
    ['Y', '2'],
    ['E', '5'],
    ['N', '6'],
    ['D', '7'],
    ['R', '8'],
    ['S', '9']]

crypt = ["SEND", "MORE", "MONEY"]

-- main = print $ isCryptSolution crypt sol