import           Control.Monad
import           Data.Maybe

data Parser a = Done ([a], String) | Fail String deriving Show

instance Functor Parser where
  fmap = liftM

instance Applicative Parser where
  pure = return
  (<*>) = ap

-- instance Monad Parser where
--   return x = Done ([x], "")
--   Fail s >>= _ = Fail s
--   Done (xs, s) >>= f  =  foldr (go. f) (Done ([], s)) xs where
--     go (Done (ys, t)) (Done (yss, ts)) = Done (ys ++ yss, t ++ ts)
--     go (Fail t) _ = Fail t
--     go _ (Fail t) = Fail t

parseList::[String]->Parser String
parseList xs = Done (xs, "")

parseSingle::Read a => String -> Parser a
parseSingle s = case listToMaybe (reads s) of
  Just (x, s') -> Done ([x], if null s' then "" else "dropping " ++ s' ++ ".")
  Nothing -> Fail $ "can not parse " ++ s  ++ ". "

parseAll::Read a=>String-> Parser a
parseAll source = do
  line <- parseList (lines source)
  word <- parseList (words line)
  parseSingle word

main::IO ()
main = do
  print (parseAll "12 23 45 \n 34 23 \n 11" ::Parser Int)
  print (parseAll "12 23 45 \n 3f 23 \n 11" ::Parser Int)
  print (parseAll "12 23 45 \n xf 23 \n 11" ::Parser Int)
