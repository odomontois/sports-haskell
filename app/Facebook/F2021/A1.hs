{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE ViewPatterns #-}

module Main where

import Control.Monad (forM_, replicateM_)
import Data.ByteString.Char8 (ByteString)
import qualified Data.ByteString.Char8 as B
import System.IO (IOMode (ReadMode, WriteMode), hGetLine, openFile, withFile)
import Text.Printf (printf, hPrintf)

inputName = "/home/odomontois/Prog/projects/haskell/sports-haskell/app/Facebook/F2021/consistency_chapter_1_input.txt"

main :: IO ()
main = withFile inputName ReadMode \f ->
  withFile "./app/Facebook/F2021/A.final.out" WriteMode \out -> do
    (read . B.unpack -> n) <- B.hGetLine f
    forM_ [1 .. n] \i -> do
      s <- B.hGetLine f
      hPrintf out "Case #%d: %d\n" (i :: Int) (solve s)

solve :: ByteString -> Int
solve t = minimum $ try <$> ['A' .. 'Z']
  where
    isVowel = flip elem "AOEUI"
    dist c1 c2
      | c1 == c2 = 0
      | isVowel c1 == isVowel c2 = 2
      | otherwise = 1
    try c = sum $ dist c <$> B.unpack t