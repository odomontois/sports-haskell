{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ViewPatterns #-}

import Control.Lens.Iso (under)
import Control.Monad
import Data.Either (lefts, rights)
import Data.List (nub, sort)
import Data.Maybe (mapMaybe)
import Data.Vector (Vector, fromList, (!))
import qualified Data.Vector as V
import GHC.Real (underflowError)
import System.IO (IOMode (ReadMode, WriteMode), hGetLine, withFile)
import Text.Printf (hPrintf, printf)
import Text.Read (Read)

pattern IS :: Int -> String
pattern IS n <- (read -> n)

inputName = "/home/odomontois/Prog/projects/haskell/sports-haskell/app/Facebook/F2021/xs_and_os_input.txt"

outputName = "./app/Facebook/F2021/B.full.out"

-- main = print $ transitive [('A', 'B'), ('A', 'C'), ('B', 'D'), ('C', 'B')]

data Cell = X | O | E deriving (Show)

cell :: Char -> Maybe Cell
cell 'X' = Just X
cell 'O' = Just O
cell '.' = Just E
cell _ = Nothing

instance Read Cell where
  readsPrec _ ((cell -> Just el) : s) = [(el, s)]
  readsPrec _ _ = []

main :: IO ()
main = withFile inputName ReadMode \f ->
  withFile outputName WriteMode \out -> do
    IS n <- hGetLine f
    forM_ [1 .. n] \i -> do
      IS n <- hGetLine f
      chars <- fromList <$> replicateM n (V.mapMaybe cell . fromList <$> hGetLine f)
      let result = maybe "Impossible" (uncurry $ printf "%d %d") $ solution chars
      hPrintf out "Case #%d: %s\n" i result

solution :: Vector (Vector Cell) -> Maybe (Int, Int)
solution m =
  let indices = [0 .. V.length m - 1]
      results = mapMaybe result (indices >>= sol)
      sol i = [col i, row i]
      col i = ((,i) <$> indices, i, True)
      row i = ((i,) <$> indices, i, False)
      result (ls, i, b) = foldM go (Right 0) $ item <$> ls
      item (i, j) = (i, j, m ! i ! j)
      go r (i, j, c) = case c of
        O -> Nothing
        X -> Just r
        E -> Just case r of
          Right 0 -> Left (i, j)
          Left _ -> Right 2
          Right i -> Right (i + 1)
      size (Left _) = 1
      size (Right i) = i
      minSize = minimum $ size <$> results
      count
        | minSize == 1 = length $ nub $ sort $ lefts results
        | minSize == 0 = 1
        | otherwise = length $ filter (== minSize) $ rights results
   in if null results then Nothing else Just (minSize, count)