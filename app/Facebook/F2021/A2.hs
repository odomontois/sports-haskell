{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ViewPatterns #-}

module Main where

import Control.Monad (forM_, replicateM)
import Data.ByteString.Char8 (ByteString)
import qualified Data.ByteString.Char8 as B
import Data.List.NonEmpty (nonEmpty)
import Data.Map ((!?))
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Data.Maybe (catMaybes, fromMaybe, mapMaybe)
import Data.Sequence (Seq ((:<|)), (><))
import qualified Data.Sequence as S
import qualified Data.Set as Set
import System.IO (IOMode (ReadMode, WriteMode), hGetLine, withFile)
import Text.Printf (hPrintf)

pattern IS :: Int -> String
pattern IS n <- (read -> n)

inputName = "/home/odomontois/Prog/projects/haskell/sports-haskell/app/Facebook/F2021/consistency_chapter_2_input.txt"

outputName = "./app/Facebook/F2021/A2.full.out"

-- main = print $ transitive [('A', 'B'), ('A', 'C'), ('B', 'D'), ('C', 'B')]

main = withFile inputName ReadMode \f ->
  withFile outputName WriteMode \out -> do
    IS n <- hGetLine f
    forM_ [1 .. n] \i -> do
      s <- hGetLine f
      IS q <- hGetLine f
      (transitive -> cs) <- replicateM q do
        [a, b] <- hGetLine f
        pure (a, b)

      hPrintf out "Case #%d: %d\n" i (solve cs s)

type Costs = Map (Char, Char) Int

transitive :: [(Char, Char)] -> Costs
transitive assocs = M.fromList $ ['A' .. 'Z'] >>= steps
  where
    list = M.fromListWith (++) $ (pure <$>) <$> assocs
    steps c = go (S.singleton (c, 0)) (Set.singleton c)
      where
        go S.Empty _ = []
        go ((x, i) :<| rest) seen =
          let seen' = Set.union seen $ Set.fromList next
              q = rest >< S.fromList ((,i + 1) <$> next)
              next = filter (not . flip Set.member seen) $ fromMaybe [] $ list !? x
           in ((c, x), i) : go q seen'

solve :: Costs -> String -> Int
solve cs s = maybe (-1) minimum $ nonEmpty $ mapMaybe try ['A' .. 'Z']
  where
    try c = sum <$> traverse (flip M.lookup cs . (,c)) s