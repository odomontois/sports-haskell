{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE TypeSynonymInstances   #-}

import           Control.Monad.ST
import           Control.Monad.State
import qualified Control.Monad.State.Strict as SS
import           Data.Array.IO
import           Data.Array.ST
import           Data.IntMap                (IntMap)
import qualified Data.IntMap.Lazy           as LIM
import qualified Data.IntMap.Strict         as SIM
import           Data.Map                   (Map)
import qualified Data.Map                   as Map
import           Data.Maybe
import           System.Environment

newtype LIM = LIM {runLIM::IntMap Int}
newtype SIM = SIM {runSIM::IntMap Int}

class Monad m => MutIntArr a m | m -> a where
  newArr::Int->m a
  getArr::a->Int->m Int
  setArr::a->Int->Int->m ()

instance MutIntArr () (State (IntMap Int)) where
  newArr _ = put SIM.empty
  getArr () i = gets (fromMaybe 0 . SIM.lookup i)
  setArr () i x = modify' (SIM.insert i x)

instance MutIntArr () (SS.State SIM) where
    newArr _ = SS.put $ SIM SIM.empty
    getArr () i = SS.gets (fromMaybe 0 . SIM.lookup i. runSIM)
    setArr () i x = SS.modify' (SIM .SIM.insert i x.runSIM)

instance MutIntArr () (SS.State LIM) where
    newArr _ = SS.put $ LIM LIM.empty
    getArr () i = SS.gets (fromMaybe 0 . LIM.lookup i. runLIM)
    setArr () i x = SS.modify' (LIM . LIM.insert i x.runLIM)

instance MutIntArr (STUArray s Int Int) (ST s) where
  newArr n = newArray_ (0, n)
  getArr = readArray
  setArr = writeArray

instance MutIntArr (IOArray Int Int) IO where
  newArr n = newArray (0, n) 0
  getArr = readArray
  setArr = writeArray

instance MutIntArr () (State (Map Int Int)) where
  newArr _ = put Map.empty
  getArr () i = gets (fromMaybe 0. Map.lookup i)
  setArr () i x = modify' (Map.insert i x)

instance MutIntArr () (SS.State (Map Int Int)) where
    newArr _ = SS.put Map.empty
    getArr () i = SS.gets (fromMaybe 0. Map.lookup i)
    setArr () i x = SS.modify' (Map.insert i x)

sieve::MutIntArr a m=>Int->m Integer
sieve size = do
  arr <- newArr size
  forM_ [2..size] $ mark arr
  nums <- forM [2..size] $ getPrime arr
  return $ sum $ fmap fromIntegral nums where
    mark arr i = do
      x <- getArr arr i
      when (x == 0) $ walk arr (i * i) i
    walk arr k i = unless (k > size) $ do
      setArr arr k i
      walk arr (k + i) i
    getPrime arr i = do
      x <- getArr arr i
      return $ if x == 0 then i else 0

main :: IO ()
main = do
  args <- getArgs
  case args of
    ["IO", num]      -> sieve (read num) >>= print
    ["ST", num]      -> print $ runST $ sieve $ read num
    ["IntMap", num]  -> print $ sieve (read num) `evalState` intMapStart
    ["Map", num]     -> print $ sieve (read num) `evalState` mapStart
    ["IntMapS", num] -> print $ sieve (read num) `SS.evalState` SIM intMapStart
    ["IntMapL", num] -> print $ sieve (read num) `SS.evalState` LIM intMapStart
    ["MapS", num]    -> print $ sieve (read num) `SS.evalState` mapStart
    _                -> error "oh shit"
  where
    intMapStart = SIM.empty :: IntMap Int
    mapStart    = Map.empty :: Map Int Int
