module Main where
import           Control.Monad
import           Data.Foldable           (foldl')
import           Data.List.NonEmpty hiding (map)
import           Data.Semigroup
import           Data.Semigroup.Foldable

merge::(Ord a, Semigroup b)=>[(a,b)]->[(a,b)]->[(a,b)]
merge [] ys = ys
merge xs [] = xs
merge xs'@((a,x):xs) ys'@((b,y):ys) = case compare a b of
  LT -> (a,x)     : merge xs  ys'
  GT -> (b,y)     : merge xs' ys
  EQ -> (a,x <> y): merge xs  ys

getList::Read a=>IO [a]
getList = map read . words <$> getLine

solution::[[Int]]->Int
solution = getMin . foldMap1 snd . foldl' step initial where
  initial = ([Nothing, Nothing], 0) :| []
  set 0 x lst
  dist
  command (robs, track) from to robo =
    let realloc = abs
  step    = undefined

main::IO ()
main = do
  t <- read <$> getLine
  replicateM_ t $ do
    [_m,n] <- getList
    qs    <-  replicateM n getList
    print $ solution qs
