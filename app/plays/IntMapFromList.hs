import qualified Data.IntMap as IntMap

data Lst x = Single x | Cons (Lst x) (Lst x) deriving Show

main = print . IntMap.fromListWith Cons $ fmap (\x -> (mod x 3, Single x)) [1..20]
