{-#LANGUAGE MagicHash #-}
ss:: Int# -> Int# -> Int#
ss 0 s = s
ss i s = ss (i-1) (s+i)

main = print $ ss 10000000 0 https://www-qa2.tcsbank.ru/travel/internal/app/projectConfig