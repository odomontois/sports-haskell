{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Stepik () where

class (Enum a, Bounded a, Eq a) => SafeEnum a where
  ssucc :: a -> a
  ssucc x | x == maxBound = minBound
          | otherwise = succ x

  spred :: a -> a
  spred x | x == minBound = maxBound
          | otherwise = pred x

newtype SEnum a = SEnum{ getSEnum :: a} deriving (Enum, Bounded, Eq)

instance (Enum a, Bounded a, Eq a) => SafeEnum (SEnum a)
