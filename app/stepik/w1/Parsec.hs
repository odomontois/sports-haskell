module Stepik.Parsec where

import           Control.Applicative
import           Text.Parsec         (Parsec, digit, many1, sepBy, string)

import           Data.List           (uncons)

getList :: Parsec String u [String]
getList = many1 digit `sepBy` string ";"


ignoreBraces :: Parsec [Char] u a -> Parsec [Char] u b -> Parsec [Char] u c -> Parsec [Char] u c
ignoreBraces open close mid = open *> mid <* close
