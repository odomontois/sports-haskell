module Stepik.W1.PrsE where

import           Control.Applicative

newtype PrsE a = PrsE { runPrsE :: String -> Either String (a, String) }

satisfyE :: (Char -> Bool) -> PrsE Char
satisfyE p = PrsE prs where
  prs [] = Left "unexpected end of input"
  prs (x:xs) | p x       = Right (x, xs)
             | otherwise = Left $ "unexpected " ++ return x

charE :: Char -> PrsE Char
charE c = satisfyE (== c)

instance Functor PrsE where
  fmap f (PrsE p) = PrsE $ \s -> do
    (x, rest) <- p s
    return (f x, rest)

instance Applicative PrsE where
  pure  = return
  PrsE f <*> PrsE g = PrsE $ \s -> do
    (h, s') <- f s
    (x, s'') <- g s'
    return (h x, s'')

instance Monad PrsE where
  return x = PrsE $ \s-> Right (x, s)
  (PrsE pa) >>= f = PrsE $ \s -> do
    (x , s') <- pa s
    runPrsE (f x) s'

instance Alternative PrsE where
  empty = PrsE f where
    f _ = Left "empty alternative"
  p <|> q = PrsE f where
    f s = let ps = runPrsE p s
      in if null ps
         then runPrsE q s
         else ps
