module Stepik.Prs where

import           Control.Applicative
import           Control.Monad
import           Data.Char
import           Data.List           (uncons)

newtype Prs a = Prs { runPrs :: String -> Maybe (a, String) }

instance Functor Prs where
  fmap f (Prs p) = Prs $ \s -> do
    (x, rest) <- p s
    return (f x, rest)

anyChr :: Prs Char
anyChr = Prs uncons

satisfy :: (Char -> Bool) -> Prs Char
satisfy p = Prs prs where
  prs [] = Nothing
  prs (x:xs) | p x = return (x, xs)
             | otherwise = mzero

char :: Char -> Prs Char
char c = satisfy (== c)

instance Applicative Prs where
  pure = Prs . (return .) . (,)
  Prs f <*> Prs g = Prs $ \s -> do
    (h, s') <- f s
    (x, s'') <- g s'
    return (h x, s'')

instance Alternative Prs where
  empty = Prs $ const Nothing
  Prs f <|> Prs g = Prs $ \s -> f s <|> g s

many1 :: Prs a -> Prs [a]
many1 prs = (:) <$> prs <*> (many1 prs <|> pure [])

digit::Prs Char
digit = satisfy isDigit

nat :: Prs Int
nat = read <$> many1 digit

mult :: Prs Int
mult = (*) <$> nat <* char '*' <*> nat
