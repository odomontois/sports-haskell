module Stepik2 where
import           Control.Applicative (ZipList (ZipList), getZipList, (<**>))

newtype Arr2 e1 e2 a = Arr2 {getArr2 :: e1 -> e2 -> a}
newtype Arr3 e1 e2 e3 a = Arr3 {getArr3 :: e1 -> e2 -> e3 -> a}

instance Functor (Arr2 e1 e2) where
  fmap f (Arr2 g) = Arr2 $  (f .) . g

instance Applicative (Arr2 e1 e2) where
  pure = Arr2 . const . const
  Arr2 f <*> Arr2 g= Arr2 $ \x y -> f x y $ g x y

instance Functor (Arr3 e1 e2 e3) where
  fmap f (Arr3 g) = Arr3 $ ((f. ) .) . g

instance Applicative (Arr3 e1 e2 e3) where
  pure = Arr3 . const . const . const
  Arr3 f <*> Arr3 g= Arr3 $ \x y z -> f x y z $ g x y z

data Triple a = Tr a a a deriving (Eq,Show)



instance Functor Triple where
  fmap f (Tr x y z) = Tr (f x) (f y) (f z)

instance Applicative Triple where
  pure x = Tr x x x
  Tr fx fy fz <*> Tr x y z = Tr (fx x) (fy y) (fz z)

data Shit a = Shit [a] deriving (Eq, Ord, Show)
instance Functor Shit where fmap f (Shit xs) = Shit (fmap f xs)

instance Applicative Shit where
 pure x = Shit [x, x]
 Shit f <*> Shit xs = Shit (f <*> xs)


(>*<)::[a->b]->[a]->[b]
(>*<) = (getZipList .) . (. ZipList ) . (<*>) . ZipList

(>$<)::(a->b)->[a]->[b]
(>$<) = (<$>)

divideList :: Fractional a => [a] -> a
divideList = foldr (/) 1

divideList' :: (Show a, Fractional a) => [a] -> (String,a)
divideList'= foldr (\ x -> (((/) <$> ("<-" ++ shows x "/", x)) <*>) ("1.0", 1)

infixl 4 <*?>
(<*?>) :: Applicative f => f a -> f (a -> b) -> f b
(<*?>) = flip (<*>)
