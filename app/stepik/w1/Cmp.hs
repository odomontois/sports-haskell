{-# LANGUAGE TypeOperators #-}

module Stepik.Cmp where

infixr 9 |.|
newtype (|.|) f g a = Cmps {getCmps::f (g a)}


newtype Cmps3 f g h a = Cmps3 { getCmps3 :: f (g (h a)) }  deriving (Eq,Show)

instance (Functor f, Functor g, Functor h) => Functor (Cmps3 f g h) where
  fmap = (. getCmps3) . (Cmps3 .) .  fmap . fmap . fmap


unCmps3 :: Functor f => (f |.| g |.| h) a -> f (g (h a))
unCmps3 = fmap getCmps . getCmps

unCmps4 :: (Functor f2, Functor f1) => (f2 |.| f1 |.| g |.| h) a -> f2 (f1 (g (h a)))
unCmps4 = fmap (fmap getCmps . getCmps) . getCmps
