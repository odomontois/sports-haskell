module Main where

import           Control.Monad
import           Data.Foldable
import           System.Environment

newtype State s a = State { runState :: s -> (a,s) }

instance Functor (State s) where
  fmap f m = State $ \st -> updater $ runState m st
    where updater ~(x, s) = (f x, s)

instance Applicative (State s) where
  pure x =  State $ \s -> (x, s)

  f <*> v = State $ \s ->
    let (g, s')  = runState f s
        (x, s'') = runState v s'
    in seq s (g x, s'')

instance Monad (State s) where
  return a = State $ \s -> (a, s)

  m >>= k = State $ \s ->
    let (x, s') = runState m s
    in seq s runState (k x) s'

get :: State s s
get = State $ \s -> (s, s)

put :: s -> State s ()
put s = State $ \_ -> ((), s)

tick :: State Int Int
tick = do
  n <- get
  put $! (n+1)
  return n

test :: Int -> Int
test i = snd $ runState (mapM_ (const tick) [1..i]) 0

main :: IO ()
main = do
  [i] <- getArgs
  print $ test $ read i
