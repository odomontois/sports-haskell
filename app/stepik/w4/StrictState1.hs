module Stepik.W4.StrictState1 where

import           Control.Applicative
import           Control.Monad
import           Data.Foldable

data StrictTuple a s =  (:!:) !a  !s

newtype State s a = State { runState' :: s -> StrictTuple a s }

runState::State s a->s->(a,s)
runState ss s = let a :!: s' = (runState' $! ss) $! s in ss `seq` s `seq` (a, s')

instance Functor (State s) where
  fmap f m = f `seq` m `seq` State  (\st ->
                       let updater (x :!: s) = (f $! x) :!: s
                       in st `seq` updater $! (runState' $! m) $! st)

instance Applicative (State s) where
  pure x =  x `seq` State (\s -> x :!: s)

  f <*> v = f `seq` v `seq` State ( \s ->
    let g :!: s'  =(runState' $! f) $! s
        x :!: s'' = (runState' $! v) $! s'
        a = g $! x
    in  s `seq` a :!: s'')

instance Monad (State s) where
  return a = a `seq` State (a :!:)

  m >>= k = m `seq` k `seq` State ( \s ->
    let x :!: s' = s `seq` (runState' $! m) $! s
        a = k $! x
    in s `seq` (runState' $! a) $! s')

get :: State s s
get = State (\s -> s :!: s)

put :: s -> State s ()
put s = s `seq` State (\_ -> () :!: s)


tick :: State Int Int
tick = do
  n <- get
  put $! (n+1)
  return n

test :: Int
test = snd $ runState (mapM_ (const tick) [1..10^7]) 0

main = print test
