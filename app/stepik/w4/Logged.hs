{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances  #-}

module Stepik.W4.Logged where

import           Control.Monad
import           Control.Monad.Identity
import           Control.Monad.Reader
import           Control.Monad.State
import           Data.Monoid

data Logged a = Logged String a deriving (Eq,Show)

newtype LoggT m a = LoggT { runLoggT :: m (Logged a) }

instance Monad m => Functor (LoggT m) where fmap = liftM
instance Monad m => Applicative (LoggT m) where pure = return; (<*>) = ap
instance Monad m => Monad (LoggT m) where
  return = LoggT . return . Logged ""
  mx >>= f = LoggT $ do
    ~(Logged s  x) <- runLoggT mx
    ~(Logged s' y) <- runLoggT (f x)
    return $ Logged (s <> s') y
  fail = LoggT . fail

logTst :: LoggT Identity Integer
logTst = do
  x <- LoggT $ Identity $ Logged "AAA" 30
  let y = 10
  z <- LoggT $ Identity $ Logged "BBB" 2
  return $ x + y + z

failTst :: [Integer] -> LoggT [] Integer
failTst xs = do
  5 <- LoggT $ fmap (Logged "") xs
  LoggT [Logged "A" ()]
  return 42

write2log :: Monad m => String -> LoggT m ()
write2log = LoggT . return . (`Logged` ())

type Logg = LoggT Identity

runLogg :: Logg a -> Logged a
runLogg = runIdentity . runLoggT

logTst' :: Logg Integer
logTst' = do
  write2log "AAA"
  write2log "BBB"
  return 42

stLog :: StateT Integer Logg Integer
stLog = do
  modify (+1)
  a <- get
  lift $ write2log $ show $ a * 10
  put 42
  return $ a * 100

instance MonadTrans LoggT where lift = LoggT . fmap (Logged mempty)

instance MonadState s m => MonadState s (LoggT m) where
  get   = lift get
  put   = lift . put
  state = lift . state

instance MonadReader r m => MonadReader r (LoggT m) where
  ask     = lift ask
  local f = LoggT . local f . runLoggT
  reader  = lift . reader


class Monad m => MonadLogg m where
  w2log :: String -> m ()
  logg :: Logged a -> m a

instance Monad m => MonadLogg (LoggT m) where
  w2log = LoggT . return . (`Logged` ())
  logg  = LoggT . return

instance MonadLogg m => MonadLogg (StateT s m) where
  w2log = lift . w2log
  logg  = lift . logg

instance MonadLogg m => MonadLogg (ReaderT r m) where
  w2log = lift . w2log
  logg  = lift . logg
