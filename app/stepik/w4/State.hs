module Stepik.W4.State where

import           Control.Applicative
import           Control.Monad.Reader

newtype StateT s m a = StateT { runStateT :: s -> m (a, s) }

evalStateT :: Monad m => StateT s m a -> s -> m a
evalStateT =  (fmap fst .). runStateT

execStateT :: Monad m => StateT s m a -> s -> m s
execStateT = (fmap snd .). runStateT

readerToStateT :: Monad m => ReaderT r m a -> StateT r m a
readerToStateT = StateT . (fmap <$> flip (,) <*>) . runReaderT

state :: Monad m => (s -> (a, s)) -> StateT s m a
state f = StateT (return . f)


instance Functor m => Functor (StateT s m) where
  fmap f m = StateT $ \st -> fmap updater $ runStateT m st
    where updater ~(x, s) = (f x, s)

instance Monad m => Applicative (StateT s m) where
  pure x = StateT $ \ s -> return (x, s)

  f <*> v = StateT $ \ s -> do
      ~(g, s') <- runStateT f s
      ~(x, s'') <- runStateT v s'
      return (g x, s'')

instance Monad m => Monad (StateT s m) where
  m >>= k  = StateT $ \s -> do
    ~(x, s') <- runStateT m s
    runStateT (k x) s'

  fail  = StateT . const. fail
