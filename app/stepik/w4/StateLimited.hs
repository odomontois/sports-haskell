{-# LANGUAGE FlexibleContexts #-}

module Stepik.W4.StateLimited where

import           Control.Monad.Except
import           Control.Monad.State

runLimited::(MonadError Int m, MonadState s m)=>(s->Bool)->[State s a]->m [a]
runLimited allow = traverse go . zip [0..] where
  go (i, st) = do
    x <- state $ runState st
    s <- get
    unless (allow s) $ throwError i
    return x

runLimited1 :: (s -> Bool) -> [State s a] -> s -> (Either Int [a], s)
-- runLimited1 p =  runState . runExceptT. runLimited p
runLimited1 p fs s = run1 (runLimited p fs) s where run1 = runState . runExceptT

runLimited2 :: (s -> Bool) -> [State s a] -> s -> Either Int ([a], s)
runLimited2 p = runStateT . runLimited p
