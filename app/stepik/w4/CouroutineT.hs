{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TupleSections         #-}
{-# LANGUAGE UndecidableInstances  #-}


module Stepik.W4.Couroutine where

newtype CoroutineT m a = CoroutineT { runCoroutineT :: forall r. (a -> m r) -> (CoroutineT m a -> m r) -> m r }

instance Functor (CoroutineT m) where fmap = liftM
instance Applicative (CoroutineT m) where pure = return ; (<*>) = ap
instance Monad (CoroutineT m) where
  return x = CoroutineT $ \ret _ -> ret x
  CoroutineT u >>= f = CoroutineT $ \ ret yld -> u (\x -> runCoroutineT (f x) ret yld) (\c -> yld (c >>= f))

execCoroutine::Monad m => CoroutineT m a->m a
execCoroutine c = runCoroutineT c return execCoroutine

runCoroutines :: Monad m => CoroutineT m () -> CoroutineT m () -> m ()
runCoroutines master slave = runCoroutineT master (\_ -> execCoroutine slave) (runCoroutines slave)

yield :: CoroutineT m ()
yield = CoroutineT $ \_ yld -> yld (return ())

instance MonadWriter w m => MonadWriter w (CoroutineT m) where
  tell w = CoroutineT $ \ret _ -> tell w >> ret ()
  listen x = CoroutineT $ \ret _ -> listen (execCoroutine x) >>= ret
  pass x = CoroutineT $ \ret _ -> pass (execCoroutine x) >>= ret

instance MonadCont (CoroutineT r m) where
  callCC f = CoroutineT $ \ret yld -> runCoroutineT (f ( \x -> CoroutineT$ \_ _ -> ret x)) ret yld

coroutine1, coroutine2, coroutine3, coroutine4::CoroutineT r (Writer String) ()

coroutine1 = do
  tell "1"
  yield
  tell "2"
--
coroutine2 = do
  tell "a"
  yield
  tell "b"

coroutine3 = do
  tell "1"
  yield
  yield
  tell "2"

coroutine4 = do
  tell "a"
  yield
  tell "b"
  yield
  tell "c"
  yield
  tell "d"
  yield
