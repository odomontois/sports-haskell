module Stepik.W4.MaybeT where

import           Control.Arrow
import           Control.Monad
import           Control.Monad.IO.Class     (liftIO)
import           Control.Monad.Trans.Except
import           Data.Char                  (isNumber, isPunctuation)
import           Data.Foldable              (msum)

--Не снимайте комментарий - эти объявления даны в вызывающем коде
newtype PwdError = PwdError String

type PwdErrorIOMonad = ExceptT PwdError IO
instance Monoid PwdError where
  mappend _ r = r
  mempty = undefined


askPassword :: PwdErrorIOMonad ()
askPassword = do
  liftIO $ putStrLn "Enter your new password:"
  _value <- msum $ repeat getValidPassword
  liftIO $ putStrLn "Storing in database..."


getValidPassword :: PwdErrorIOMonad String
getValidPassword = do
  s <- liftIO getLine
  either ((>>) <$> liftIO . putStrLn <*> (throwE . PwdError)) return $ validatePass s


validatePass :: String -> Either String String
validatePass s = left ("Incorrect input: " ++) $ do
  unless (length s >= 8)       $ Left "password is too short!"
  unless (any isNumber s)      $ Left "password must contain some digits!"
  unless (any isPunctuation s) $ Left "password must contain some punctuation!"
  return s
