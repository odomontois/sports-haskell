module Stepik.W4.Except where

import           Control.Arrow
import           Control.Monad
import qualified Control.Monad.Except       as E
import           Control.Monad.Trans
import           Control.Monad.Trans.Except
import           Control.Monad.Trans.Writer
import           Data.Foldable
import           Data.Monoid

data Tile = Floor | Chasm | Snake  deriving Show

data DeathReason = Fallen | Poisoned  deriving (Eq, Show)

type Point = (Integer, Integer)
type GameMap = Point -> Tile

moves :: GameMap -> Int -> Point -> [Either DeathReason Point]
moves game = (E.runExceptT .).  go  where
  go 0 p = verify p
  go k p = steps p >>= go (k - 1)
  verify::Point->E.ExceptT DeathReason [] Point
  verify p = case game p of
    Floor -> return p
    Chasm -> E.throwError Fallen
    Snake -> E.throwError Poisoned
  neighbor (x, y) = [(x+1, y), (x-1, y), (x, y+1), (x, y-1)]
  steps p = verify p >> lift (neighbor p)


waysToDie :: DeathReason -> GameMap -> Int -> Point -> Int
waysToDie r = (((length . filter (== Left r)).).). moves

map1 :: GameMap
map1 (2, 2) = Snake
map1 (4, 1) = Snake
map1 (x, y)
  | 0 < x && x < 5 && 0 < y && y < 5 = Floor
  | otherwise                        = Chasm


data ReadError = EmptyInput | NoParse String    deriving Show

tryRead :: (Read a, Monad m) => String -> ExceptT ReadError m a
tryRead s | null s = throwE EmptyInput
          | otherwise = case reads s of
            (x, []): _ -> return x
            _          -> throwE $ NoParse s

treeSum t = let (err, s) = runWriter . runExceptT $ traverse_ go t
            in (maybeErr err, getSum s)
  where
    maybeErr :: Either ReadError () -> Maybe ReadError
    maybeErr = either Just (const Nothing)


go :: String -> ExceptT ReadError (Writer (Sum Integer)) ()
go s = (lift . tell . Sum ) =<< tryRead s
