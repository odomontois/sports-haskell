{-# LANGUAGE FlexibleContexts #-}

module Stepik.W4.MonadError where

import Control.Monad.Except
import Control.Monad.Writer
import Data.Traversable
import Control.Applicative


data ReadError = EmptyInput | NoParse String
  deriving Show

tryRead :: (Read a, MonadError ReadError m) => String -> m a
tryRead s | null s = throwError EmptyInput
          | otherwise = case reads s of
            (x, []): _ -> return x
            _          -> throwError $ NoParse s



data Tree a = Leaf a | Fork (Tree a) a (Tree a) deriving Show

instance Functor Tree where fmap = fmapDefault
instance Foldable Tree where foldMap = foldMapDefault
instance Traversable Tree where
  traverse f (Leaf x) = Leaf <$> f x
  traverse f (Fork l x r) = Fork <$> traverse f l <*> f x <*> traverse f r

treeSum :: Tree String -> Either ReadError Integer
treeSum = (getSum <$>) . execWriterT . traverse ((>>= tell . Sum).tryRead)
