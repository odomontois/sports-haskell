module Stepik.W4.Tree where

import           Control.Applicative
import           Control.Monad.State
import           Control.Monad.Writer

data Tree a = Leaf a | Fork (Tree a) a (Tree a) deriving Show

numberAndCount :: Tree () -> (Tree Integer, Integer)
numberAndCount t = getSum <$> runWriter (evalStateT (go t) 1)
  where
    pick = state ((,) <$> id <*> (+1))
    go :: Tree () -> StateT Integer (Writer (Sum Integer)) (Tree Integer)
    go (Leaf _)     = lift (tell (Sum 1)) >> Leaf <$> pick
    go (Fork l _ r) = Fork <$> go l <*> pick <*> go r
