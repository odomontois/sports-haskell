{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}


module Stepik.W4.Couroutine where

import           Control.Monad

newtype CoroutineT m a = CoroutineT { runCoroutineT :: forall r. (a -> m r) -> m r -> m r }

instance Functor (CoroutineT m) where fmap = liftM
instance Applicative (CoroutineT m) where pure = return ; (<*>) = ap
instance Monad (CoroutineT m) where
  return x = CoroutineT $ \fin _ -> fin x
  CoroutineT u >>= f = CoroutineT $ \ fin yld -> u (\x -> runCoroutineT (f x) fin yld) yld

class MonadCoroutine m where yield :: m ()

instance Monad m => MonadCoroutine (CoroutineT m) where
  yield = CoroutineT $ \fin yld -> yld
