{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE TupleSections              #-}
{-# LANGUAGE UndecidableInstances       #-}


module Stepik.W4.Couroutine where

import           Control.Monad
import           Control.Monad.Cont
import           Control.Monad.State
import           Control.Monad.Writer hiding ((<>))
import           Data.Foldable
import           Data.List.NonEmpty   (NonEmpty (..))
import           Data.Semigroup


newtype CoroutineTR r m a = CoroutineT { runCoroutineT :: (a -> m r) -> (CoroutineTR r m a -> m r) -> m r }

type CoroutineT = CoroutineTR ()

instance Functor (CoroutineTR r m) where fmap = liftM
instance Applicative (CoroutineTR r m) where pure = return ; (<*>) = ap
instance Monad (CoroutineTR r m) where
  return x = CoroutineT $ const . flip ($) x
  CoroutineT u >>= f = CoroutineT $ \ ret yld -> u (\x -> runCoroutineT (f x) ret yld) (\c -> yld (c >>= f))

yield :: CoroutineTR r m ()
yield = CoroutineT $ \_ yld -> yld (return ())

runCoroutineList::Monad m => NonEmpty (CoroutineTR r m r) -> m r
runCoroutineList (master :| []) = runCor master return
runCoroutineList (master :| slave : slaves) = runCoroutineT master ret yld where
   ret _ = runCoroutineList (slave :| slaves)
   yld next =  runCoroutineList (slave :| slaves ++ [next])

runCoroutines :: Monad m => CoroutineTR r m r -> CoroutineTR r m r -> m r
runCoroutines master slave = runCoroutineList (master :| [slave])

runCor::Monad m => CoroutineTR r m a->(a -> m r)->m r
runCor c f = runCoroutineT c f (`runCor` f)

--WARNING: listen and pass methods are lying
instance MonadWriter w m => MonadWriter w (CoroutineTR r m) where
  tell w  = CoroutineT $ \ret -> const $ tell w >> ret ()
  listen x = CoroutineT $ \ret -> const $ fst <$> listen (runCor x (\a -> ret (a, mempty)))
  pass x = CoroutineT $ \ret -> const $ pass $ (, id) <$> runCor x (\(a, _) -> ret a)

instance MonadCont (CoroutineTR r m) where
  callCC f = CoroutineT $ \ret yld -> runCoroutineT (f ( \x -> CoroutineT$ \_ _ -> ret x)) ret yld


--- теперь самое крутое - неограниченная конкатенация корутин
newtype Coro r m = Coro{getCoro:: NonEmpty (CoroutineTR r m r)} deriving Semigroup

execCoro::Monad m=>Coro r m->m r
execCoro = runCoroutineList . getCoro

coro::CoroutineTR r m r->Coro r m
coro c = Coro (c :| [])

coro1, coro2, coro3, coro4::Coro () (Writer String)
[coro1, coro2, coro3, coro4] = coro <$> [coroutine1, coroutine2, coroutine3, coroutine4]

coroutine1, coroutine2, coroutine3, coroutine4::CoroutineT (Writer String) ()

coroutine1 = do
  tell "1"
  yield
  tell "2"
--
coroutine2 = do
  tell "a"
  yield
  tell "b"

coroutine3 = do
  tell "1"
  yield
  yield
  tell "2"

coroutine4 = do
  tell "a"
  yield
  tell "b"
  yield
  tell "c"
  yield
  tell "d"
  yield
