{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}

module Stepik.W4.Functor' where

class Functor' c e | c -> e where
  fmap' :: (e -> e) -> c -> c

instance Functor' [a] a where fmap' = fmap
instance Functor' (Maybe a) a where fmap' = fmap
