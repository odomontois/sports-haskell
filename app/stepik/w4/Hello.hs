module Main where

main :: IO ()
main = do
  s <- getContents
  putStrLn $ "Hello, " ++ s
