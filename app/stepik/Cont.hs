module Stepik.Cont where

decode f = f 0 0

as x y f = f x y
a = as
number = (+)

num n x y f = f (x + y) n
mult p x y f = f x (y * p)

[one, two, three, four, five, six, seven, eight, nine, ten,
 eleven, twelve, thirteen, fourteen, fifteen,
 sixteen, seventeen, eighteen, nineteen, twenty] = fmap num [1..20]
[thirty, fourty, fifty, sixty, seventy, eighty, ninety] = fmap num [30,40..90]
hundred = mult 100
thousand = mult 1000
