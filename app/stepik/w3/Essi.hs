module Stepik.W3.Essi where

import           Control.Monad.Except
import qualified Control.Monad.Except       as MTL
import           Control.Monad.Reader       as MTL
import qualified Control.Monad.State        as MTL
import           Control.Monad.Trans.Except
import           Control.Monad.Trans.State

tickCollatz :: State Integer Integer
tickCollatz = do
  n <- get
  let res = if odd n then 3 * n + 1 else n `div` 2
  put res
  return n

type EsSi = ExceptT String (State Integer)

runEsSi :: EsSi a -> Integer -> (Either String a, Integer)
runEsSi = runState . runExceptT

go' :: Integer -> Integer -> State Integer Integer -> EsSi ()
go' low high action = do
  void $ lift action
  current <- MTL.get
  when (current <= low)  $ MTL.throwError "Lower bound"
  when (current >= high) $ MTL.throwError "Upper bound"

go'' :: Integer -> Integer -> State Integer Integer -> EsSi ()
go'' low high action =
  lift action >>
  MTL.get >>= \current ->
  if current <= low then MTL.throwError "Lower bound" else return () >>
  if current >= high then  MTL.throwError "Upper bound" else return ()

type RiiEsSiT m = ReaderT (Integer,Integer) (ExceptT String (StateT Integer m))

runRiiEsSiT :: ReaderT (Integer,Integer) (ExceptT String (StateT Integer m)) a
                -> (Integer,Integer)
                -> Integer
                -> m (Either String a, Integer)
runRiiEsSiT =  (runStateT .) . (runExceptT .). runReaderT

go :: Monad m => StateT Integer m Integer -> RiiEsSiT m ()
go action = do
  (low, high) <- MTL.ask
  void $ lift $ lift action
  current <- MTL.get
  when (current <= low)  $ MTL.throwError "Lower bound"
  when (current >= high) $ MTL.throwError "Upper bound"

tickCollatz' :: StateT Integer IO Integer
tickCollatz' = do
  n <- get
  let res = if odd n then 3 * n + 1 else n `div` 2
  lift $ putStrLn $ show res
  put res
  return n
