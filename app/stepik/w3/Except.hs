module Stepik.W3.Except where
import           Control.Arrow

newtype Except e a = Except{runExcept :: Either e a}

withExcept :: (e -> e') -> Except e a -> Except e' a
withExcept  f =  Except . left f . runExcept

data ListIndexError = ErrIndexTooLarge Int | ErrNegativeIndex
  deriving (Eq, Show)
