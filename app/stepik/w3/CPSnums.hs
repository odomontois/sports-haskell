{-# LANGUAGE RankNTypes #-}
module Stepi.W3.CPSNums where

decode f = f (0, 0)

as (i, x) f = f (i + x)
a = flip ($)
number = id

num n (i, x) f = f (i + x, n)
mult p (i, x) f = f (i, x * p)

one = num 1
two = num 2
three = num 3
seventeen = num 17
twenty = num 20
hundred = mult 100
thousand = mult 1000
