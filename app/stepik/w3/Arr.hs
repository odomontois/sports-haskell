module Stepik.W3.Arr where

-- import           Control.Monad.Trans.Class

newtype Arr2T e1 e2 m a = Arr2T { getArr2T :: e1 -> e2 -> m a }
newtype Arr3T e1 e2 e3 m a = Arr3T { getArr3T :: e1 -> e2 -> e3 -> m a }


arr2 :: Monad m => (e1 -> e2 -> a) -> Arr2T e1 e2 m a
arr3 :: Monad m => (e1 -> e2 -> e3 -> a) -> Arr3T e1 e2 e3 m a

arr2 = Arr2T . ((return.).)
arr3 = Arr3T . (((return.).).)

instance Functor m => Functor (Arr2T e1 e2 m) where fmap f = Arr2T.((fmap f.).).getArr2T
instance Functor m => Functor (Arr3T e1 e2 e3 m) where fmap f = Arr3T.(((fmap f.).).).getArr3T

instance Applicative m => Applicative (Arr2T e1 e2 m) where
  pure = Arr2T . pure . pure . pure
  (Arr2T f) <*> (Arr2T x) = Arr2T $ \a b -> f a b <*> x a b

instance Applicative m => Applicative (Arr3T e1 e2 e3 m) where
  pure = Arr3T . pure . pure . pure . pure
  (Arr3T f) <*> (Arr3T x) = Arr3T $ \a b c -> f a b c <*> x a b c

instance Monad m => Monad (Arr2T e1 e2 m) where
  return = pure
  (Arr2T x) >>= f = Arr2T $ \a b-> x a b >>= \y -> getArr2T (f y) a b

instance Monad m => Monad (Arr3T e1 e2 e3 m) where
  return = pure
  (Arr3T x) >>= f = Arr3T $ \a b c-> x a b c >>= \y -> getArr3T (f y) a b c
  fail  = Arr3T . const . const . const . fail


class MonadTrans t where
  lift :: Monad m => m a -> t m a

instance MonadTrans (Arr2T e1 e2) where lift = Arr2T. const . const

asks2 :: Monad m => (e1 -> e2 -> a) -> Arr2T e1 e2 m a
asks2 = Arr2T . ((return .).)
