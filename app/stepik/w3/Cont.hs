{-# LANGUAGE FlexibleInstances #-}
module Stepik.W3.Cont where

-- import           Control.Monad.Cont       (cont)
-- import           Control.Monad.Trans.Cont (Cont)
import           Control.Monad
import           Data.Semigroup

newtype Cont r a = Cont {runCont::(a->r)->r}

instance Functor (Cont r) where fmap = liftM
instance Applicative (Cont r) where
  pure = return
  (<*>) = ap
instance Monad (Cont r) where
  return x = Cont ($ x)
  Cont v >>= k = Cont $ \c -> v (\a -> runCont (k a) c)

showCont :: Show a => Cont String a -> String
showCont = (`runCont` show)

evalCont  :: Cont a a->a
evalCont c = runCont c id

type Checkpointed a = (a -> Cont a ())-> Cont a a

checkpointed ::(a -> Bool) -> Checkpointed a -> Cont r a
checkpointed cond cp = return $ evalCont $ cp (\x -> Cont $ \c' -> let y = c' () in if cond y then y else x)

addTens :: Int -> Checkpointed Int
addTens x1 checkpoint = do
  checkpoint x1
  let x2 = x1 + 10
  checkpoint x2     {- x2 = x1 + 10 -}
  let x3 = x2 + 10
  checkpoint x3     {- x3 = x1 + 20 -}
  let x4 = x3 + 10
  return x4         {- x4 = x1 + 30 -}

callCC ::((a -> Cont r b) -> Cont r a) -> Cont r a
callCC f = Cont $ \c-> runCont (f $ \a-> Cont $ \_ -> c a) c
