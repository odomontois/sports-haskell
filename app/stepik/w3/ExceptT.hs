module Stepik.W3.ExceptT where

import           Control.Applicative
import           Control.Monad.Except

data ListIndexError = ErrIndexTooLarge Int | ErrNegativeIndex deriving (Eq, Show)

infixl 9 !!!
(!!!) :: [a] -> Int -> Except ListIndexError a
(!!!) = let go::Int->[a]->Except ListIndexError a
            go i | i < 0 = const $ throwError ErrNegativeIndex
                 | otherwise = foldr search tooLarge . zip [0..] where
                   tooLarge = throwError $ ErrIndexTooLarge i
                   search (j, x) next = if  i == j then return x else next
        in flip go

data ReadError = EmptyInput | NoParse String deriving Show


tryRead :: Read a => String -> Except ReadError a
tryRead []                   = throwError EmptyInput
tryRead s = case reads s of
  (x, []): _ -> return x
  _          -> throwError $ NoParse s

data SumError = SumError Int ReadError deriving Show

trySum :: [String] -> Except SumError Integer
trySum = fmap sum.traverse tr.zip[1..] where tr=uncurry$ (.tryRead).withExcept.SumError

newtype SimpleError = Simple { getSimple :: String } deriving (Eq, Show)

-- deriving instance Monoid SimpleError

instance Monoid SimpleError where
  mempty = Simple mempty
  mappend = (Simple.).(.getSimple).mappend.getSimple

lie2se :: ListIndexError -> SimpleError
lie2se x = Simple ('[': display x "]") where
  display ErrNegativeIndex = mappend "negative index"
  display (ErrIndexTooLarge i) = mappend "index (" .
                                 shows i .
                                 mappend ") is too large"


newtype Validate e a = Validate { getValidate :: Either [e] a }

instance Functor (Validate e) where fmap = liftA
instance Applicative (Validate e) where
  pure = Validate . return
  Validate fa <*> Validate xa = Validate $ either (Left . either (flip mappend) (flip const) xa) (<$> xa) fa

collectE :: Except e a -> Validate e a
collectE = Validate .runExcept.withExcept return

validateSum :: [String] -> Validate SumError Integer
validateSum = fmap sum.traverse (collectE.tr).zip[1..] where tr=uncurry$ (.tryRead).withExcept.SumError
