{-# LANGUAGE FlexibleContexts #-}

module Stepik.W3.Trans where
import           Control.Monad
import           Control.Monad.Reader (Reader, ReaderT, lift, runReaderT)
import qualified Control.Monad.Reader as MTL
import           Control.Monad.Writer (Writer, WriterT, runWriterT)
import qualified Control.Monad.Writer as MTL
import           Data.Char
import           Data.Maybe           (catMaybes)
import           Data.Monoid

logFirstAndRetSecond' :: WriterT String (Reader [String]) String
logFirstAndRetSecond' = do
  el1 <- MTL.asks head
  el2 <- MTL.asks (map toUpper.head.tail)
  MTL.tell el1
  return el2


separate :: (a -> Bool) -> (a -> Bool) -> [a] -> WriterT [a] (Writer [a]) [a]
separate p1 p2 = fmap catMaybes. mapM analyze where
    analyze x = do
      when (p1 x) $  MTL.tell [x]
      when (p2 x) $ lift $  MTL.tell [x]
      return$ mfilter (not.or.sequenceA[p1,p2]) $ Just x


type MyRWT m = ReaderT [String] (WriterT String m)

runMyRWT :: MyRWT m a -> [String] -> m (a, String)
runMyRWT = (runWriterT .). runReaderT

myAsks :: Monad m => ([String] -> a) -> MyRWT m a
myAsks = MTL.asks

myTell :: Monad m => String -> MyRWT m ()
myTell = MTL.tell

myLift :: Monad m => m a -> MyRWT m a
myLift = lift . lift

myAsk :: Monad m => MyRWT m [String]
myAsk = MTL.ask


logFirstAndRetSecond :: MyRWT Maybe String
logFirstAndRetSecond = do
  xs <- myAsk
  case xs of
    (el1 : el2 : _) -> myTell el1 >> return (map toUpper el2)
    _               -> myLift Nothing

headOpt::MonadPlus m=>[a]->m (a, [a])
headOpt (x:xs) = return (x, xs)
headOpt []     = mzero

veryComplexComputation :: MyRWT Maybe (String, String)
veryComplexComputation = do
  odds <- MTL.asks (filter (odd.length))
  evens <- MTL.asks (filter (even.length))
  (o1, o2) <- getHeads odds
  (e1, e2) <- getHeads evens
  MTL.tell $ e1 <> "," <> o1
  return (e2, o2) where
    getHeads lst = do
      (x, rest) <- headOpt lst
      (y, _)    <- headOpt rest
      return (x, toUpper <$> y)
