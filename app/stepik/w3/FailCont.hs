module Stepik.W3.FailCont where

import           Control.Monad
import           Control.Monad.Except

newtype FailCont r e a = FailCont { runFailCont :: (a->r)->(e->r)->r }

instance Functor (FailCont r e) where fmap = liftM
instance Applicative (FailCont r e) where
  pure = return
  (<*>) = ap
instance Monad (FailCont r e) where
  return x = FailCont $ \ok _->ok x
  FailCont c >>= f = FailCont $ \ok ex->c (\a -> runFailCont (f a) ok ex) ex

toFailCont :: Except e a -> FailCont r e a
toFailCont x = FailCont$ \ok ex->either ex ok $ runExcept x

evalFailCont :: FailCont (Either e a) e a -> Either e a
evalFailCont (FailCont c) = c Right Left

-- callCC ::((a -> Cont r b) -> Cont r a) -> Cont r a
-- callCC f = Cont $ \c-> runCont (f $ \a-> Cont $ \_ -> c a) c


callCFC ::((a -> FailCont r e b) -> FailCont r e a) -> FailCont r e a
callCFC f = FailCont $ \c -> runFailCont (f $ \a-> FailCont $ \_ _ ->c a) c
