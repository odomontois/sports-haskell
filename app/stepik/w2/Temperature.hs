{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Stepik.W2.Temperature where

newtype Temperature a = Temperature Double deriving (Eq, Num,Show)

data Celsius
data Fahrenheit
data Kelvin

k2c :: Temperature Kelvin -> Temperature Celsius
k2c (Temperature x) = Temperature $ x + 239.5
