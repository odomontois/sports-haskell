module Stepik.W1.PrsE where

import           Control.Applicative

newtype PrsEP a = PrsEP { runPrsEP :: Int->String -> (Int, Either String (a, String)) }

parseEP :: PrsEP a -> String -> Either String (a, String)
parseEP p  = snd . runPrsEP p 0

reportPos::Int->ShowS
reportPos p = ("pos " ++) . shows p . (": " ++)

satisfyEP :: (Char -> Bool) -> PrsEP Char
satisfyEP cond = PrsEP prs where
  prs pos [] = (pos + 1, Left $ reportPos (pos + 1) "unexpected end of input")
  prs pos (x:xs) | cond x       = (pos + 1, Right (x, xs))
                 | otherwise    = (pos + 1, Left $ reportPos (pos + 1) $ "unexpected " ++ return x)

charEP :: Char -> PrsEP Char
charEP c = satisfyEP (== c)
--
instance Functor PrsEP where
  fmap f (PrsEP p) = PrsEP $ \i s ->
    let (j, res) = p i s
    in case res of
      Left err        -> (j, Left err)
      Right (x, rest) -> (j, Right (f x, rest))

--
instance Applicative PrsEP where
  pure x = PrsEP $ \i s->(i, Right (x, s))
  PrsEP pl <*> PrsEP pr = PrsEP $ \i s ->
    let (j, mid) = pl i s
    in case mid of
      Left err -> (j, Left err)
      Right (f, took) ->
        let (k, res) = pr j took
        in case res of
          Left err        -> (k, Left err)
          Right (x, rest) -> (k, Right (f x, rest))
--
-- instance Monad PrsE where
--   return x = PrsE $ \s-> Right (x, s)
--   (PrsE pa) >>= f = PrsE $ \s -> do
--     (x , s') <- pa s
--     runPrsE (f x) s'
--
instance Alternative PrsEP where
  empty = PrsEP $ \_ _ -> (0, Left "pos 0: empty alternative")
  PrsEP pl <|> PrsEP pr = PrsEP $ \i s ->
      let retl@(jl, resl) = pl i s
          retr@(jr, resr) = pr i s
          result | not $ null resl = retl
                 | not $ null resr = retr
                 | jl >= jr         = retl
                 | otherwise       = retr
      in result
