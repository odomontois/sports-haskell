module Stepik.W2.Traversable where
import           Control.Monad
import           Data.Foldable
import           Data.Monoid
import           Data.Traversable

data OddC a = Un a | Bi a a (OddC a) deriving (Eq,Show)

instance Functor OddC where
  fmap f (Un x)        = Un (f x)
  fmap f (Bi x y rest) = Bi (f x) (f y) $ fmap f rest

instance Foldable OddC where
  foldMap f (Un x)        = f x
  foldMap f (Bi x y rest) = f x <> f y <> foldMap f rest

instance Traversable OddC where
  sequenceA (Un x)        = Un <$> x
  sequenceA (Bi x y rest) = Bi <$> x <*> y <*> sequenceA rest

concat2OC::OddC a-> OddC a->(a, OddC a)
concat2OC (Un a) x        = (a, x)
concat2OC (Bi a b rest) x = let (c, t) = concat2OC rest x in (a, Bi b c t)

concat3OC :: OddC a -> OddC a -> OddC a -> OddC a
concat3OC (Un a) y z        = let (b, rest) = concat2OC y z in Bi a b rest
concat3OC (Bi a b rest) y z = Bi a b $ concat3OC rest y z

concatOC :: OddC (OddC a) -> OddC a
concatOC (Un x)        =       x
concatOC (Bi a b rest) = concat3OC a b (concatOC rest)

instance Applicative OddC where
  pure = return
  (<*>) = ap

instance Monad OddC where
  return = Un
  (>>=) = flip $ (concatOC .) . fmap
