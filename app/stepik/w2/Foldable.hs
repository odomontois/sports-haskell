module Stepik.W2.Foldable where

import           Data.Foldable
import           Data.Monoid

traverse2list :: (Foldable t, Applicative f) => (a -> f b) -> t a -> f [b]
traverse2list = (. toList) . traverse

mkEndo :: Foldable t => t (a -> a) -> Endo a
mkEndo = foldMap Endo
