module Stepik.W2.Result where
import           Data.Monoid

data Result a = Ok a | Error String deriving (Eq,Show)

instance Functor Result where
  fmap f (Ok x)    = Ok (f x)
  fmap _ (Error s) = Error s

instance Foldable Result where
  foldMap f (Ok x)    = f x
  foldMap _ (Error _) = mempty

instance Traversable Result where
  sequenceA (Ok x)    = Ok <$> x
  sequenceA (Error s) = pure (Error s)
