module Stepik.W2.Tree where

import           Data.Monoid
import           Data.Traversable

data Tree a = Nil | Branch (Tree a) a (Tree a)   deriving (Eq, Show)

foldMapInt::Monoid b=>(b->b->b->b)->(a->b)->Tree a->b
foldMapInt g f = go where
  go Nil            = mempty
  go (Branch l m r) = g (go l) (f m) (go r)

zipl::Monoid a=>[a]->[a]->[a]
zipl [] x           = x
zipl x []           = x
zipl (x:xs) (y: ys) = x <> y : zipl xs ys

instance Foldable Tree where foldMap = foldMapDefault

instance Functor Tree where
  fmap _ Nil            = Nil
  fmap f (Branch l m r) = Branch (fmap f l) (f m) (fmap f r)

-- instance Traversable Tree where
--   sequenceA Nil            = pure Nil
--   sequenceA (Branch l m r) = Branch <$> sequenceA l <*> m <*> sequenceA r

instance Traversable Tree where
  sequenceA Nil            = pure Nil
  sequenceA (Branch fl fm fr) = (flip.Branch) <$> sequenceA fl <*> sequenceA fr <*> fm

newtype Preorder a   = PreO   (Tree a)    deriving (Eq, Show)
newtype Postorder a  = PostO  (Tree a)    deriving (Eq, Show)
newtype Levelorder a = LevelO (Tree a)    deriving (Eq, Show)

instance Functor Preorder where fmap f = PreO .fmap f.(\(PreO x) -> x)
instance Functor Postorder where fmap f = PostO .fmap f.(\(PostO x) -> x)
instance Functor Levelorder where fmap f = LevelO .fmap f.(\(LevelO x) -> x)

instance Foldable Preorder where foldMap = (. \(PreO x) -> x) . foldMapInt (\l m r->m <> l <> r)
instance Foldable Postorder where foldMap = (. \(PostO x) -> x). foldMapInt (\l m r->l <> r <> m)
instance Foldable Levelorder where
  foldMap f (LevelO t) = foldMap id $ go t where
    go Nil            = []
    go (Branch l m r) = f m : zipl (go l) (go r)

infTree::Num a=>Tree a
infTree = mkTree 0 where mkTree x = Branch (mkTree $ 2 * x + 1) x  (mkTree $ 2 * x + 2)
