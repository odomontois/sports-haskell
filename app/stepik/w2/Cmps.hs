{-# LANGUAGE TypeOperators #-}
module Stepik.W2.Cmps where
import           Data.Monoid

infixr 9 |.|
newtype (|.|) f g a = Cmps { getCmps :: f (g a) }  deriving (Eq,Show)

instance (Functor f, Functor g)=>Functor (f |.| g) where fmap f  = Cmps . fmap (fmap f ). getCmps

instance (Foldable a, Foldable b)=>Foldable (a |.| b) where foldMap = (. getCmps).foldMap.foldMap

instance (Traversable f, Traversable g)=>Traversable (f |.| g) where
  sequenceA (Cmps x) = Cmps <$> sequenceA (sequenceA <$> x)
