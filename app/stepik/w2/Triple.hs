module Stepik.W2.Triple where
import           Data.Monoid
data Triple a = Tr a a a  deriving (Eq,Show)

instance Foldable Triple where foldMap f (Tr x y z) = f x <> f y <> f z
instance Functor Triple where fmap f (Tr x y z) = Tr (f x) (f y) (f z)
instance Traversable Triple where sequenceA (Tr x y z) = Tr <$> x <*> y <*> z
